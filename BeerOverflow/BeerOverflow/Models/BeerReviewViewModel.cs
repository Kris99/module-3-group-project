﻿using BeerOverflow.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Models
{
    public class BeerReviewViewModel
    {
        public int Id { get; set; }

        [Required, MinLength(3), MaxLength(500)]
        public string Review { get; set; }
        public int BeerId { get; set; }
        public string BeerName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool IsFlagged { get; set; }
        public bool IsLiked { get; set; }
        public int Likes { get; set; }
        public bool IsDisliked { get; set; }
    }
}
