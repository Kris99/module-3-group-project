﻿using BeerOverflow.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Models
{
    public class BeerViewModel
    {
        [DisplayName("ID")]
        public int Id { get; set; }

        [Required, MinLength(3), MaxLength(70)]
        public string Name { get; set; }

        [Required, Range(0, 25)]
        public double ABV { get; set; }

        [Required]
        [DisplayName("Style name")]
        public string StyleName { get; set; }

        [Required]
        [DisplayName("Brewery name")]
        public string BreweryName { get; set; }
        public List<BeerReview> BeerReviews { get; set; }
        public double Rating { get; set; }

        public bool ShowSuccessMessage { get; set; }
        public bool IsRated { get; set; }
        public bool IsReviewed { get; set; }

    }
}
