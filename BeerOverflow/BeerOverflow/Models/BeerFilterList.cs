﻿using BeerOverflow.Service.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Models
{
    public class BeerFilterList
    {
        public IEnumerable<BeerDTO> ListBeers { get; set; }

        public FilterCriteria FilterCriteria { get; set; }
    }
}
