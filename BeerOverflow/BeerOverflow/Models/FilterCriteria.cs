﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Models
{
    public class FilterCriteria
    {
        public int Country { get; set; }

        public int Style { get; set; }
    }
}
