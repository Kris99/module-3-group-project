﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Service.DTO;

namespace BeerOverflow.Models
{
    public class BreweryViewModel
    {
        public ICollection<BreweryDTO> Breweries { get; set; }

        public int CurrentPage { get; set; }
    }
}
