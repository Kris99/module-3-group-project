﻿using BeerOverflow.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Models
{
    public class ReviewsViewModel
    {
        public IEnumerable<BeerReview> Reviews { get; set; }
    }
}
