﻿using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.API_Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeersApiController : ControllerBase
    {
        private readonly IBeerService _service;
        private readonly IBeerReviewService _reviewService;

        public BeersApiController(IBeerService service, IBeerReviewService reviewService)
        {
            _service = service;
            _reviewService = reviewService;
        }

        //GET api/beers
        [HttpGet("")]
        public IActionResult GetAllBeers()
        {
            var beers = _service.GetAllBeersAsync().Result;      

            if(beers == null)
            {
                return NotFound();
            }

            return Ok(beers);
        }

        //GET api/beers/:id
        [HttpGet("{id}")]
        public IActionResult GetBeerById(int id)
        {
            var beer = _service.GetBeerByIdAsync(id).Result;

            if (beer == null)
            {
                return NotFound();
            }

            return Ok(beer);
        }

        //POST api/beers
        [HttpPost("")]
        public IActionResult CreateBeer([FromBody] BeerViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            var beerDTO = new BeerDTO
            {
                Name = model.Name,
                ABV = model.ABV,
                StyleName = model.StyleName,
                BreweryName = model.BreweryName
            };

            var beer = this._service.CreateBeerAsync(beerDTO).Result;

            return Created("post", beer);
        }

        //PUT api/beers/:id
        [HttpPut("{id}")]
        public IActionResult UpdateBeer(int id, [FromBody] BeerViewModel model)
        {
            if (id < 1 || model == null)
            {
                return BadRequest();
            }
            
            var beerDTO = new BeerDTO
            {
                Name = model.Name,
                ABV = model.ABV,
                StyleName = model.StyleName,
                BreweryName = model.BreweryName
            };

            var updatedBeerDTO = this._service.UpdateBeerAsync(id, beerDTO).Result;

            return Ok(updatedBeerDTO);
        }

        //DELETE api/beers/:id
        [HttpDelete("{id}")]
        public IActionResult DeleteBeer(int id)
        {
            var result = this._service.DeleteBeerAsync(id).Result;

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        //UNLIST api/beers/:id/unlist/
        [HttpPut("{id}/unlist")]
        public IActionResult UnlistBeer(int id)
        {
            bool result = this._service.UnlistBeerAsync(id).Result;

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        //RATE api/beers/:id/rate
        [HttpPut("{id}/rate")]
        public IActionResult RateBeer([FromBody] BeerRatingDTO beerRatingDTO)
        {
            var rating = this._service.RateBeerAsync(beerRatingDTO).Result;

            return Created("rating", rating);
        }

        //GET api/beers/filter?criteria&filter    https://localhost:5001/api/beers/filter?criteria=country&filter=Ireland
        [HttpGet("filter")]
        public IActionResult FilterBeers([FromQuery] string criteria = "", [FromQuery] string filter = "")
        {
            var beers = _service.FilterBeersAsync(criteria, filter).Result;

            if (beers == null)
            {
                return NotFound();
            }

            return Ok(beers);
        }

        //GET api/beers/sort?name https://localhost:5001/api/beers/sort?criteria=name
        [HttpGet("sort")]
        public IActionResult SortBeers([FromQuery] string criteria = "")
        {
            var beers = _service.SortBeersAsync(criteria);

            if (beers == null)
            {
                return NotFound();
            }

            return Ok(beers);
        }

        //GET api/beers/id/reviews 
        [HttpGet("{id}/reviews")]
        public IActionResult GetBeerReviews(int id)
        {
            var beerReviews = this._reviewService.GetBeerReviewsAsync(id);

            if (beerReviews == null)
            {
                return NotFound();
            }

            return Ok(beerReviews);
        }

        //POST api/beers/id/reviews
        [HttpPost("{id}/reviews")]
        public IActionResult CreateBeerReview([FromBody] BeerReviewViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            var beerReviewDTO = new BeerReviewDTO
            {
                Review = model.Review.ToString(),
            };

            var beerReview = this._reviewService.CreateBeerReviewAsync(beerReviewDTO);

            return Created("post", beerReview);
        }

        //DELETE api/beers/id/reviews/reviewId
        [HttpDelete("{id}/reviews/{reviewId}")]
        public IActionResult DeleteBeerReview(int reviewId)
        {
            bool result = this._reviewService.DeleteBeerReviewAsync(reviewId).Result;

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
        

        //FLAG api/beers/id/reviews/reviewId/flag 
        [HttpGet("{id}/reviews/{reviewId}/flag")]
        public IActionResult FlagBeerReview(int reviewId)
        {
            bool result = this._reviewService.FlagBeerReviewAsync(reviewId).Result;

            if (result == true)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
