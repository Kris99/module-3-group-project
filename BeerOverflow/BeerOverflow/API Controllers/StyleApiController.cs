﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.API_Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StyleApiController : ControllerBase
    {
        private readonly IStyleService _service;

        public StyleApiController(IStyleService service)
        {
            _service = service;
        }

        //GET api/style/getstyle
        [HttpGet]
        [Route("getstyle")]
        public async Task<IActionResult> GetStyleById(int id)
        {
            var style = await _service.GetStyleAsync(id);

            if (style == null)
            {
                return NotFound();
            }

            return Ok(style);
        }


        //GET api/style/getallstyles
        [HttpGet]
        [Route("getallstyles")]
        public IActionResult GetAllStyles()
        {
            var style = _service.GetAllStyles();

            if (style == null)
            {
                return NotFound();
            }

            return Ok(style);
        }


        //GET api/style/createstyle
        [HttpGet]
        [Route("createstyle")]
        public async Task<IActionResult> CreateStyle(string name)
        {
            var styleDTO = new StyleDTO
            {
                Name = name,
            };

            var style = await _service.CreateStyleAsync(styleDTO);

            return Created("post", styleDTO);
        }

        //GET api/style/updatestyle
        [HttpGet]
        [Route("updatestyle")]
        public async Task<IActionResult> UpdateStyle(int id, string newName)
        {
            var style = await _service.UpdateStyleAsync(id, newName);

            if (style == null)
            {
                return NotFound();
            }

            return Ok();
        }

        //GET api/style/deletestyle
        [HttpGet]
        [Route("deletestyle")]
        public async Task<IActionResult> DeleteStyle(int id)
        {
            await _service.DeleteStyleAsync(id);
            return Ok();
        }
    }
}
