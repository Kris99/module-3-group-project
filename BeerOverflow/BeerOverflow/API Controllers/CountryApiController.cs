﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Mvc;


namespace BeerOverflow.API_Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryApiController : ControllerBase
    {
        private readonly ICountryService _service;

        public CountryApiController(ICountryService service)
        {
            _service = service;
        }

        //GET api/country/getcountry
        [HttpGet]
        [Route("getcountry")]
        public async Task<IActionResult> GetCountryById(int id)
        {
            var country = await _service.GetCountryAsync(id);

            if (country == null)
            {
                return NotFound();
            }

            return Ok(country);
        }

        //GET api/country/getallcountries
        [HttpGet]
        [Route("getallcountries")]
        public IActionResult GetAllCountries()
        {
            var country = _service.GetAllCountries();

            if (country == null)
            {
                return NotFound();
            }

            return Ok(country);
        }

        //GET api/country/createcountry
        [HttpGet]
        [Route("createcountry")]
        public async Task<IActionResult> CreateCountry(string name)
        {
            var countryDTO = new CountryDTO
            {
                Name = name,
            };

            var brewery = await _service.CreateCountryAsync(countryDTO);
            
            return Created("post", countryDTO);
        }

        //GET api/country/updatecountry
        [HttpGet]
        [Route("updatecountry")]
        public async Task<IActionResult> UpdateCountry(int id, string newName)
        {
            var country = await _service.UpdateCountryAsync(id, newName);

            if (country == null)
            {
                return NotFound();
            }

            return Ok();
        }

        //GET api/country/deletecountry
        [HttpGet]
        [Route("deletecountry")]
        public async Task<IActionResult> DeleteCountry(int id)
        {
            await _service.DeleteCountryAsync(id);
            return Ok();
        }
    }
}
