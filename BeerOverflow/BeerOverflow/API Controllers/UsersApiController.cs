﻿using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.API_Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersApiController : ControllerBase
    {
        private readonly IUserService _service;
        public UsersApiController(IUserService service)
        {
            _service = service;
        }

        //GET api/users
        [HttpGet("")]
        public IActionResult GetAllUsers()
        {

            var users = _service.GetAllUsers();      

            if(users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        //GET api/users/:id
        [HttpGet("{id}")]
        public IActionResult GetUserById(int id)
        {
            var user = _service.GetUserById(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        //POST api/users
        [HttpPost("")]
        public IActionResult CreateUser([FromBody] User user)
        {
            var userDTO = new UserDTO(user);

            var result = _service.CreateUserAsync(userDTO);

            if (result.Result == true)
                return Created("post", userDTO);
            else
                return NotFound();
        }

        //PUT api/users?id=[value]
        [HttpPut("")]
        public IActionResult UpdateUser([FromBody] User user)
        {
            var userDTO = _service.UpdateUserAsync(user);

            if(userDTO == null)
            {
                return NotFound();
            }

            return Ok();
        }

        //DELETE api/users/:id
        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            Task<UserDTO> result = _service.DeleteUserAsync(id);

            if (result == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        //GET api/users/:id
        [HttpGet("{id}")]
        public IActionResult GetUsersWishlist(int userId)
        {
            IEnumerable<Beer> wishlist = _service.GetWishlist(userId);
        
            if (wishlist == null)
            {
                return NotFound();
            }
        
            return Ok(wishlist);
        }

        //GET api/users/:id
        [HttpGet("{id}")]
        public IActionResult GetUsersDranklist(int userId)
        {
            IEnumerable<Beer> wishlist = _service.GetDranklist(userId);
        
            if (wishlist == null)
            {
                return NotFound();
            }
        
            return Ok(wishlist);
        }


        //PUT api/users/:id
        [HttpPut("{id}")]
        public IActionResult AddBeerToWishlist(int beerID, User user)
        {
            Task<bool> result = _service.AddBeerToWishlistAsync(beerID, user.Id);

            if (result.Result == false)
            {
                return NotFound();
            }

            return Ok();
        }


        //PUT api/users/:id
        [HttpPut("{id}")]
        public IActionResult AddBeerToDranklist(int beerID, int userID)
        {
            Task<bool> result = _service.AddBeerToDranklistAsync(beerID, userID);

            if (result.Result != true)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
