﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Mvc;


namespace BeerOverflow.API_Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BreweryApiController : ControllerBase
    {
        private readonly IBreweryService _service;

        public BreweryApiController(IBreweryService service)
        {
            _service = service;
        }

        //GET api/brewery/getbrewery
        [HttpGet]
        [Route("getbrewery")]
        public async Task<IActionResult> GetBreweryById(int id)
        {
            var brewery = await _service.GetBreweryAsync(id);

            if (brewery == null)
            {
                return NotFound();
            }

            return Ok(brewery);
        }

        //GET api/brewery/getallbreweries
        [HttpGet]
        [Route("getallbreweries")]
        public IActionResult GetAllBreweries()
        {
            var brewery = _service.GetAllBreweries();

            if (brewery == null)
            {
                return NotFound();
            }

            return Ok(brewery);
        }

        //GET api/brewery/createbrewery
        [HttpPost]
        [Route("createbrewery")]
        public async Task<IActionResult> CreateBrewery(string name, int countryId)
        {
            var breweryDTO = new BreweryDTO
            {
                Name = name,
                CountryId = countryId,
            };

            var brewery = await _service.CreateBreweryAsync(breweryDTO);

           return Created("post", brewery);
        }

        //GET api/brewery/updatebrewery
        [HttpGet]
        [Route("updatebrewery")]
        public async Task<IActionResult> UpdateBrewery(int id, string name, int countryId)
        {
            var brewery = await _service.UpdateBreweryAsync(id, name, countryId);

            if (brewery == null)
            {
                return NotFound();
            }

            return Ok();
        }

        //GET api/brewery/deletebrewery
        [HttpGet]
        [Route("deletebrewery")]
        public async Task<IActionResult> DeleteBrewery(int id)
        {
            await _service.DeleteBreweryAsync(id);
            return Ok();
        }
    }
}
