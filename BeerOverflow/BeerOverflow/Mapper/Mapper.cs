﻿using AutoMapper;
using BeerOverflow.Data.Models;
using BeerOverflow.Models;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Mapper
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<BeerDTO, BeerViewModel>();
            CreateMap<BeerViewModel, BeerDTO>();
            CreateMap<IEnumerable<BeerReviewDTO>, IEnumerable<BeerReview>>();
            CreateMap<BeerReviewDTO, BeerReviewViewModel>();
            CreateMap<BeerReviewViewModel, BeerReviewDTO>();

            //UserDTO <-> UserViewModel
            CreateMap<UserDTO, UserViewModel>();
            CreateMap<UserViewModel, UserDTO>();

            //User <-> UserDTO
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();

            //register <-> UserDTO
            CreateMap<RegisterViewModel, UserDTO>();
            CreateMap<UserDTO, RegisterViewModel>();

            //login <-> UserDTO
            CreateMap<LoginViewModel, UserDTO>();
            CreateMap<UserDTO, LoginViewModel>();

            //User <-> UserViewModel
            CreateMap<User, UserViewModel>();
            CreateMap<UserViewModel, User>();

            //User -> ReviewsViewModel
            CreateMap<User, ReviewsViewModel>();

        }
    }
}
