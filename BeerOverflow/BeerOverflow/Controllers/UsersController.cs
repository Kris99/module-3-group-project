﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Models;
using AutoMapper;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Microsoft.AspNetCore.Authorization;

namespace BeerOverflow.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService _service;
        private readonly IMapper _mapper;
        private readonly IBeerService beerService;
        
        public UsersController(IUserService service, IMapper mapper, IBeerService beerService)
        {
            _service = service;
            _mapper = mapper;
            this.beerService = beerService;
        }

        // GET: Users
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            var viewModel = new UserViewModel();

            viewModel.Users = await _service.GetAllUsers();

            if(viewModel.Users == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userDTO = await _service.GetUserById(id);
            if (userDTO == null)
            {
                return NotFound();
            }

            UserViewModel viewModel = _mapper.Map<UserViewModel>(userDTO);

            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        // GET: Users/Create
        public IActionResult Create()
        {
            return View();
        }


        // POST: Users/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RegisterViewModel model) //creating a user is the same as registering one, thats why I am using this viewModel
        {
            
            if (ModelState.IsValid)
            {
                var userDTO = _mapper.Map<UserDTO>(model);
                bool result = await _service.CreateUserAsync(userDTO);

                if(result == true)
                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }
        // GET: Users/Login
        public IActionResult Login()
        {
            return View();
        }


        // GET: Users/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {

            if (ModelState.IsValid)
            {
                var userDTO = _mapper.Map<UserDTO>(model);
                bool result = await _service.LoginUserAsync(userDTO);

                if (result == true)
                    return View("LoginConfirmed", userDTO);
            }

            return View(model);
        }

        // GET: Users/Logout
        public IActionResult Logout()
        {
             _service.LogoutUserAsync();

            return RedirectToAction("Index", "Home");
        }

        // GET: Users/Register
        public IActionResult Register()
        {
            return View();
        }

        // POST: Users/Register  
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {

            if (ModelState.IsValid)
            {
                var userDTO = _mapper.Map<UserDTO>(model);
                bool result = await _service.RegisterUserAsync(userDTO);

                if (result == true)
                    return View("RegistrationConfirmed", userDTO);
            }

            return View(model);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _service.GetUserById(id);

            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FirstName,LastName,Id,UserName,Email")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.UpdateUserAsync(user);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (UserExists(id) == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            UserViewModel viewModel = _mapper.Map<UserViewModel>(user);

            return View(viewModel);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _service.GetUserById(id);

            if (user == null)
            {
                return NotFound();
            }

            UserViewModel viewModel = _mapper.Map<UserViewModel>(user);

            return View(viewModel);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _service.DeleteUserAsync(id);

            return RedirectToAction(nameof(Index));
        }

        // GET: Users/BanUser/id
        public async Task<IActionResult> BanUser(int? id)
        {
            if(id == null)
            {
                return NotFound();
            }

            var user = await _service.GetUserById(id);

            if(user == null)
            {
                return NotFound();
            }

            UserViewModel viewModel = _mapper.Map<UserViewModel>(user);

            return View(viewModel);
        }

        // POST: Users/BanUser/5
        [HttpPost, ActionName("BanUser")]
        [ValidateAntiForgeryToken]
        public IActionResult BanConfirmed(int id)
        {
             _service.BanUser(id);

            return RedirectToAction(nameof(Index));
        }

        // GET: Users/UnbanUser/id
        public async Task<IActionResult> UnbanUser(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _service.GetUserById(id);

            if (user == null)
            {
                return NotFound();
            }

            UserViewModel viewModel = _mapper.Map<UserViewModel>(user);

            return View(viewModel);
        }

        // POST: Users/BanUser/5
        [HttpPost, ActionName("UnbanUser")]
        [ValidateAntiForgeryToken]
        public IActionResult UnbanConfirmed(int id)
        {
            _service.UnbanUser(id);

            return RedirectToAction(nameof(Index));
        }

        private  Task<UserDTO> UserExists(int id)
        {
            return _service.GetUserById(id);
        }

        [HttpPost]
        [ActionName("AddToWishlist")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddToWishlist(int beerId, int userId)
        {
            var result = await _service.AddBeerToWishlistAsync(beerId, userId);

            TempData["Success"] = true;

            if (result == false)
            {
                TempData["Success"] = false;
            }

            var beer = await beerService.GetBeerByIdAsync(beerId);

            var beerViewModel = _mapper.Map<BeerViewModel>(beer);

            beerViewModel.ShowSuccessMessage = (bool)TempData["Success"];

            return RedirectToAction(nameof(Details), "Beers", new { id = beer.Id });
        }

        [HttpPost]
        [ActionName("AddToDranklist")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddToDranklist(int beerId, int userId)
        {
            var result = await _service.AddBeerToDranklistAsync(beerId, userId);

            TempData["Success"] = true;

            if (result == false)
            {
                TempData["Success"] = false;
            }

            var beer = await beerService.GetBeerByIdAsync(beerId);

            var beerViewModel = _mapper.Map<BeerViewModel>(beer);

            beerViewModel.ShowSuccessMessage = (bool)TempData["Success"];

            return RedirectToAction(nameof(Details), "Beers", new { id = beer.Id });
        }

    }
}
