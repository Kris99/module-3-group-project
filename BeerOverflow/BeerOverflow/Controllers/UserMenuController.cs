﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Controllers
{
    public class UserMenuController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IUserService _service;

        public UserMenuController(UserManager<User> manager, IUserService service)
        {
            this.userManager = manager;
            _service = service;
        }

        public async Task<IActionResult> MyWishlist()
        {
            var user = await userManager.GetUserAsync(User);

            var list = _service.GetWishlist(user.Id);

            return View(list);
        }

        public async Task<IActionResult> MyDranklist()
        {
            var user = await userManager.GetUserAsync(User);

            var list = _service.GetDranklist(user.Id);

            return View(list);
        }

        public async Task<IActionResult> MyStatus()
        {
            var user = await userManager.GetUserAsync(User);

            return View(user);
        }
    }
}
