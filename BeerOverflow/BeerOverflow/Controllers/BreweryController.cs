﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Data.Models;
using BeerOverflow.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Controllers
{
    public class BreweryController : Controller
    {
        private readonly IBreweryService _service;

        public BreweryController(IBreweryService service)
        {
            this._service = service;
        }

        public async Task<IActionResult> Index()
        {
            var list = await _service.GetAllBreweries();

            return View(list);
        }

        public async Task<IActionResult> Details(int id)
        {
            var brewery = await _service.GetBreweryAsync(id);

            return View(brewery);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BreweryDTO breweryDTO)
        {
            var brewery = await _service.CreateBreweryAsync(breweryDTO);

            return Redirect("Index");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var brewery = await _service.GetBreweryAsync((int)id);

            return View(brewery);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Id,Name,CountryId")] BreweryDTO breweryDTO)
        {
            var brewery = await _service.UpdateBreweryAsync(breweryDTO);

            return View("Details",brewery);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var dto = await _service.GetBreweryAsync((int)id);
            
            return View(dto);
        }

        [HttpPost]
        public async Task<IActionResult> Delete([Bind("Id")] BreweryDTO breweryDTO)
        {
            await _service.DeleteBreweryAsync((int)breweryDTO.Id);

            return RedirectToAction("Index");
        }
    }
}