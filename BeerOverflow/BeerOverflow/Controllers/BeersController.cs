﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Controllers
{
    [Route("/[controller]")]
    public class BeersController : Controller
    {
        private readonly BeerOverflowContext _context;
        private readonly IMapper _mapper;
        private readonly IBeerService _service;
        private readonly IBeerReviewService _reviewService;
        private readonly IBreweryService _breweryService;
        private readonly IStyleService _styleService;
        private readonly ICountryService _countryService;
        private readonly IUserService _userService;
        private readonly UserManager<User> _userManager;


        public BeersController(BeerOverflowContext context, IBeerService service, IBeerReviewService reviewService, IMapper mapper,
            IBreweryService breweryService, IStyleService styleService, ICountryService countryService, UserManager<User> userManager,
            IUserService userService)
        {
            this._context = context;
            this._service = service;
            this._reviewService = reviewService;
            this._breweryService = breweryService;
            this._styleService = styleService;
            this._mapper = mapper;
            this._countryService = countryService;
            this._userService = userService;
            this._userManager = userManager;
        }

        // GET: Beers
        [HttpGet("/List")]
        public async Task<IActionResult> List()
        {
            IEnumerable<BeerDTO> beers = await _service.GetAllBeersAsync();

            if (beers == null)
            {
                return NotFound();
            }

            BeerFilterList beerFilterList = new BeerFilterList
            {
                ListBeers = beers,
                FilterCriteria = null
            };

            ViewData["Countries"] = new SelectList(_countryService.GetAllCountries().OrderBy(x => x.Name), "Id", "Name");
            ViewData["Styles"] = new SelectList(_styleService.GetAllStyles().Result.OrderBy(x => x.Name), "Id", "Name");

            return View(beerFilterList);
        }

        // Post: List
        [HttpPost("/List")]
        public async Task<IActionResult> List([Bind("Country,Style")] FilterCriteria filterCriteria)
        {
            if (ModelState.IsValid)
            {
                var filteredCountry = filterCriteria.Country;
                var filteredStyle = filterCriteria.Style;
                IEnumerable<BeerDTO> beers = await _service.FilterBeersWebAsync(filteredCountry, filteredStyle);
                BeerFilterList beerFilterList = new BeerFilterList
                {
                    ListBeers = beers,
                    FilterCriteria = filterCriteria
                };

                ViewData["Countries"] = new SelectList(_countryService.GetAllCountries().OrderBy(x => x.Name), "Id", "Name");
                ViewData["Styles"] = new SelectList(_styleService.GetAllStyles().Result.OrderBy(x => x.Name), "Id", "Name");

                return View(beerFilterList);
            }

            return RedirectToAction(nameof(List));
        }

        // GET: Beers/Details/5
        [HttpGet("/Details/{id}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beer = await _service.GetBeerByIdAsync(id.Value);
            if (beer == null)
            {
                return NotFound();
            }

            beer.BreweryName = _breweryService.GetBreweryAsync(beer.BreweryId).Result.Name;
            beer.StyleName = _styleService.GetStyleAsync(beer.StyleId).Result.Name;
            var beerViewModel = (BeerViewModel)_mapper.Map(beer, typeof(BeerDTO), typeof(BeerViewModel));
            
            beerViewModel.Rating = _service.GetBeerRatingAsync(id.Value).Result;

            var beerReviewsDTO = _reviewService.GetBeerReviewsAsync(id.Value).Result;
            beerViewModel.BeerReviews = new List<BeerReview>();

            if (beerReviewsDTO != null)
            {
                foreach (var review in beerReviewsDTO)
                {
                    BeerReview beerReview = new BeerReview() { BeerId = review.BeerId, UserId = review.UserId, Id = review.Id, Review = review.Review, Likes = review.Likes };
                    beerViewModel.BeerReviews.Add(beerReview);
                }
            }

            if (TempData["Success"] != null)
            {
                beerViewModel.ShowSuccessMessage = (bool)TempData["Success"];
            }

            return View(beerViewModel);
        }

        // GET: Beers/Create
        [HttpGet("/Create")]
        [Authorize(Roles = "Admin, User")]
        public IActionResult Create()
        {
            ViewData["BreweryId"] = new SelectList(_breweryService.GetAllBreweries().Result, "Id", "Name");
            ViewData["StyleId"] = new SelectList(_styleService.GetAllStyles().Result, "Id", "Name");
            return View();
        }

        // POST: Beers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("/Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,ABV,BreweryId,Description,StyleId")] BeerDTO beer)
        {
            if (ModelState.IsValid)
            {
                BeerDTO resultBeerDTO = await _service.CreateBeerAsync(beer);
                return RedirectToAction(nameof(List));
            }
            return View();
        }

        //GET: Beers/Edit/5
        [HttpGet("/Edit")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beer = await _service.GetBeerByIdAsync(id.Value);
            if (beer == null)
            {
                return NotFound();
            }
            
            ViewData["BreweryId"] = new SelectList(_breweryService.GetAllBreweries().Result, "Id", "Name", beer.BreweryId);
            ViewData["StyleId"] = new SelectList(_styleService.GetAllStyles().Result, "Id", "Name", beer.StyleId);
            return View(beer);
        }

        //POST: Beers/Edit/5
        [HttpPost("/Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,ABV,BreweryId,Description,StyleId")] BeerDTO beer)
        {
            if (id != beer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    BeerDTO resultBeerDTO = await _service.UpdateBeerAsync(beer.Id, beer);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BeerExists(beer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(List));
            }

            return RedirectToAction(nameof(List));
        }

        // GET: Beers/Delete/5
        [HttpGet("/Delete/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beer = await _service.GetBeerByIdAsync(id.Value);

            if (beer == null)
            {
                return NotFound();
            }

            return View(beer);
        }

        // POST: Beers/Delete/5
        [HttpPost("/DeleteConfirmed"), ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var beer = await _service.DeleteBeerAsync(id);
            return RedirectToAction(nameof(List));
        }

        private bool BeerExists(int id)
        {
            if(_service.GetBeerByIdAsync(id) != null)
            {
                return true;
            }

            return false;
        }

        // GET: Beers/Unlist/5
        [HttpGet("/Unlist/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Unlist(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beer = await _service.GetBeerByIdAsync(id.Value);

            if (beer == null)
            {
                return NotFound();
            }

            return View(beer);
        }

        // POST: Beers/Unlist/5
        [HttpPost("/UnlistConfirmed"), ActionName("UnlistConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UnlistConfirmed(int id)
        {
            var beer = await _service.UnlistBeerAsync(id);
            return RedirectToAction(nameof(List));
        }

        //POST: Beers/RateBeer
        [HttpPost("/RateBeer"), ActionName("RateBeer")]
        [Authorize(Roles = "Admin, User")]
        public async Task<IActionResult> RateBeer(int beerId, int rating)
        {
            var currentUser = _userManager.GetUserAsync(User).Result;

            var beerRatingDTO = new BeerRatingDTO
            {
                UserId = currentUser.Id,
                BeerId = beerId,
                Rating = rating
            };

            var ratingDTO = await _service.RateBeerAsync(beerRatingDTO);

            if (ratingDTO != null)
            {
                TempData["Success"] = true;
            }

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        // POST: BeerReviews/Create
        [HttpPost("/CreateReview")]
        [Authorize(Roles = "Admin, User")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateReview(int beerId, string beerReview)
        {
            var currentUser = _userManager.GetUserAsync(User).Result;
            var beerReviewDTO = new BeerReviewDTO
            {
                UserId =  currentUser.Id,
                BeerId = beerId,
                Review = beerReview
            };

            BeerReviewDTO beerReviewResult = await _reviewService.CreateBeerReviewAsync(beerReviewDTO);

            if (beerReviewResult != null)
            {
                TempData["Success"] = true;
            }

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        //GET: Beers/Edit/5
        [HttpGet("/EditReview"), ActionName("EditReview")]
        [Authorize(Roles = "Admin, User")]
        public async Task<IActionResult> EditReview(int id)
        {
            var beerReviewDTO = await _reviewService.GetBeerReviewByIdAsync(id);
            if (beerReviewDTO == null)
            {
                return NotFound();
            }

            var beerReviewViewModel = (BeerReviewViewModel)_mapper.Map(beerReviewDTO, typeof(BeerReviewDTO), typeof(BeerReviewViewModel));
            beerReviewViewModel.BeerName = _service.GetBeerByIdAsync(beerReviewDTO.BeerId).Result.Name;
            beerReviewViewModel.UserName = _userService.GetUserById(beerReviewDTO.UserId).Result.UserName;
            
            return View(beerReviewViewModel);
        }

        //POST: Beers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("/EditReview"), ActionName("EditReview")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditReview(int id, int beerId, string review)
        {
            var beerReviewDTO = new BeerReviewDTO
            {
                Id = id,
                Review = review
            };

            BeerReviewDTO beerReviewResult = await _reviewService.UpdateBeerReviewAsync(id, beerReviewDTO);

            if (beerReviewResult != null)
            {
                TempData["Success"] = true;
            }

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        // GET: Beers/DeleteReview/5
        [HttpGet("/DeleteReview/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteReview(int id)
        {
            var beerReview = await _reviewService.GetBeerReviewByIdAsync(id);
            var beerReviewViewModel = _mapper.Map(beerReview, typeof(BeerReviewDTO), typeof(BeerReviewViewModel));

            return View(beerReviewViewModel);
        }

        // POST: Beers/DeleteReview/5
        [HttpPost("/DeleteReviewConfirmed"), ActionName("DeleteReviewConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteReviewConfirmed(int id, int beerId)
        {
            var result = await _reviewService.DeleteBeerReviewAsync(id);

            if (result == true)
            {
                TempData["Success"] = true;
            }

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        // GET: Beers/FlagReview/5
        [HttpGet("/FlagReview/{id}")]
        public async Task<IActionResult> FlagReview(int id)
        {
            var beerReview = await _reviewService.GetBeerReviewByIdAsync(id);
            var beerReviewViewModel = _mapper.Map(beerReview, typeof(BeerReviewDTO), typeof(BeerReviewViewModel));

            return View(beerReviewViewModel);
        }

        // POST: Beers/FlagReview/5
        [HttpPost("/FlagReview"), ActionName("FlagReviewConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> FlagReviewConfirmed(int id, int beerId)
        {
            var result = await _reviewService.FlagBeerReviewAsync(id);

            if (result == true)
            {
                TempData["Success"] = true;
            }

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        [HttpGet("/LikeReview/{id}")]
        [Authorize(Roles = "Admin, User")]
        public async Task<IActionResult> LikeReview(int id)
        {
            var beerReview = await _reviewService.GetBeerReviewByIdAsync(id);
            var beerReviewViewModel = _mapper.Map(beerReview, typeof(BeerReviewDTO), typeof(BeerReviewViewModel));

            return View(beerReviewViewModel);
        }

        [HttpPost("/LikeReview"), ActionName("LikeReviewConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LikeReviewConfirmed(int id, int? userId, int? beerId)
        {
            var result = await _reviewService.LikeBeerReviewAsync(beerId, userId);

            if (result == true)
            {
                TempData["Success"] = true;
            }
            return RedirectToAction(nameof(Details), new { id = beerId });
        }

        [HttpGet("/DislikeReview/{id}")]
        [Authorize(Roles = "Admin, User")]
        public async Task<IActionResult> DislikeReview(int id)
        {
            var beerReview = await _reviewService.GetBeerReviewByIdAsync(id);
            var beerReviewViewModel = _mapper.Map(beerReview, typeof(BeerReviewDTO), typeof(BeerReviewViewModel));

            return View(beerReviewViewModel);
        }

        [HttpPost("/DislikeReview"), ActionName("DislikeReviewConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DislikeReviewConfirmed(int id, int? userId, int? beerId)
        {
            var result = await _reviewService.DislikeBeerReviewAsync(beerId, userId);

            if (result == true)
            {
                TempData["Success"] = true;
            }

            return RedirectToAction(nameof(Details), new { id = beerId });
        }

    }
}
