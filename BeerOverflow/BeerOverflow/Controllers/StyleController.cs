﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Controllers
{
    public class StyleController : Controller
    {
        private readonly IStyleService _service;

        public StyleController(IStyleService service)
        {
            this._service = service;
        }

        public async Task<IActionResult> Index()
        {
            var list = await _service.GetAllStyles();

            return View(list);
        }

        public async Task<IActionResult> Details(int id)
        {
            var style = await _service.GetStyleAsync(id);

            return View(style);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(StyleDTO styleDTO)
        {
            var style = await _service.CreateStyleAsync(styleDTO);

            return Redirect("Index");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var style = await _service.GetStyleAsync((int)id);

            return View(style);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Id,Name")] StyleDTO styleDTO)
        {
            var style = await _service.UpdateStyleAsync(styleDTO);

            return View("Details", style);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var dto = await _service.GetStyleAsync((int)id);

            return View(dto);
        }
        
        [HttpPost]
        public async Task<IActionResult> Delete([Bind("Id")] StyleDTO styleDTO)
        {
            await _service.DeleteStyleAsync((int)styleDTO.Id);

            return RedirectToAction("Index");
        }
    }
}