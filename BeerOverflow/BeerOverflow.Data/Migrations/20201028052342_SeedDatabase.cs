﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BeerOverflow.Data.Migrations
{
    public partial class SeedDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 2, "aafc79cb-2e8f-4917-985f-0c72d8a5bb6a", "User", "USER" },
                    { 1, "4b1c14c3-d912-4725-81c0-2c5f99ec640c", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "BanExpirationDate", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "IsBanned", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 34, 0, null, "f5e3c26d-b408-4e77-9a2e-9d96a5e2a991", "g.georgiev@gmail.com", false, "Georgi", false, "Georgiev", false, null, "G.GEORGIEV@GMAIL.COM", "HELLYZ", "AQAAAAEAACcQAAAAEJvcCrx87Kp1L7JChNQfIoyEvY+p/CZcp7R/Mk2QMQ2jD3CEoYZ7agWpSmpt7S46bA==", null, false, "2002725f-ae44-4346-91b1-0c42f1ba6d2a", false, "Hellyz" },
                    { 33, 0, null, "67cdc682-c811-44ce-9e4b-7941bd8271ef", "kris.dimitrow99@abv.bg", false, "Kristian", false, "Dimitrov", false, null, "KRIS.DIMITROW99@ABV.BG", "KRISKO", "AQAAAAEAACcQAAAAECuW3WM4FBln1g+SKR7EHC/zWy+SW5xjz2N/yT4h4He40Tc580qCqW+9ymxsOufIRA==", null, false, "ac432612-1a04-4f00-b82d-2067ec5bd94e", false, "Krisko" }
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 22, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2643), null, false, null, "India" },
                    { 23, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2644), null, false, null, "Hungary" },
                    { 24, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2646), null, false, null, "French Polynesia" },
                    { 25, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2647), null, false, null, "Aruba" },
                    { 26, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2649), null, false, null, "Myanmar" },
                    { 27, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2650), null, false, null, "Macedonia, the Former Yugoslav Republic of" },
                    { 29, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2653), null, false, null, "Thailand" },
                    { 21, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2642), null, false, null, "Brazil" },
                    { 30, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2655), null, false, null, "Portugal" },
                    { 31, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2656), null, false, null, "Lithuania" },
                    { 32, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2658), null, false, null, "England" },
                    { 33, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2659), null, false, null, "Sweden" },
                    { 28, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2652), null, false, null, "Russia" },
                    { 20, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2640), null, false, null, "Norway" },
                    { 1, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(1190), null, false, null, "Australia" },
                    { 18, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2637), null, false, null, "Korea, Republic of" },
                    { 2, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2557), null, false, null, "Spain" },
                    { 19, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2639), null, false, null, "Switzerland" },
                    { 3, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2583), null, false, null, "United States" },
                    { 4, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2585), null, false, null, "United Kingdom" },
                    { 6, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2593), null, false, null, "Japan" },
                    { 7, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2594), null, false, null, "Netherlands" },
                    { 8, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2596), null, false, null, "Belgium" },
                    { 9, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2597), null, false, null, "Czech Republic" },
                    { 5, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2587), null, false, null, "Austria" },
                    { 11, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2625), null, false, null, "El Salvador" },
                    { 12, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2627), null, false, null, "Germany" },
                    { 13, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2628), null, false, null, "Macao" },
                    { 14, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2630), null, false, null, "New Zealand" },
                    { 15, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2631), null, false, null, "Canada" },
                    { 17, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2634), null, false, null, "Mexico" },
                    { 10, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2624), null, false, null, "Panama" },
                    { 16, new DateTime(2020, 10, 28, 5, 23, 41, 390, DateTimeKind.Utc).AddTicks(2633), null, false, null, "Ireland" }
                });

            migrationBuilder.InsertData(
                table: "Styles",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 36, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9029), null, false, null, "Pumpkin Beer" },
                    { 35, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9028), null, false, null, "Belgian-Style Pale Ale" },
                    { 34, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9026), null, false, null, "German-Style Oktoberfest" },
                    { 31, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9021), null, false, null, "Irish-Style Red Ale" },
                    { 32, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9022), null, false, null, "American-Style Dark Lager" },
                    { 30, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9020), null, false, null, "French & Belgian-Style Saison" },
                    { 29, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9018), null, false, null, "Oatmeal Stout" },
                    { 37, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9030), null, false, null, "German-Style Heller Bock/Maibock" },
                    { 33, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9024), null, false, null, "German-Style Brown Ale/Altbier" },
                    { 38, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9032), null, false, null, "Belgian-Style Dubbel" },
                    { 45, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9041), null, false, null, "Out of Category" },
                    { 40, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9034), null, false, null, "Sweet Stout" },
                    { 41, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9036), null, false, null, "American-Style India Pale Ale" },
                    { 42, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9037), null, false, null, "American-Style Cream Ale or Lager" },
                    { 43, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9038), null, false, null, "Scotch Ale" },
                    { 44, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9040), null, false, null, "Traditional German-Style Bock" },
                    { 28, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9017), null, false, null, "Classic Irish-Style Dry Stout" },
                    { 46, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9042), null, false, null, "American-Style Barley Wine Ale" },
                    { 47, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9044), null, false, null, "Winter Warmer" },
                    { 48, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9045), null, false, null, "Old Ale" },
                    { 49, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9046), null, false, null, "Kellerbier - Ale" },
                    { 39, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9033), null, false, null, "American Rye Ale or Lager" },
                    { 27, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9015), null, false, null, "Extra Special Bitter" },
                    { 18, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9003), null, false, null, "German-Style Pilsener" },
                    { 25, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9013), null, false, null, "Light American Wheat Ale or Lager" },
                    { 1, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(7683), null, false, null, "American-Style Light Lager" },
                    { 2, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8947), null, false, null, "Baltic-Style Porter" },
                    { 3, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8973), null, false, null, "American-Style Stout" },
                    { 4, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8974), null, false, null, "American-Style Brown Ale" },
                    { 5, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8976), null, false, null, "English-Style Pale Mild Ale" },
                    { 6, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8984), null, false, null, "English-Style Dark Mild Ale" },
                    { 7, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8986), null, false, null, "Other Belgian-Style Ales" },
                    { 9, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8989), null, false, null, "Golden or Blonde Ale" },
                    { 10, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8991), null, false, null, "American-Style Lager" },
                    { 11, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8993), null, false, null, "Dark American-Belgo-Style Ale" },
                    { 26, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9014), null, false, null, "Classic English-Style Pale Ale" },
                    { 12, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8994), null, false, null, "English-Style India Pale Ale" },
                    { 14, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8997), null, false, null, "American-Style Pale Ale" },
                    { 15, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8998), null, false, null, "Porter" },
                    { 16, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8999), null, false, null, "Ordinary Bitter" },
                    { 17, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9001), null, false, null, "Scottish-Style Light Ale" },
                    { 19, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9005), null, false, null, "German-Style Schwarzbier" },
                    { 20, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9006), null, false, null, "Belgian-Style White" },
                    { 21, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9007), null, false, null, "American-Style Amber/Red Ale" },
                    { 22, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9009), null, false, null, "Herb and Spice Beer" },
                    { 23, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9010), null, false, null, "South German-Style Hefeweizen" },
                    { 24, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(9011), null, false, null, "Belgian-Style Fruit Lambic" },
                    { 13, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8995), null, false, null, "Special Bitter or Best Bitter" },
                    { 8, new DateTime(2020, 10, 28, 5, 23, 41, 392, DateTimeKind.Utc).AddTicks(8987), null, false, null, "Fruit Beer" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 34, 2 },
                    { 33, 1 }
                });

            migrationBuilder.InsertData(
                table: "Breweries",
                columns: new[] { "Id", "CountryId", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 381, 32, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1612), null, false, null, "Youngs & Company Brewery" },
                    { 336, 4, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(363), null, false, null, "Williams Brothers Brewing Company" },
                    { 320, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9900), null, false, null, "Samuel Smith Old Brewery (Tadcaster)" },
                    { 301, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9288), null, false, null, "Hampshire Brewery" },
                    { 300, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9272), null, false, null, "Hambleton Ales" },
                    { 298, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9241), null, false, null, "Greene King" },
                    { 270, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8677), null, false, null, "Bass Brewers" },
                    { 259, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8498), null, false, null, "Whitbread Beer Company" },
                    { 191, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7413), null, false, null, "Scottish & Newcastle Breweries" },
                    { 159, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6939), null, false, null, "St Peter's Brewery" },
                    { 154, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6868), null, false, null, "RCH Brewery" },
                    { 138, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6635), null, false, null, "Hop Back Brewery" },
                    { 135, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6589), null, false, null, "Fuller, Smith & Turner PBC" },
                    { 83, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5722), null, false, null, "St. Austell Brewery" },
                    { 65, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5483), null, false, null, "BrewDog Ltd" },
                    { 53, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5332), null, false, null, "Orkney Brewery" },
                    { 37, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5077), null, false, null, "Belhaven Brewery" },
                    { 14, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4688), null, false, null, "Moorhouse's Brewery (Burnley)" },
                    { 8, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4609), null, false, null, "Cains" },
                    { 5, 4, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4571), null, false, null, "Adnams & Co." },
                    { 414, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3272), null, false, null, "Duck-Rabbit Craft Brewery" },
                    { 413, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3213), null, false, null, "Cigar City Brewing" },
                    { 337, 4, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(379), null, false, null, "Wychwood Brewery Company Ltd" },
                    { 343, 4, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(569), null, false, null, "Jennings Brewery" },
                    { 370, 4, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1318), null, false, null, "Morland and Co." },
                    { 375, 4, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1445), null, false, null, "Ridgeway Brewing" },
                    { 283, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8898), null, false, null, "Brouwerij der Sint-Benedictusabdij de Achelse Kluis" },
                    { 279, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8834), null, false, null, "Brasserie-Brouwerij Cantillon" },
                    { 276, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8778), null, false, null, "Brasserie de Brunehaut" },
                    { 263, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8554), null, false, null, "3 Fonteinen Brouwerij Ambachtelijke Geuzestekerij" },
                    { 242, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8202), null, false, null, "Brouwerij van Hoegaarden" },
                    { 205, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7635), null, false, null, "Chimay (Abbaye Notre Dame de Scourmont)" },
                    { 125, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6336), null, false, null, "Brouwerij Van Honsebrouck" },
                    { 124, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6322), null, false, null, "Brouwerij De Keersmaeker" },
                    { 17, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4730), null, false, null, "Brasserie d'Orval" },
                    { 303, 7, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9340), null, false, null, "Heineken International" },
                    { 408, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3103), null, false, null, "Brooklyn Brewery" },
                    { 299, 7, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9257), null, false, null, "Grolsche Bierbrouwerij" },
                    { 16, 7, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4716), null, false, null, "Amstel Brouwerij" },
                    { 338, 6, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(426), null, false, null, "Uehara Shuzou Co. Ltd. / Echigo Beer Pub" },
                    { 311, 6, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9649), null, false, null, "Nikenjayamochi Kadoya Honten Co." },
                    { 23, 6, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4846), null, false, null, "Kiuchi Shuzou Goushi Kaisya" },
                    { 13, 6, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4674), null, false, null, "Kirin Brewery Company, Ltd" },
                    { 323, 5, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9962), null, false, null, "Schlossbru" },
                    { 11, 5, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4648), null, false, null, "Stieglbrauerei zu Salzburg GmbH" },
                    { 384, 4, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1929), null, false, null, "Daleside Brewery" },
                    { 383, 4, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1898), null, false, null, "Bulmer Cider" },
                    { 380, 4, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1561), null, false, null, "Wells and (&) Youngs Brewing Company Ltd." },
                    { 272, 7, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8706), null, false, null, "Bierbrouwerij De Koningshoeven" },
                    { 407, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3084), null, false, null, "BridgePort Brewing" },
                    { 406, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2887), null, false, null, "Bonaventure Brewing Co" },
                    { 405, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2872), null, false, null, "Big Boss Brewing Company" },
                    { 351, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(781), null, false, null, "Surly Brewing" },
                    { 350, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(758), null, false, null, "Southampton Publick House" },
                    { 349, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(738), null, false, null, "Sleeping Lady Brewing Company" },
                    { 348, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(713), null, false, null, "Saint Louis Brewery / Schlafy Tap Room" },
                    { 347, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(685), null, false, null, "Rivertowne Pour House" },
                    { 346, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(671), null, false, null, "Real Ale Brewing Company" },
                    { 345, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(649), null, false, null, "Pyramid Ales Brewery" },
                    { 344, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(622), null, false, null, "Pyramid Alehouse, Brewery and Restaurant - Berkeley" },
                    { 342, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(547), null, false, null, "Hopworks Urban Brewery" },
                    { 341, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(526), null, false, null, "Full Sail Brewing #1" },
                    { 352, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(815), null, false, null, "Two Brothers Brewing" },
                    { 340, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(492), null, false, null, "Double Mountain Brewery & Taproom" },
                    { 330, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(164), null, false, null, "The Livery" },
                    { 328, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(86), null, false, null, "Stone City Brewing" },
                    { 325, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2), null, false, null, "Seabright Brewery" },
                    { 321, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9934), null, false, null, "San Diego Brewing" },
                    { 319, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9873), null, false, null, "Rock Bottom Restaurant & Brewery - Denver" },
                    { 318, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9846), null, false, null, "Port Brewing Company" },
                    { 314, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9738), null, false, null, "Pennichuck Brewing Company" },
                    { 313, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9702), null, false, null, "Oskar Blues Grill and Brew" },
                    { 312, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9683), null, false, null, "On Tap Bistro & Brewery" },
                    { 308, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9546), null, false, null, "Mogollon Brewing Company" },
                    { 332, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(238), null, false, null, "Tyranena Brewing" },
                    { 284, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8914), null, false, null, "Brouwerij Girardin" },
                    { 353, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(839), null, false, null, "(512) Brewing Company" },
                    { 359, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1031), null, false, null, "Bube's Brewery" },
                    { 402, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2728), null, false, null, "William Kuether Brewing" },
                    { 401, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2653), null, false, null, "Ska Brewing Company" },
                    { 399, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2552), null, false, null, "Magnolia Pub and Brewery" },
                    { 394, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2302), null, false, null, "Columbus Brewing Company" },
                    { 392, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2246), null, false, null, "The Cambridge House" },
                    { 390, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2199), null, false, null, "Saint Arnold Brewing" },
                    { 389, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2173), null, false, null, "Rockyard Brewing" },
                    { 386, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1988), null, false, null, "Elm City Brewing Co" },
                    { 385, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1954), null, false, null, "Elk Grove Brewery & Restaurant" },
                    { 382, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1654), null, false, null, "JoBoy's Brew Pub" },
                    { 355, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(929), null, false, null, "Avery Brewing Company" },
                    { 378, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1514), null, false, null, "Thomas Hooker Brewing" },
                    { 376, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1465), null, false, null, "Santa Fe Brewing Company" },
                    { 374, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1424), null, false, null, "Richbrau Brewing Company" },
                    { 372, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1349), null, false, null, "Old Dominion Brewing Co." },
                    { 371, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1333), null, false, null, "New Holland Brewing Company" },
                    { 369, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1303), null, false, null, "Minhas Craft Brewery" },
                    { 364, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1145), null, false, null, "Founders Brewing" },
                    { 363, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1131), null, false, null, "Flat Earth Brewing Company" },
                    { 362, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1117), null, false, null, "Flat Branch Pub & Brewing" },
                    { 361, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1067), null, false, null, "Costal Extreme Brewing Company" },
                    { 360, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1044), null, false, null, "Capital City Brewing Company" },
                    { 377, 3, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1480), null, false, null, "Sixpoint Craft Ales" },
                    { 286, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8952), null, false, null, "Brouwerij Van Steenberge" },
                    { 292, 8, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9044), null, false, null, "Daas" },
                    { 307, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9462), null, false, null, "Metropolitan Brewing" },
                    { 84, 16, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5741), null, false, null, "Strangford Lough Brewing Company Ltd" },
                    { 64, 16, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5472), null, false, null, "Beamish & Crawford" },
                    { 387, 15, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2050), null, false, null, "Gold Crown Brewing" },
                    { 333, 15, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(264), null, false, null, "Unibroue" },
                    { 327, 15, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(72), null, false, null, "Steam Whistle Brewery" },
                    { 309, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9593), null, false, null, "Moosehead Breweries" },
                    { 290, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9002), null, false, null, "Cameron's Brewing" },
                    { 278, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8818), null, false, null, "Brasserie McAuslan" },
                    { 273, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8723), null, false, null, "Big Rock Brewery" },
                    { 264, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8574), null, false, null, "Agassiz Brewing" },
                    { 86, 16, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5785), null, false, null, "Arthur Guinness & Son" },
                    { 221, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7870), null, false, null, "Mission Springs Brewing" },
                    { 91, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5873), null, false, null, "Dockside Brewing" },
                    { 67, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5535), null, false, null, "Molson Breweries of Canada" },
                    { 50, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5285), null, false, null, "Labatt Ontario Breweries" },
                    { 397, 14, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2472), null, false, null, "Epic Brewing Company" },
                    { 107, 14, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6080), null, false, null, "Monteith's Brewing Co." },
                    { 58, 14, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5395), null, false, null, "Tui Brewery" },
                    { 52, 14, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5321), null, false, null, "New Zealand Breweries Limited" },
                    { 45, 14, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5216), null, false, null, "Dux de Lux" },
                    { 43, 13, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5188), null, false, null, "Cervejaria de Macau" },
                    { 415, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3292), null, false, null, "Eder & Heylands" },
                    { 185, 15, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7329), null, false, null, "Granville Island Brewing Company" },
                    { 411, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3161), null, false, null, "Bürgerbräu Wolnzach" },
                    { 88, 16, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5816), null, false, null, "Carlow Brewing Company" },
                    { 66, 17, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5515), null, false, null, "Grupo Modelo" },
                    { 379, 31, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1546), null, false, null, "vyturio Alaus Darykla" },
                    { 334, 30, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(293), null, false, null, "Unicer" },
                    { 331, 29, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(214), null, false, null, "Tropical Beverage (International) Co., Ltd" },
                    { 317, 28, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9832), null, false, null, "Pivzavod Baltika /" },
                    { 315, 27, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9784), null, false, null, "Pivara Skopje" },
                    { 310, 26, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9633), null, false, null, "Myanmar Brewery and Distillery" },
                    { 285, 25, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8938), null, false, null, "Brouwerij Nacional Balashi" },
                    { 277, 24, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8801), null, false, null, "Brasserie de Tahiti" },
                    { 302, 23, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9323), null, false, null, "Heineken Hungária" },
                    { 294, 23, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9097), null, false, null, "Dreher Srgyrak Zrt." },
                    { 160, 16, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6958), null, false, null, "St. Francis Abbey Brewery" },
                    { 275, 23, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8764), null, false, null, "Borsodi Sörgyár" },
                    { 236, 22, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8092), null, false, null, "United Breweries Limited" },
                    { 182, 21, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7284), null, false, null, "Cervejaria Kaiser Brasil" },
                    { 403, 20, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2779), null, false, null, "Aass Brewery" },
                    { 148, 20, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6777), null, false, null, "Nøgne Ø - Det Kompromissløse Bryggeri A/S" },
                    { 335, 19, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(331), null, false, null, "Wdi-Bru-Huus" },
                    { 123, 19, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6308), null, false, null, "Brauhaus Sternen" },
                    { 137, 18, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6625), null, false, null, "Hite Brewery" },
                    { 109, 18, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6117), null, false, null, "Oriental Brewery" },
                    { 128, 17, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6389), null, false, null, "Cucapa Brewing Company" },
                    { 126, 17, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6356), null, false, null, "Cervecera Cuauhtmoc-Moctezuma" },
                    { 255, 22, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8415), null, false, null, "Sabmiller India," },
                    { 412, 33, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3199), null, false, null, "Carlsberg Sverige AB" },
                    { 404, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2815), null, false, null, "Allguer Brauhaus AG Kempten" },
                    { 398, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2537), null, false, null, "Klosterbrauerei Weltenburg" },
                    { 229, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7991), null, false, null, "Radeberger Exportbierbrauerei" },
                    { 227, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7964), null, false, null, "Privatbrauerei Frankenheim" },
                    { 226, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7940), null, false, null, "Privat-Brauerei Schmucker Ober-Mossau KG" },
                    { 219, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7836), null, false, null, "Köstritzer Schwarzbierbrauerei" },
                    { 218, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7819), null, false, null, "Keesmann Bru" },
                    { 212, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7734), null, false, null, "Flensburger Brauerei" },
                    { 209, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7693), null, false, null, "Dortmunder Actien Brauerei  DAB" },
                    { 202, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7598), null, false, null, "Bitburger Brauerei" },
                    { 169, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7083), null, false, null, "Brauerei Schumacher" },
                    { 162, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6992), null, false, null, "Uerige Obergrige Hausbrauerei" },
                    { 238, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8122), null, false, null, "Warsteiner Brauerei" },
                    { 120, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6265), null, false, null, "Beck's" },
                    { 38, 11, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5092), null, false, null, "Cervecería La Constancia" },
                    { 32, 10, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4995), null, false, null, "Cervecera Nacional" },
                    { 316, 9, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9802), null, false, null, "Pivovary Staropramen" },
                    { 282, 9, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8883), null, false, null, "Brewery Budweiser Budvar" },
                    { 110, 9, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6134), null, false, null, "Plzesk Prazdroj" },
                    { 30, 9, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4954), null, false, null, "Breznak" },
                    { 410, 8, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3139), null, false, null, "Brouwerij St. Bernardus" },
                    { 409, 8, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(3121), null, false, null, "Brouwerij Bavik - De Brabandere" },
                    { 373, 8, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1392), null, false, null, "Palm Breweries" },
                    { 366, 8, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1216), null, false, null, "InBev" },
                    { 39, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5117), null, false, null, "Klosterbrauerei Neuzelle" },
                    { 400, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2601), null, false, null, "Private Weissbierbrauerei G. Schneider & Sohn GmbH" },
                    { 240, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8162), null, false, null, "Bamberger Mahr's-Bru" },
                    { 244, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8232), null, false, null, "Diebels Privatbrauerei" },
                    { 395, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2365), null, false, null, "Bayerische Staatsbrauerei Weihenstephan" },
                    { 393, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2277), null, false, null, "Tucher Bru" },
                    { 391, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2221), null, false, null, "Schussenrieder Erlebnisbrauerei" },
                    { 388, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2146), null, false, null, "Privatbrauerei Erdinger Weissbru" },
                    { 368, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1276), null, false, null, "Martini-Brauerei" },
                    { 367, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1246), null, false, null, "Maisel Bru" },
                    { 365, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1194), null, false, null, "Hirschbru Privatbrauerei Hss" },
                    { 357, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(992), null, false, null, "Brauerei Schwelm" },
                    { 356, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(972), null, false, null, "Brauerei Leibinger" },
                    { 339, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(474), null, false, null, "Brauerei Gbr. Maisel KG" },
                    { 241, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8178), null, false, null, "Barfer - das kleine Brauhaus" },
                    { 329, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(117), null, false, null, "Stuttgarter Hofbru" },
                    { 324, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9987), null, false, null, "Schöfferhofer" },
                    { 281, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8867), null, false, null, "Brauhaus am Waldschlösschen" },
                    { 280, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8850), null, false, null, "Brauerei Fssla" },
                    { 258, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8487), null, false, null, "Wernesgrüner Brauerei GmbH" },
                    { 257, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8468), null, false, null, "Wernesgrüner Brauerei" },
                    { 253, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8380), null, false, null, "Privatbrauerei Bolten" },
                    { 250, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8331), null, false, null, "Kulmbacher Brauerei AG" },
                    { 249, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8315), null, false, null, "Jever Brewery" },
                    { 246, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8266), null, false, null, "Einbecker Brauhaus AG" },
                    { 245, 12, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8249), null, false, null, "Dinkelacker-Schwaben Bru" },
                    { 326, 12, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(38), null, false, null, "Spaten-Franziskaner-Bräu" },
                    { 358, 8, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(1009), null, false, null, "Brouwerij Artois" },
                    { 306, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9437), null, false, null, "Lost Coast Brewery" },
                    { 304, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9353), null, false, null, "Hereford & Hops Steakhouse and Brewpub #3" },
                    { 98, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5956), null, false, null, "Jolly Pumpkin Artisan Ales" },
                    { 97, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5938), null, false, null, "Hoppin Frog Brewery" },
                    { 96, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5927), null, false, null, "Harpoon Brewery - Boston" },
                    { 95, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5917), null, false, null, "Great Bear Brewing" },
                    { 94, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5905), null, false, null, "Gordon Biersch Brewing" },
                    { 93, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5893), null, false, null, "F.X. Matt Brewing" },
                    { 92, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5883), null, false, null, "Engine House #9" },
                    { 90, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5844), null, false, null, "Diamond Knot Brewery & Alehouse" },
                    { 89, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5826), null, false, null, "Cooperstown Brewing Company" },
                    { 87, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5797), null, false, null, "Cambridge Brewing" },
                    { 99, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5968), null, false, null, "Maritime Pacific Brewing" },
                    { 85, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5760), null, false, null, "Walldorff Brew Pub" },
                    { 81, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5701), null, false, null, "South Shore Brewery" },
                    { 80, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5690), null, false, null, "Raccoon Lodge and Brewpub / Cascade Brewing" },
                    { 79, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5679), null, false, null, "New Glarus Brewing Company" },
                    { 78, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5669), null, false, null, "New Belgium Brewing" },
                    { 77, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5659), null, false, null, "Moose's Tooth Pub and Pizzeria" },
                    { 76, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5646), null, false, null, "Jacob Leinenkugel Brewing Company" },
                    { 75, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5636), null, false, null, "High Falls Brewing" },
                    { 74, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5622), null, false, null, "Estes Park Brewery" },
                    { 73, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5610), null, false, null, "Crabby Larry's Brewpub Steak & Crab House" },
                    { 72, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5598), null, false, null, "Coors Brewing - Golden Brewery" },
                    { 82, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5711), null, false, null, "Sprecher Brewing" },
                    { 71, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5579), null, false, null, "Iron City Brewing Co." },
                    { 100, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5980), null, false, null, "Scuttlebutt Brewing" },
                    { 102, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6003), null, false, null, "Yards Brewing" },
                    { 133, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6558), null, false, null, "East End Brewing Company" },
                    { 132, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6548), null, false, null, "Dragonmead Microbrewery" },
                    { 131, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6538), null, false, null, "Deschutes Brewery" },
                    { 130, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6526), null, false, null, "Dark Horse Brewing Co." },
                    { 129, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6400), null, false, null, "D.L. Geary Brewing Company" },
                    { 127, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6370), null, false, null, "Cricket Hill" },
                    { 122, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6285), null, false, null, "Blue Ridge Brewing" },
                    { 121, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6275), null, false, null, "Blue Point Brewing" },
                    { 119, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6248), null, false, null, "Bavarian Barbarian Brewing Company" },
                    { 118, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6237), null, false, null, "Barrel House Brewing Co." },
                    { 101, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5991), null, false, null, "Twin Rivers Brewing" },
                    { 117, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6227), null, false, null, "Atwater Block Brewing" },
                    { 115, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6200), null, false, null, "The St. Louis Brewrey" },
                    { 114, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6178), null, false, null, "Troegs Brewing" },
                    { 113, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6168), null, false, null, "Stone Brewing Co." },
                    { 112, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6156), null, false, null, "Spoetzl Brewery" },
                    { 111, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6145), null, false, null, "Sierra Nevada Brewing Co." },
                    { 108, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6093), null, false, null, "North Coast Brewing Company" },
                    { 106, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6057), null, false, null, "Granite City Food & Brewery - Omaha" },
                    { 105, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6045), null, false, null, "Fitger's Brewhouse, Brewery and Grill" },
                    { 104, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6034), null, false, null, "Dick's Brewing" },
                    { 103, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6016), null, false, null, "Boulevard Brewing Company" },
                    { 116, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6215), null, false, null, "Appalachian Brewing Company" },
                    { 134, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6569), null, false, null, "Elliott Bay Brewery and Pub" },
                    { 70, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5568), null, false, null, "White Marsh Brewing Company" },
                    { 68, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5547), null, false, null, "Sly Fox Brewhouse and Eatery - Royersford" },
                    { 26, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4902), null, false, null, "Yazoo Brewing" },
                    { 25, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4883), null, false, null, "Sea Dog Brewing Company" },
                    { 24, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4864), null, false, null, "McKenzie Brew House" },
                    { 22, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4824), null, false, null, "Jack's Brewing" },
                    { 21, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4809), null, false, null, "Gottberg Brew Pub" },
                    { 20, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4772), null, false, null, "Flyers Restraunt and Brewery" },
                    { 19, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4758), null, false, null, "Coeur d'Alene Brewing Company" },
                    { 18, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4743), null, false, null, "Brewery Creek Brewing" },
                    { 15, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4700), null, false, null, "Yuengling & Son Brewing" },
                    { 12, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4659), null, false, null, "The Alchemist" },
                    { 27, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4918), null, false, null, "21st Amendment Brewery Cafe" },
                    { 10, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4635), null, false, null, "Anchor Brewing" },
                    { 7, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4597), null, false, null, "Otto's Pub and Brewery" },
                    { 6, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4586), null, false, null, "Big Buck Brewery" },
                    { 4, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4544), null, false, null, "Thirsty Dog Brewing" },
                    { 3, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4506), null, false, null, "Miller Brewing" },
                    { 396, 2, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(2410), null, false, null, "Cervezas Alhambra" },
                    { 2, 2, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4418), null, false, null, "S.A. Damm" },
                    { 354, 1, new DateTime(2020, 10, 28, 5, 23, 41, 395, DateTimeKind.Utc).AddTicks(915), null, false, null, "Australian Brewing Corporation" },
                    { 322, 1, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9946), null, false, null, "Scharer's Little Brewery" },
                    { 175, 1, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7180), null, false, null, "Tooheys" },
                    { 172, 1, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7125), null, false, null, "Little Creatures Brewery" },
                    { 9, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4623), null, false, null, "Barley Creek Brewing" },
                    { 69, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5558), null, false, null, "The Church Brew Works" },
                    { 28, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4928), null, false, null, "Barley Island Brewing" },
                    { 31, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4971), null, false, null, "Marzoni's Brick Oven & Brewing Co" },
                    { 63, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5452), null, false, null, "Anheuser-Busch" },
                    { 62, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5436), null, false, null, "Valley Brewing Company" },
                    { 61, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5426), null, false, null, "Utah Brewers Cooperative" },
                    { 60, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5416), null, false, null, "Upper Mississippi Brewing" },
                    { 59, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5405), null, false, null, "Uinta Brewing Compnay" },
                    { 57, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5377), null, false, null, "Skagit River Brewing" },
                    { 56, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5367), null, false, null, "Sacramento Brewing Company" },
                    { 55, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5356), null, false, null, "River Horse Brewing Company" },
                    { 54, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5342), null, false, null, "Penn Brewery" },
                    { 51, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5297), null, false, null, "Long Trail Brewing Co" },
                    { 29, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(4940), null, false, null, "Sullivan's Black Forest Brew Haus & Grill" },
                    { 49, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5267), null, false, null, "Four Peaks Brewing" },
                    { 47, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5240), null, false, null, "Erie Brewing Company" }
                });

            migrationBuilder.InsertData(
                table: "Breweries",
                columns: new[] { "Id", "CountryId", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 46, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5226), null, false, null, "Elysian Brewing - TangleTown" },
                    { 44, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5199), null, false, null, "Dogfish Head Craft Brewery" },
                    { 42, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5169), null, false, null, "Ballast Point Brewing" },
                    { 41, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5156), null, false, null, "The Blind Bat Brewery LLC" },
                    { 40, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5138), null, false, null, "Michigan Brewing" },
                    { 36, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5057), null, false, null, "Upstream Brewing Old Market" },
                    { 35, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5041), null, false, null, "Pleasanton Main Street Brewery" },
                    { 34, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5030), null, false, null, "Harmon Brewing Company" },
                    { 33, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5011), null, false, null, "Pabst Brewing Company" },
                    { 48, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(5250), null, false, null, "Fish Brewing Company & Fish Tail Brewpub" },
                    { 136, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6603), null, false, null, "Half Moon Bay Brewing" },
                    { 139, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6646), null, false, null, "Humboldt Brewing" },
                    { 140, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6659), null, false, null, "Iron Springs Pub & Brewery" },
                    { 237, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8106), null, false, null, "Victory Brewing" },
                    { 235, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8065), null, false, null, "Three Floyds Brewing" },
                    { 234, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8052), null, false, null, "Sweet Water Tavern and Brewery" },
                    { 233, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8039), null, false, null, "Spring House Brewing Company" },
                    { 232, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8029), null, false, null, "Sonoran Brewing Company" },
                    { 231, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8013), null, false, null, "Rogue Ales" },
                    { 230, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8003), null, false, null, "Rock Art Brewery" },
                    { 228, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7975), null, false, null, "Pumphouse Brewery & Restaurant" },
                    { 225, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7921), null, false, null, "Onopa Brewing" },
                    { 224, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7907), null, false, null, "Oaken Barrel Brewing" },
                    { 239, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8136), null, false, null, "Boulder Beer Company" },
                    { 223, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7891), null, false, null, "Moylan's Brewery & Restaurant" },
                    { 220, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7850), null, false, null, "Mendocino Brewing - Saratoga Springs" },
                    { 217, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7801), null, false, null, "Iron Hill Brewery - Wilmingon" },
                    { 216, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7784), null, false, null, "Great Lakes Brewing" },
                    { 215, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7774), null, false, null, "Gaslight Brewery" },
                    { 214, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7760), null, false, null, "Flying Fish Brewing Company" },
                    { 213, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7744), null, false, null, "Flying Bison Brewing" },
                    { 211, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7715), null, false, null, "Elysian Brewery & Public House" },
                    { 210, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7705), null, false, null, "Eel River Brewing" },
                    { 208, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7674), null, false, null, "Dock Street Beer" },
                    { 207, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7657), null, false, null, "Climax Brewing Copmany" },
                    { 222, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7881), null, false, null, "Moon River Brewing Company" },
                    { 206, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7647), null, false, null, "City Brewing Company, LLC" },
                    { 243, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8214), null, false, null, "Clipper City Brewing Co." },
                    { 248, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8288), null, false, null, "Ithaca Beer Company" },
                    { 297, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9212), null, false, null, "Green Mountain Beverage" },
                    { 296, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9198), null, false, null, "Great Divide Brewing" },
                    { 295, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9107), null, false, null, "DuClaw" },
                    { 293, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9058), null, false, null, "Dixie Brewing" },
                    { 291, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9013), null, false, null, "Chelsea Brewing Company" },
                    { 289, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8984), null, false, null, "Caldera Brewing" },
                    { 288, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8974), null, false, null, "Buzzards Bay Brewing Inc." },
                    { 287, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8963), null, false, null, "Bullfrog Brewery" },
                    { 274, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8738), null, false, null, "Big Time Brewing" },
                    { 271, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8692), null, false, null, "Bethlehem Brew Works" },
                    { 247, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8276), null, false, null, "Fifty Fifty Brewing Co." },
                    { 269, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8659), null, false, null, "Back Road Brewery" },
                    { 267, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8613), null, false, null, "Allagash Brewing" },
                    { 266, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8602), null, false, null, "AleSmith Brewing" },
                    { 265, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8592), null, false, null, "Alaskan Brewing" },
                    { 262, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8536), null, false, null, "Diamond Bear Brewing Co." },
                    { 261, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8522), null, false, null, "Breckenridge Brewery" },
                    { 260, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8508), null, false, null, "Widmer Brothers Brewing" },
                    { 256, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8437), null, false, null, "Sweetwater Brewing - Atlanta" },
                    { 254, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8392), null, false, null, "Pyramid Alehouse, Brewery and Restaurant - Seattle" },
                    { 252, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8362), null, false, null, "Mercury Brewing Company" },
                    { 251, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8344), null, false, null, "Marin Brewing" },
                    { 268, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(8643), null, false, null, "Asheville Pizza and Brewing Co." },
                    { 204, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7619), null, false, null, "Chama River Brewing" },
                    { 203, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7608), null, false, null, "Captain Lawrence Brewing Company" },
                    { 201, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7578), null, false, null, "Bell's Brewery Inc." },
                    { 167, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7048), null, false, null, "Amherst Brewing Company" },
                    { 166, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7037), null, false, null, "Warwick Valley Wine Co." },
                    { 165, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7025), null, false, null, "Wynkoop Brewing" },
                    { 164, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7015), null, false, null, "Weyerbacher Brewing Company" },
                    { 163, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7003), null, false, null, "Upland Brewing" },
                    { 161, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6975), null, false, null, "Trap Rock Restaurant and Brewery" },
                    { 158, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6928), null, false, null, "St John Brewers" },
                    { 157, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6914), null, false, null, "Southern Tier Brewing Co" },
                    { 156, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6898), null, false, null, "Shipyard Brewing - Portland" },
                    { 155, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6882), null, false, null, "Russian River Brewing" },
                    { 168, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7065), null, false, null, "August Schell Brewing" },
                    { 153, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6851), null, false, null, "Rahr & Sons Brewing Company" },
                    { 151, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6824), null, false, null, "Orlio Organic" },
                    { 150, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6806), null, false, null, "Oggi's Pizza and Brewing - Vista" },
                    { 149, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6793), null, false, null, "Oak Creek Brewery" },
                    { 147, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6744), null, false, null, "Moosejaw Pizza & Dells Brewing Company" },
                    { 146, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6733), null, false, null, "Minneapolis Town Hall Brewery" },
                    { 145, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6717), null, false, null, "Midnight Sun Brewing Co." },
                    { 144, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6706), null, false, null, "Middle Ages Brewing" },
                    { 143, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6694), null, false, null, "Magic Hat" },
                    { 142, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6684), null, false, null, "Lion Brewery Inc." },
                    { 141, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6674), null, false, null, "Legacy Brewing Co." },
                    { 152, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(6837), null, false, null, "Philadelphia Brewing Co" },
                    { 170, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7098), null, false, null, "Firestone Walker Brewing Company" },
                    { 171, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7116), null, false, null, "Latrobe Brewing" },
                    { 173, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7145), null, false, null, "Odell Brewing" },
                    { 200, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7564), null, false, null, "Bar Harbor Brewing Company" },
                    { 199, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7538), null, false, null, "Boston Beer Company" },
                    { 198, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7500), null, false, null, "Abita Brewing Company" },
                    { 197, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7487), null, false, null, "Denver ChopHouse and Brewery" },
                    { 196, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7469), null, false, null, "Terrapin Beer Company" },
                    { 195, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7459), null, false, null, "Tailgate Beer" },
                    { 194, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7449), null, false, null, "Summit Brewing" },
                    { 193, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7434), null, false, null, "Stoudt's Brewery" },
                    { 192, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7424), null, false, null, "Smuttynose Brewing Co." },
                    { 190, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7403), null, false, null, "Roy Pitz Brewing Company" },
                    { 189, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7388), null, false, null, "Margaritaville Brewing Company" },
                    { 188, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7378), null, false, null, "Manayunk Brewery and Restaurant" },
                    { 187, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7363), null, false, null, "Lancaster Brewing Co." },
                    { 186, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7342), null, false, null, "Highland Brewing Company" },
                    { 184, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7311), null, false, null, "Flying Dog Brewery" },
                    { 183, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7295), null, false, null, "Cold Spring Brewing" },
                    { 181, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7262), null, false, null, "Blacksburg Brewing Company" },
                    { 180, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7250), null, false, null, "BJ's Restaurant and Brewery" },
                    { 179, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7238), null, false, null, "Big Sky Brewing" },
                    { 178, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7228), null, false, null, "Baron Brewing Company" },
                    { 177, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7218), null, false, null, "Allentown Brew Works" },
                    { 176, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7201), null, false, null, "FINNEGANS" },
                    { 174, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(7164), null, false, null, "Pizza Beer" },
                    { 305, 3, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(9371), null, false, null, "Hoppy Brewing Company" },
                    { 1, 1, new DateTime(2020, 10, 28, 5, 23, 41, 394, DateTimeKind.Utc).AddTicks(824), null, false, null, "Coopers Brewery" }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "Id", "ABV", "BreweryId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "IsUnlisted", "ModifiedOn", "Name", "StyleId", "UnlistedOn" },
                values: new object[,]
                {
                    { 1, 0.5, 1, new DateTime(2020, 10, 28, 5, 23, 41, 397, DateTimeKind.Utc).AddTicks(1436), null, null, false, false, null, "Birell", 1, null },
                    { 489, 5.0, 266, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9166), null, null, false, false, null, "X Extra Pale Ale", 14, null },
                    { 490, 5.0, 267, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9355), null, null, false, false, null, "Allagash White", 20, null },
                    { 499, 5.0, 268, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(43), null, null, false, false, null, "Roland's ESB", 27, null },
                    { 848, 5.2999999999999998, 268, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(8336), null, null, false, false, null, "Shiva IPA", 41, null },
                    { 502, 5.0, 269, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(420), null, null, false, false, null, "Back Road American Pale Ale", 14, null },
                    { 503, 5.0, 269, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(617), null, null, false, false, null, "Back Road Ale", 14, null },
                    { 504, 5.0, 269, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(794), null, null, false, false, null, "Belle Gunness Stout", 3, null },
                    { 505, 5.0, 269, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(981), null, null, false, false, null, "Millennium Lager", 18, null },
                    { 511, 5.0, 271, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(1848), null, null, false, false, null, "CH-47 Pale Ale", 14, null },
                    { 849, 5.2999999999999998, 271, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(8543), null, null, false, false, null, "Arc Weld Alt", 33, null },
                    { 515, 5.0, 274, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(2657), null, null, false, false, null, "Bhagwan's Best IPA", 41, null },
                    { 532, 5.0, 287, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(5758), null, null, false, false, null, "Fast Eddies Pale Ale", 14, null },
                    { 533, 5.0, 288, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(5953), null, null, false, false, null, "Olde Buzzard Lager", 10, null },
                    { 534, 5.0, 289, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(6152), null, null, false, false, null, "Pilsener Bier", 18, null },
                    { 913, 5.4000000000000004, 289, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(1966), null, null, false, false, null, "Pale Ale", 14, null },
                    { 980, 5.5, 289, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(3032), null, null, false, false, null, "Ashland Amber", 21, null },
                    { 536, 5.0, 291, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(6669), null, null, false, false, null, "Sunset Red Ale", 14, null },
                    { 545, 5.0, 293, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(7850), null, null, false, false, null, "Blackened Voodoo", 10, null },
                    { 551, 5.0, 295, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8700), null, null, false, false, null, "Blackout", 19, null },
                    { 707, 5.0999999999999996, 295, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(1697), null, null, false, false, null, "Bad Moon Porter", 15, null },
                    { 708, 5.0999999999999996, 295, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(2014), null, null, false, false, null, "Misfit Red", 21, null },
                    { 572, 5.0, 296, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(706), null, null, false, false, null, "Hot Shot ESB", 27, null },
                    { 928, 5.4000000000000004, 296, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(3884), null, null, false, false, null, "Denver Pale Ale / DPA", 14, null },
                    { 929, 5.4000000000000004, 296, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(4095), null, null, false, false, null, "Ridgeline Amber", 21, null },
                    { 573, 5.0, 297, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(909), null, null, false, false, null, "Woodchuck Amber Draft Cider", 8, null },
                    { 574, 5.0, 297, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(1113), null, null, false, false, null, "Woodchuck Granny Smith Varietal Draft Cider", 8, null },
                    { 575, 5.0, 297, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(1314), null, null, false, false, null, "Woodchuck Dark and Dry Draft Cider", 8, null },
                    { 842, 5.2999999999999998, 265, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(7639), null, null, false, false, null, "Alaskan White Ale", 20, null },
                    { 583, 5.0, 304, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(3246), null, null, false, false, null, "Hefeweizen", 10, null },
                    { 841, 5.2999999999999998, 265, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(7444), null, null, false, false, null, "Alaskan Amber", 48, null },
                    { 488, 5.0, 265, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(8955), null, null, false, false, null, "Alaskan ESB", 27, null },
                    { 480, 5.0, 239, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(7662), null, null, false, false, null, "Singletrack Copper Ale", 14, null },
                    { 971, 5.5, 239, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(881), null, null, false, false, null, "Planet Porter / Boulder Porter", 15, null },
                    { 442, 4.9000000000000004, 243, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(2555), null, null, false, false, null, "Gold Ale", 14, null },
                    { 538, 5.0, 243, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(7018), null, null, false, false, null, "Pale Ale", 14, null },
                    { 447, 4.9000000000000004, 247, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3313), null, null, false, false, null, "Base Camp Golden Ale", 9, null },
                    { 865, 5.2999999999999998, 247, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(1531), null, null, false, false, null, "Manifesto Pale Ale", 14, null },
                    { 866, 5.2999999999999998, 247, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(1717), null, null, false, false, null, "Foggy Goggle Belgian White", 20, null },
                    { 449, 4.9000000000000004, 248, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3550), null, null, false, false, null, "Nut Brown", 4, null },
                    { 450, 4.9000000000000004, 248, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3730), null, null, false, false, null, "Apricot Wheat", 25, null },
                    { 589, 5.0, 248, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4151), null, null, false, false, null, "Partly Sunny", 25, null },
                    { 459, 4.9000000000000004, 251, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(4639), null, null, false, false, null, "Albion Amber Ale", 4, null },
                    { 717, 5.0999999999999996, 251, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(3699), null, null, false, false, null, "Hefe Weiss", 10, null },
                    { 460, 4.9000000000000004, 252, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(4810), null, null, false, false, null, "Ipswich Summer", 9, null },
                    { 933, 5.4000000000000004, 252, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(4990), null, null, false, false, null, "Ipswich Original Ale", 14, null },
                    { 463, 4.9000000000000004, 254, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(5324), null, null, false, false, null, "Krystal Weizen", 10, null },
                    { 724, 5.0999999999999996, 254, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(5114), null, null, false, false, null, "DPA", 14, null },
                    { 809, 5.2000000000000002, 254, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(9908), null, null, false, false, null, "Hefeweizen", 10, null },
                    { 469, 4.9000000000000004, 256, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(5957), null, null, false, false, null, "Blue", 8, null },
                    { 741, 5.0999999999999996, 256, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(8346), null, null, false, false, null, "Georgia Brown", 4, null },
                    { 742, 5.0999999999999996, 256, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(8539), null, null, false, false, null, "Hummer", 20, null },
                    { 944, 5.4000000000000004, 256, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(6722), null, null, false, false, null, "420 Extra Pale Ale", 14, null },
                    { 476, 4.9000000000000004, 260, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(6975), null, null, false, false, null, "Widmer Hefeweizen", 23, null },
                    { 477, 4.9000000000000004, 260, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(7162), null, null, false, false, null, "Widmer W'08", 25, null },
                    { 691, 5.0, 260, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(9401), null, null, false, false, null, "Drop Top Amber Ale", 21, null },
                    { 479, 4.9000000000000004, 261, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(7482), null, null, false, false, null, "Thunder Stout", 3, null },
                    { 912, 5.4000000000000004, 261, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(1583), null, null, false, false, null, "Avalanche Amber", 21, null },
                    { 482, 5.0, 262, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(8098), null, null, false, false, null, "Paradise Porter", 15, null },
                    { 750, 5.2000000000000002, 265, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(9499), null, null, false, false, null, "Alaskan Pale", 9, null },
                    { 785, 5.2000000000000002, 304, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(5684), null, null, false, false, null, "India Pale Ale", 14, null },
                    { 585, 5.0, 305, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(3569), null, null, false, false, null, "Liquid Sunshine Blonde Ale", 9, null },
                    { 596, 5.0, 306, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4824), null, null, false, false, null, "Downtown Brown", 4, null },
                    { 726, 5.0999999999999996, 345, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(5603), null, null, false, false, null, "Pyramid Apricot Weizen", 8, null },
                    { 810, 5.2000000000000002, 345, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(154), null, null, false, false, null, "Pyramid Hefe Weizen", 23, null },
                    { 727, 5.0999999999999996, 346, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(5846), null, null, false, false, null, "Firemans #4", 9, null },
                    { 884, 5.2999999999999998, 346, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(5849), null, null, false, false, null, "Rio Blanco Pale Ale", 14, null },
                    { 728, 5.0999999999999996, 347, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(6104), null, null, false, false, null, "Shepard's Crook Scotish Ale", 43, null },
                    { 885, 5.2999999999999998, 347, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(6213), null, null, false, false, null, "LeRoy's Brown Ale", 4, null },
                    { 732, 5.0999999999999996, 348, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(6894), null, null, false, false, null, "Schlafly Oktoberfest", 34, null },
                    { 735, 5.0999999999999996, 349, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(7352), null, null, false, false, null, "Urban Wilderness English Pale", 26, null },
                    { 737, 5.0999999999999996, 350, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(7761), null, null, false, false, null, "Altbier", 33, null },
                    { 740, 5.0999999999999996, 351, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(8167), null, null, false, false, null, "Bender Beer", 4, null },
                    { 744, 5.0999999999999996, 352, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(8880), null, null, false, false, null, "The Bitter End Pale Ale", 14, null },
                    { 747, 5.2000000000000002, 353, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(9223), null, null, false, false, null, "(512) Wit", 20, null },
                    { 761, 5.2000000000000002, 355, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(1064), null, null, false, false, null, "Karma", 35, null },
                    { 770, 5.2000000000000002, 359, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(3121), null, null, false, false, null, "Wet Hop Requiem IPA", 41, null },
                    { 771, 5.2000000000000002, 360, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(3377), null, null, false, false, null, "Irish Red Ale", 31, null },
                    { 772, 5.2000000000000002, 360, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(3632), null, null, false, false, null, "Irish Red Ale", 31, null },
                    { 854, 5.2999999999999998, 360, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(9454), null, null, false, false, null, "Amber Waves", 21, null },
                    { 855, 5.2999999999999998, 360, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(9702), null, null, false, false, null, "Amber Waves", 21, null },
                    { 774, 5.2000000000000002, 361, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(4046), null, null, false, false, null, "Newport Storm Hurricane Amber Ale", 21, null },
                    { 780, 5.2000000000000002, 362, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(4785), null, null, false, false, null, "Blackberry Ale", 14, null },
                    { 781, 5.2000000000000002, 363, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(5048), null, null, false, false, null, "Flat Earth Belgian-style Pale Ale", 35, null },
                    { 782, 5.2000000000000002, 364, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(5299), null, null, false, false, null, "Founders Pale Ale", 14, null },
                    { 799, 5.2000000000000002, 369, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(8180), null, null, false, false, null, "Berghoff Oktoberfest Beer", 34, null },
                    { 801, 5.2000000000000002, 371, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(8713), null, null, false, false, null, "Ichabod", 46, null },
                    { 836, 5.2000000000000002, 371, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(6258), null, null, false, false, null, "Sundog", 21, null },
                    { 802, 5.2000000000000002, 372, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(8966), null, null, false, false, null, "Oak Barrel Stout", 3, null },
                    { 812, 5.2000000000000002, 374, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(619), null, null, false, false, null, "Richbrau Oatmeal Stout", 29, null },
                    { 725, 5.0999999999999996, 345, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(5357), null, null, false, false, null, "Apricot Weizen Ale", 23, null },
                    { 723, 5.0999999999999996, 344, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(4934), null, null, false, false, null, "Apricot Wheat", 25, null },
                    { 722, 5.0999999999999996, 344, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(4692), null, null, false, false, null, "Apricot Wheat", 25, null },
                    { 873, 5.2999999999999998, 342, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(3497), null, null, false, false, null, "Crosstown Pale Ale", 14, null },
                    { 599, 5.0, 307, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5315), null, null, false, false, null, "Flywheel Bright Lager", 10, null },
                    { 615, 5.0, 308, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(6911), null, null, false, false, null, "Apache Trout Stout", 40, null },
                    { 616, 5.0, 308, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(7127), null, null, false, false, null, "Superstition Pale Ale", 14, null },
                    { 617, 5.0, 308, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(7348), null, null, false, false, null, "Wapiti Amber Ale", 21, null },
                    { 629, 5.0, 312, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(9165), null, null, false, false, null, "Alt-Er-Ego Amber", 4, null },
                    { 803, 5.2000000000000002, 312, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(9188), null, null, false, false, null, "Patriot Pale Ale", 14, null },
                    { 631, 5.0, 313, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(9467), null, null, false, false, null, "One Nut Brown Ale", 4, null },
                    { 880, 5.2999999999999998, 313, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(4710), null, null, false, false, null, "Mama's Little Yella Pils", 18, null },
                    { 635, 5.0, 314, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(9796), null, null, false, false, null, "Engine 5", 21, null },
                    { 636, 5.0, 314, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(17), null, null, false, false, null, "Feuerwehrmann Schwarzbier", 19, null },
                    { 720, 5.0999999999999996, 314, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(4231), null, null, false, false, null, "Halligan IPA", 41, null },
                    { 721, 5.0999999999999996, 314, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(4452), null, null, false, false, null, "Shouboushi Ginger Pilsner", 18, null },
                    { 642, 5.0, 318, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(1157), null, null, false, false, null, "Amigo Lager", 10, null },
                    { 426, 4.7999999999999998, 239, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(527), null, null, false, false, null, "Hazed & Infused Dry-Hopped Ale", 14, null },
                    { 645, 5.0, 319, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(1613), null, null, false, false, null, "Rocktoberfest", 34, null },
                    { 657, 5.0, 325, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(4416), null, null, false, false, null, "Pelican Pale", 14, null },
                    { 658, 5.0, 325, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(4644), null, null, false, false, null, "Amber", 21, null },
                    { 665, 5.0, 328, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(5819), null, null, false, false, null, "Iowa Pale Ale", 14, null },
                    { 675, 5.0, 330, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(7077), null, null, false, false, null, "The Livery Hefe Weizen", 23, null },
                    { 838, 5.2000000000000002, 330, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(6765), null, null, false, false, null, "Paris-Roubaix Pale Ale", 14, null },
                    { 839, 5.2000000000000002, 330, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(6996), null, null, false, false, null, "The Livery Dunkel Weizen", 25, null },
                    { 682, 5.0, 332, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(7932), null, null, false, false, null, "Headless Man Amber Alt", 33, null },
                    { 840, 5.2000000000000002, 332, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(7235), null, null, false, false, null, "Fargo Brothers Hefeweizen", 23, null },
                    { 705, 5.0999999999999996, 340, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(1379), null, null, false, false, null, "Double Mountain Pale Ale", 14, null },
                    { 710, 5.0999999999999996, 341, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(2346), null, null, false, false, null, "Session Premium Lager", 10, null },
                    { 712, 5.0999999999999996, 342, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(2806), null, null, false, false, null, "HUB Lager", 18, null },
                    { 789, 5.2000000000000002, 342, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(6395), null, null, false, false, null, "Velvet ESB", 27, null },
                    { 872, 5.2999999999999998, 342, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(3059), null, null, false, false, null, "Survival Stout", 29, null },
                    { 652, 5.0, 321, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(3131), null, null, false, false, null, "Dubble Fantasy", 7, null },
                    { 813, 5.2000000000000002, 374, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(902), null, null, false, false, null, "Griffin Golden Ale", 9, null },
                    { 947, 5.4000000000000004, 237, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(7114), null, null, false, false, null, "Throwback Lager", 10, null },
                    { 686, 5.0, 237, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(8630), null, null, false, false, null, "Whirlwind Wit", 20, null },
                    { 332, 4.7000000000000002, 190, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7652), null, null, false, false, null, "White Horse Hefeweizen", 23, null },
                    { 334, 4.7000000000000002, 192, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7919), null, null, false, false, null, "Shoals Pale Ale", 14, null },
                    { 410, 4.7999999999999998, 192, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(8301), null, null, false, false, null, "Smuttynose Winter Ale", 38, null },
                    { 736, 5.0999999999999996, 192, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(7510), null, null, false, false, null, "Smuttynose Pumpkin Ale", 36, null },
                    { 940, 5.4000000000000004, 192, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(6194), null, null, false, false, null, "Robust Porter", 15, null },
                    { 335, 4.7000000000000002, 193, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8063), null, null, false, false, null, "Stoudt's Pils", 18, null },
                    { 336, 4.7000000000000002, 193, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8200), null, null, false, false, null, "Stoudt's Gold Lager", 10, null },
                    { 666, 5.0, 193, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(5972), null, null, false, false, null, "Scarlet Lady Ale", 27, null },
                    { 667, 5.0, 193, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(6117), null, null, false, false, null, "Stoudt's American Pale Ale", 14, null },
                    { 338, 4.7000000000000002, 194, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8426), null, null, false, false, null, "Hefe-Weizen", 23, null },
                    { 943, 5.4000000000000004, 194, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(6536), null, null, false, false, null, "Extra Pale Ale", 14, null },
                    { 339, 4.7000000000000002, 195, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8558), null, null, false, false, null, "Tailgate Light", 1, null },
                    { 670, 5.0, 195, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(6549), null, null, false, false, null, "Tailgate Hefeweizen", 23, null },
                    { 340, 4.7000000000000002, 196, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8707), null, null, false, false, null, "SunRay Wheat Beer", 25, null },
                    { 671, 5.0, 196, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(6717), null, null, false, false, null, "Terrapin Golden Ale", 42, null },
                    { 344, 4.7000000000000002, 197, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(9036), null, null, false, false, null, "Redwing", 21, null },
                    { 481, 5.0, 197, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(7902), null, null, false, false, null, "Pilsner", 10, null },
                    { 775, 5.2000000000000002, 197, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(4186), null, null, false, false, null, "Nut Brown Ale", 4, null },
                    { 987, 5.5, 197, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(4234), null, null, false, false, null, "Pale Ale", 14, null },
                    { 346, 4.7999999999999998, 198, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(9220), null, null, false, false, null, "Abita Purple Haze", 25, null },
                    { 486, 5.0, 198, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(8499), null, null, false, false, null, "Restoration Pale Ale", 14, null },
                    { 701, 5.0999999999999996, 198, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(754), null, null, false, false, null, "Satsuma Harvest Wit", 20, null },
                    { 956, 5.5, 198, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(8393), null, null, false, false, null, "Christmas Ale", 4, null },
                    { 347, 4.7999999999999998, 199, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(9365), null, null, false, false, null, "Samuel Adams Boston Lager", 10, null },
                    { 437, 4.9000000000000004, 199, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(1974), null, null, false, false, null, "Samuel Adams Black Lager", 19, null },
                    { 478, 4.9000000000000004, 199, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(7305), null, null, false, false, null, "Samuel Adams Boston Ale", 14, null },
                    { 764, 5.2000000000000002, 199, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(1473), null, null, false, false, null, "Samuel Adams Noble Pils", 18, null },
                    { 329, 4.7000000000000002, 189, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7425), null, null, false, false, null, "Landshark Lager", 1, null },
                    { 765, 5.2000000000000002, 199, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(1672), null, null, false, false, null, "Samuel Adams Summer Ale", 25, null },
                    { 876, 5.2999999999999998, 188, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(4085), null, null, false, false, null, "Krooks Mill", 14, null },
                    { 328, 4.7000000000000002, 188, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7297), null, null, false, false, null, "Bohemian Blonde", 18, null },
                    { 299, 4.5999999999999996, 174, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4321), null, null, false, false, null, "Mamma Mia! Pizza Beer", 22, null },
                    { 305, 4.5999999999999996, 176, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4842), null, null, false, false, null, "Irish Amber", 31, null },
                    { 308, 4.7000000000000002, 177, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(5031), null, null, false, false, null, "Pig Pen Pilsener", 18, null },
                    { 751, 5.2000000000000002, 177, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(9638), null, null, false, false, null, "Fegley's Amber Lager", 21, null },
                    { 752, 5.2000000000000002, 177, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(9776), null, null, false, false, null, "Steelgaarden Wit", 20, null },
                    { 753, 5.2000000000000002, 177, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(9929), null, null, false, false, null, "Pumpkin Ale", 36, null },
                    { 902, 5.4000000000000004, 177, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(9905), null, null, false, false, null, "Apricot Ale", 25, null },
                    { 309, 4.7000000000000002, 178, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(5164), null, null, false, false, null, "Baron Pilsner", 18, null },
                    { 434, 4.9000000000000004, 178, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(1489), null, null, false, false, null, "Baron Schwarzbier", 19, null },
                    { 310, 4.7000000000000002, 179, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(5294), null, null, false, false, null, "Scape Goat Pale Ale", 14, null },
                    { 311, 4.7000000000000002, 179, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(5437), null, null, false, false, null, "Trout Slayer", 25, null },
                    { 850, 5.2999999999999998, 179, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(8680), null, null, false, false, null, "Moose Drool Brown Ale", 15, null },
                    { 312, 4.7000000000000002, 180, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(5572), null, null, false, false, null, "Nit Wit", 20, null },
                    { 436, 4.9000000000000004, 180, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(1725), null, null, false, false, null, "Harvest Hefeweizen", 23, null },
                    { 313, 4.7000000000000002, 181, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(5713), null, null, false, false, null, "Blacksburger Pils", 18, null },
                    { 315, 4.7000000000000002, 183, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(5983), null, null, false, false, null, "Stite Golden Pilsner", 10, null },
                    { 319, 4.7000000000000002, 184, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6332), null, null, false, false, null, "In-Heat Wheat", 23, null }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "Id", "ABV", "BreweryId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "IsUnlisted", "ModifiedOn", "Name", "StyleId", "UnlistedOn" },
                values: new object[,]
                {
                    { 375, 4.7999999999999998, 184, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(3773), null, null, false, false, null, "Woody Creek White", 20, null },
                    { 867, 5.2999999999999998, 184, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(1977), null, null, false, false, null, "Dogtoberfest Octoberfest", 34, null },
                    { 322, 4.7000000000000002, 186, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6741), null, null, false, false, null, "Cattail Peak", 25, null },
                    { 787, 5.2000000000000002, 186, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(5896), null, null, false, false, null, "St.Terese's Pale Ale", 14, null },
                    { 871, 5.2999999999999998, 186, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(2704), null, null, false, false, null, "Black Mocha Stout", 3, null },
                    { 326, 4.7000000000000002, 187, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7095), null, null, false, false, null, "Lancaster Strawberry Wheat", 25, null },
                    { 387, 4.7999999999999998, 187, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(5307), null, null, false, false, null, "Rare Rooster Summer Rye Ale", 14, null },
                    { 594, 5.0, 187, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4549), null, null, false, false, null, "Celtic Rose", 31, null },
                    { 715, 5.0999999999999996, 187, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(3372), null, null, false, false, null, "Gold Star Pilsner", 18, null },
                    { 874, 5.2999999999999998, 187, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(3729), null, null, false, false, null, "Lancaster Milk Stout", 3, null },
                    { 716, 5.0999999999999996, 188, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(3523), null, null, false, false, null, "Brilliant Barstool", 26, null },
                    { 833, 5.2000000000000002, 199, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(5565), null, null, false, false, null, "Samuel Adams Pale Ale", 14, null },
                    { 852, 5.2999999999999998, 199, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(8934), null, null, false, false, null, "Samuel Adams Coastal Wheat", 23, null },
                    { 901, 5.2999999999999998, 199, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(9746), null, null, false, false, null, "Samuel Adams Brown Ale", 4, null },
                    { 930, 5.4000000000000004, 216, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(4251), null, null, false, false, null, "Holy Moses", 7, null },
                    { 383, 4.7999999999999998, 217, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(4774), null, null, false, false, null, "Anvil Ale", 27, null },
                    { 587, 5.0, 217, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(3840), null, null, false, false, null, "Vienna Red Lager", 10, null },
                    { 713, 5.0999999999999996, 217, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(2991), null, null, false, false, null, "Ironbound Ale", 14, null },
                    { 389, 4.7999999999999998, 220, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(5576), null, null, false, false, null, "White Ale", 20, null },
                    { 392, 4.7999999999999998, 222, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(6149), null, null, false, false, null, "Wild Wacky Wit", 20, null },
                    { 461, 4.9000000000000004, 222, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(4982), null, null, false, false, null, "Savannah Fest", 37, null },
                    { 393, 4.7999999999999998, 223, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(6299), null, null, false, false, null, "Irish Dry Stout", 3, null },
                    { 397, 4.7999999999999998, 224, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(6679), null, null, false, false, null, "Meridian Street Premium Lager", 10, null },
                    { 627, 5.0, 224, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(8825), null, null, false, false, null, "Razz-Wheat", 25, null },
                    { 400, 4.7999999999999998, 225, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(7073), null, null, false, false, null, "Nut Brown Ale", 4, null },
                    { 804, 5.2000000000000002, 225, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(9353), null, null, false, false, null, "American Pale Ale", 14, null },
                    { 404, 4.7999999999999998, 228, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(7565), null, null, false, false, null, "Four Alarm Alt", 4, null },
                    { 406, 4.7999999999999998, 230, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(7903), null, null, false, false, null, "Rock Art American Red Ale", 21, null },
                    { 644, 5.0, 230, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(1381), null, null, false, false, null, "Whitetail Golden Ale", 9, null },
                    { 407, 4.7999999999999998, 231, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(8076), null, null, false, false, null, "Half-E-Weizen", 20, null },
                    { 646, 5.0, 231, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(1865), null, null, false, false, null, "Kells Irish Style Lager", 10, null },
                    { 729, 5.0999999999999996, 231, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(6279), null, null, false, false, null, "Dry Hopped St. Rogue Red Ale", 21, null },
                    { 815, 5.2000000000000002, 231, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(1332), null, null, false, false, null, "Über Pilsner", 18, null },
                    { 887, 5.2999999999999998, 231, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(6828), null, null, false, false, null, "Mocha Porter / New Porter", 15, null },
                    { 888, 5.2999999999999998, 231, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(7097), null, null, false, false, null, "Juniper Pale Ale", 22, null },
                    { 411, 4.7999999999999998, 232, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(8465), null, null, false, false, null, "Burning Bird Pale Ale", 14, null },
                    { 412, 4.7999999999999998, 233, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(8638), null, null, false, false, null, "Goofy Foot Summer Wheat", 25, null },
                    { 414, 4.7999999999999998, 234, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(8915), null, null, false, false, null, "Black Stallion Oatmeal Stout", 29, null },
                    { 416, 4.7999999999999998, 235, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(9189), null, null, false, false, null, "Drunk Monk Hefeweizen", 23, null },
                    { 417, 4.7999999999999998, 235, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(9359), null, null, false, false, null, "Gumballhead", 25, null },
                    { 422, 4.7999999999999998, 237, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(9899), null, null, false, false, null, "Victory Lager", 10, null },
                    { 379, 4.7999999999999998, 216, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(4332), null, null, false, false, null, "Ohio City Oatmeal Stout", 3, null },
                    { 563, 5.0, 215, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9547), null, null, false, false, null, "Bison Brown", 4, null },
                    { 378, 4.7999999999999998, 215, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(4181), null, null, false, false, null, "Perfect Stout", 3, null },
                    { 376, 4.7999999999999998, 214, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(3928), null, null, false, false, null, "Extra Pale Ale", 14, null },
                    { 909, 5.4000000000000004, 199, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(1208), null, null, false, false, null, "Samuel Adams Scotch Ale", 43, null },
                    { 969, 5.5, 199, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(540), null, null, false, false, null, "Samuel Adams Blackberry Witbier", 20, null },
                    { 970, 5.5, 199, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(708), null, null, false, false, null, "Samuel Adams Chocolate Bock", 44, null },
                    { 351, 4.7999999999999998, 200, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(104), null, null, false, false, null, "Thunder Hole Ale", 4, null },
                    { 354, 4.7999999999999998, 201, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(306), null, null, false, false, null, "Third Coast Beer", 9, null },
                    { 509, 5.0, 201, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(1473), null, null, false, false, null, "Winter White Ale", 20, null },
                    { 510, 5.0, 201, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(1616), null, null, false, false, null, "Lager Beer", 10, null },
                    { 762, 5.2000000000000002, 201, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(1219), null, null, false, false, null, "Pale Ale", 14, null },
                    { 966, 5.5, 201, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(9765), null, null, false, false, null, "Octoberfest Beer", 34, null },
                    { 357, 4.7999999999999998, 203, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(628), null, null, false, false, null, "Brown Bird Brown Ale", 4, null },
                    { 358, 4.7999999999999998, 204, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(788), null, null, false, false, null, "Rye On", 10, null },
                    { 360, 4.7999999999999998, 206, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(1182), null, null, false, false, null, "Lager Beer", 10, null },
                    { 983, 5.5, 206, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(3742), null, null, false, false, null, "Pale Ale", 10, null },
                    { 897, 5.2999999999999998, 237, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(8995), null, null, false, false, null, "Prima Pils", 18, null },
                    { 361, 4.7999999999999998, 207, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(1347), null, null, false, false, null, "Climax Helles", 37, null },
                    { 773, 5.2000000000000002, 207, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(3780), null, null, false, false, null, "Climax Nut Brown Ale", 4, null },
                    { 984, 5.5, 207, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(3902), null, null, false, false, null, "Climax Extra Special Bitter Ale", 27, null },
                    { 365, 4.7999999999999998, 208, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(1923), null, null, false, false, null, "Bohemian Dock Street Pilsner", 18, null },
                    { 546, 5.0, 208, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8025), null, null, false, false, null, "Summer Session", 25, null },
                    { 990, 5.5, 208, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(4599), null, null, false, false, null, "Satellite Stout", 3, null },
                    { 369, 4.7999999999999998, 210, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(2842), null, null, false, false, null, "Certified Organic Amber Ale", 21, null },
                    { 993, 5.5, 210, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(5316), null, null, false, false, null, "Certified Organic Porter", 15, null },
                    { 370, 4.7999999999999998, 211, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(3081), null, null, false, false, null, "Elysian Fields Pale Ale", 14, null },
                    { 861, 5.2999999999999998, 211, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(997), null, null, false, false, null, "Ambrosia Maibock", 44, null },
                    { 920, 5.4000000000000004, 211, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(2871), null, null, false, false, null, "Perseus Porter", 15, null },
                    { 373, 4.7999999999999998, 213, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(3473), null, null, false, false, null, "Barnstormer Pale Ale", 14, null },
                    { 374, 4.7999999999999998, 213, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(3622), null, null, false, false, null, "Buffalo Lager", 10, null },
                    { 834, 5.2000000000000002, 213, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(5730), null, null, false, false, null, "Aviator Red", 21, null },
                    { 537, 5.0, 207, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(6848), null, null, false, false, null, "Climax Wheat Ale", 25, null },
                    { 879, 5.2999999999999998, 173, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(4486), null, null, false, false, null, "90 Shilling", 17, null },
                    { 816, 5.2000000000000002, 376, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(1586), null, null, false, false, null, "Santa Fe Nut Brown", 4, null },
                    { 817, 5.2000000000000002, 377, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(2126), null, null, false, false, null, "Hop Obama", 21, null },
                    { 992, 5.5, 415, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(5157), null, null, false, false, null, "Schlappeseppl Export", 10, null },
                    { 59, 4.0, 43, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4222), null, null, false, false, null, "Lager", 10, null },
                    { 61, 4.0, 45, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4325), null, null, false, false, null, "Ginger Tom", 22, null },
                    { 76, 4.0, 52, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5078), null, null, false, false, null, "Speight's Gold Medal Ale", 26, null },
                    { 84, 4.0, 58, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5565), null, null, false, false, null, "East India Pale Ale", 14, null },
                    { 159, 4.4000000000000004, 107, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(464), null, null, false, false, null, "Monteith's Celtic Beer", 31, null },
                    { 620, 5.0, 107, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(7580), null, null, false, false, null, "Monteith's Summer Ale", 22, null },
                    { 921, 5.4000000000000004, 397, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(3141), null, null, false, false, null, "Epic Pale Ale", 14, null },
                    { 69, 4.0, 50, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4705), null, null, false, false, null, "Labatt Blue Light", 1, null },
                    { 70, 4.0, 50, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4749), null, null, false, false, null, "Labatt Sterling", 1, null },
                    { 325, 4.7000000000000002, 50, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6950), null, null, false, false, null, "Labatt Blue", 10, null },
                    { 458, 4.9000000000000004, 50, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(4471), null, null, false, false, null, "Labatt Wildcat", 10, null },
                    { 590, 5.0, 50, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4220), null, null, false, false, null, "Labatt 50", 14, null },
                    { 591, 5.0, 50, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4288), null, null, false, false, null, "Genuine Lager", 10, null },
                    { 592, 5.0, 50, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4341), null, null, false, false, null, "Labatt Crystal", 10, null },
                    { 593, 5.0, 50, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4395), null, null, false, false, null, "Genuine Honey Lager", 10, null },
                    { 98, 4.0999999999999996, 67, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6296), null, null, false, false, null, "Carling", 10, null },
                    { 618, 5.0, 67, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(7419), null, null, false, false, null, "Red Jack", 21, null },
                    { 619, 5.0, 67, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(7484), null, null, false, false, null, "Molson Canadian", 10, null },
                    { 137, 4.2999999999999998, 91, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8784), null, null, false, false, null, "Cartwright Pale Ale", 14, null },
                    { 366, 4.7999999999999998, 91, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(2006), null, null, false, false, null, "Haupenthal Hefeweizen", 23, null },
                    { 858, 5.2999999999999998, 91, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(282), null, null, false, false, null, "Old Bridge Dark Lager", 10, null },
                    { 320, 4.7000000000000002, 185, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6464), null, null, false, false, null, "Cypress Honey Lager", 10, null },
                    { 321, 4.7000000000000002, 185, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6599), null, null, false, false, null, "Cypress Honey Lager", 10, null },
                    { 566, 5.0, 185, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9807), null, null, false, false, null, "English Bay Pale Ale", 14, null },
                    { 567, 5.0, 185, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9951), null, null, false, false, null, "English Bay Pale Ale", 14, null },
                    { 568, 5.0, 185, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(86), null, null, false, false, null, "Island Lager", 10, null },
                    { 979, 5.5, 411, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(2824), null, null, false, false, null, "Roggenbier", 10, null },
                    { 569, 5.0, 185, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(220), null, null, false, false, null, "Kitsilano Maple Cream Ale", 10, null },
                    { 959, 5.5, 404, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(9123), null, null, false, false, null, "Winterfestival", 34, null },
                    { 936, 5.4000000000000004, 400, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(5681), null, null, false, false, null, "Schneider Weisse", 25, null },
                    { 444, 4.9000000000000004, 244, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(2787), null, null, false, false, null, "Alt", 4, null },
                    { 445, 4.9000000000000004, 245, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(2965), null, null, false, false, null, "CD-Pils", 18, null },
                    { 446, 4.9000000000000004, 246, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3144), null, null, false, false, null, "Schwarzbier / Dunkel", 19, null },
                    { 456, 4.9000000000000004, 249, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(4244), null, null, false, false, null, "Jever Pilsener", 18, null },
                    { 457, 4.9000000000000004, 250, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(4421), null, null, false, false, null, "Mönchshof Premium Schwarzbier", 19, null },
                    { 462, 4.9000000000000004, 253, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(5152), null, null, false, false, null, "Alt", 4, null },
                    { 473, 4.9000000000000004, 257, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(6412), null, null, false, false, null, "Pils Legende", 18, null },
                    { 474, 4.9000000000000004, 258, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(6596), null, null, false, false, null, "Wernesgrüner Pils Legende", 18, null },
                    { 523, 5.0, 280, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(4130), null, null, false, false, null, "Weizla Hell", 23, null },
                    { 524, 5.0, 281, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(4326), null, null, false, false, null, "Waldschlösschen Dunkel", 19, null },
                    { 656, 5.0, 324, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(4181), null, null, false, false, null, "Schöfferhofer Hefeweizen Hell", 23, null },
                    { 661, 5.0, 326, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(5103), null, null, false, false, null, "Franziskaner Hefe-Weissbier Hell  / Franziskaner Club-Weiss", 23, null },
                    { 820, 5.2000000000000002, 326, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(2785), null, null, false, false, null, "Münchner Hell / Premium Lager", 10, null },
                    { 668, 5.0, 329, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(6351), null, null, false, false, null, "Malteser Weissbier", 23, null },
                    { 704, 5.0999999999999996, 339, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(1148), null, null, false, false, null, "Maisel's Weisse Kristall", 20, null },
                    { 766, 5.2000000000000002, 356, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(2039), null, null, false, false, null, "Hefe-Weizen", 23, null },
                    { 767, 5.2000000000000002, 357, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(2292), null, null, false, false, null, "Hefe-Weizen", 23, null },
                    { 788, 5.2000000000000002, 365, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(6148), null, null, false, false, null, "Bavarian-Weissbier Hefeweisse / Weisser Hirsch", 23, null },
                    { 793, 5.2000000000000002, 367, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(7309), null, null, false, false, null, "Eine Bamberger Weisse Hell", 23, null },
                    { 796, 5.2000000000000002, 368, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(7747), null, null, false, false, null, "Weissbier Hell", 23, null },
                    { 883, 5.2999999999999998, 388, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(5452), null, null, false, false, null, "Erdinger Weizen", 25, null },
                    { 890, 5.2999999999999998, 391, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(7714), null, null, false, false, null, "Schwarzbier", 19, null },
                    { 896, 5.2999999999999998, 393, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(8822), null, null, false, false, null, "Helles Hefe Weizen", 23, null },
                    { 907, 5.4000000000000004, 395, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(764), null, null, false, false, null, "Kristall Weissbier", 23, null },
                    { 908, 5.4000000000000004, 395, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(1042), null, null, false, false, null, "Hefe Weissbier", 23, null },
                    { 931, 5.4000000000000004, 398, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(4536), null, null, false, false, null, "Winter-Traum", 34, null },
                    { 935, 5.4000000000000004, 400, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(5405), null, null, false, false, null, "Weisse Hefe-Weizen", 23, null },
                    { 958, 5.5, 404, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(8717), null, null, false, false, null, "Winterfestival", 34, null },
                    { 570, 5.0, 185, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(356), null, null, false, false, null, "Kitsilano Maple Cream Ale", 10, null },
                    { 571, 5.0, 185, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(491), null, null, false, false, null, "Island Lager", 10, null },
                    { 391, 4.7999999999999998, 221, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(5986), null, null, false, false, null, "Cream Ale", 10, null },
                    { 688, 5.0, 335, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(8943), null, null, false, false, null, "Ur-Weizen", 23, null },
                    { 230, 4.5, 148, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7090), null, null, false, false, null, "Nøgne Ø Bitter", 16, null },
                    { 231, 4.5, 148, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7212), null, null, false, false, null, "Havre Stout", 29, null },
                    { 232, 4.5, 148, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7317), null, null, false, false, null, "Nøgne Ø Brown Ale", 4, null },
                    { 233, 4.5, 148, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7432), null, null, false, false, null, "Nøgne Ø Wit", 20, null },
                    { 954, 5.5, 403, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(7978), null, null, false, false, null, "Genuine Pilsner", 18, null },
                    { 955, 5.5, 403, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(8248), null, null, false, false, null, "Gull Classic", 10, null },
                    { 314, 4.7000000000000002, 182, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(5851), null, null, false, false, null, "Xingu", 19, null },
                    { 419, 4.7999999999999998, 236, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(9625), null, null, false, false, null, "Kingfisher Premium", 1, null },
                    { 464, 4.9000000000000004, 255, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(5502), null, null, false, false, null, "Foster's Lager Beer", 10, null },
                    { 647, 5.0, 255, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(2045), null, null, false, false, null, "Royal Challenge", 10, null },
                    { 730, 5.0999999999999996, 255, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(6463), null, null, false, false, null, "Peroni", 10, null },
                    { 731, 5.0999999999999996, 255, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(6645), null, null, false, false, null, "Peroni Nastro Azzurro", 10, null },
                    { 517, 5.0, 275, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(2943), null, null, false, false, null, "Borsodi", 18, null },
                    { 550, 5.0, 294, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8490), null, null, false, false, null, "Dreher Classic", 18, null },
                    { 581, 5.0, 302, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(2611), null, null, false, false, null, "Soproni", 18, null },
                    { 519, 5.0, 277, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(3334), null, null, false, false, null, "Hinano", 10, null },
                    { 530, 5.0, 285, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(5366), null, null, false, false, null, "Balashi", 18, null },
                    { 624, 5.0, 310, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(8352), null, null, false, false, null, "Lager Beer", 10, null },
                    { 639, 5.0, 315, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(488), null, null, false, false, null, "Skopsko", 10, null },
                    { 641, 5.0, 317, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(940), null, null, false, false, null, "Baltika #8", 25, null },
                    { 881, 5.2999999999999998, 317, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(4931), null, null, false, false, null, "Baltika #5", 10, null },
                    { 679, 5.0, 331, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(7492), null, null, false, false, null, "Phuket Lager", 10, null },
                    { 684, 5.0, 334, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(8407), null, null, false, false, null, "SuperBock", 44, null },
                    { 823, 5.2000000000000002, 379, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(3417), null, null, false, false, null, "Baltas Alus", 23, null },
                    { 828, 5.2000000000000002, 381, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(4632), null, null, false, false, null, "Winter Warmer", 48, null },
                    { 829, 5.2000000000000002, 381, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(4910), null, null, false, false, null, "Double Chocolate Stout", 40, null },
                    { 972, 5.5, 123, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(992), null, null, false, false, null, "Weizentrumpf", 23, null },
                    { 186, 4.5, 123, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2995), null, null, false, false, null, "Huusbier Schwarz", 15, null },
                    { 210, 4.5, 137, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5149), null, null, false, false, null, "Hite", 10, null },
                    { 161, 4.4000000000000004, 109, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(651), null, null, false, false, null, "OB Lager", 10, null },
                    { 487, 5.0, 264, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(8688), null, null, false, false, null, "Catfish Cream Ale", 10, null },
                    { 513, 5.0, 273, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(2220), null, null, false, false, null, "Buzzard Breath (discontinued)", 14, null },
                    { 514, 5.0, 273, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(2445), null, null, false, false, null, "Traditional Ale", 4, null },
                    { 520, 5.0, 278, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(3536), null, null, false, false, null, "St-Ambroise Oatmeal Stout", 29, null }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "Id", "ABV", "BreweryId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "IsUnlisted", "ModifiedOn", "Name", "StyleId", "UnlistedOn" },
                values: new object[,]
                {
                    { 521, 5.0, 278, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(3734), null, null, false, false, null, "St-Ambroise Apricot Wheat Ale", 25, null },
                    { 535, 5.0, 290, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(6364), null, null, false, false, null, "Auburn Ale", 21, null },
                    { 621, 5.0, 309, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(7795), null, null, false, false, null, "Moosehead Lager", 10, null },
                    { 622, 5.0, 309, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(8011), null, null, false, false, null, "Canadian Lager Beer", 10, null },
                    { 664, 5.0, 327, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(5592), null, null, false, false, null, "Steamwhistle Lager", 10, null },
                    { 683, 5.0, 333, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(8161), null, null, false, false, null, "Blanche de Chambly", 20, null },
                    { 868, 5.2999999999999998, 387, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(2249), null, null, false, false, null, "Premium Lager", 10, null },
                    { 94, 4.0999999999999996, 64, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6099), null, null, false, false, null, "Genuine Irish Stout", 3, null },
                    { 125, 4.2000000000000002, 84, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7953), null, null, false, false, null, "St. Patrick's Best", 16, null },
                    { 433, 4.9000000000000004, 241, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(1354), null, null, false, false, null, "Rotgold-Pils", 10, null },
                    { 261, 4.5, 84, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(186), null, null, false, false, null, "Barelegs Brew", 26, null },
                    { 413, 4.7999999999999998, 84, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(8719), null, null, false, false, null, "St. Patrick's Gold", 25, null },
                    { 131, 4.2999999999999998, 86, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8333), null, null, false, false, null, "Guinness Draught", 28, null },
                    { 132, 4.2999999999999998, 86, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8402), null, null, false, false, null, "Guinness Draught", 3, null },
                    { 496, 5.0, 86, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9707), null, null, false, false, null, "Guinness 250th Anniversary Stout", 3, null },
                    { 497, 5.0, 86, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9775), null, null, false, false, null, "Guinness Extra Stout", 3, null },
                    { 498, 5.0, 86, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9848), null, null, false, false, null, "Harp Lager", 10, null },
                    { 134, 4.2999999999999998, 88, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8554), null, null, false, false, null, "O'Hara's Celtic Stout", 3, null },
                    { 260, 4.5, 160, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(106), null, null, false, false, null, "Smithwick's", 31, null },
                    { 96, 4.0999999999999996, 66, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6219), null, null, false, false, null, "Corona Light", 1, null },
                    { 207, 4.5, 66, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4853), null, null, false, false, null, "Pacífico Clara", 10, null },
                    { 284, 4.5999999999999996, 66, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2946), null, null, false, false, null, "Corona Extra", 10, null },
                    { 189, 4.5, 126, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3332), null, null, false, false, null, "Cerveza Sol", 10, null },
                    { 193, 4.5, 128, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3604), null, null, false, false, null, "Cucapa Honey Ale", 21, null },
                    { 337, 4.7000000000000002, 84, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8279), null, null, false, false, null, "Legbiter Ale", 26, null },
                    { 837, 5.2000000000000002, 376, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(6524), null, null, false, false, null, "Santa Fe Wheat", 25, null },
                    { 432, 4.9000000000000004, 240, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(1188), null, null, false, false, null, "Weisse", 23, null },
                    { 405, 4.7999999999999998, 229, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(7727), null, null, false, false, null, "Pilsener", 18, null },
                    { 47, 3.8999999999999999, 37, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3475), null, null, false, false, null, "Scottish Ale", 17, null },
                    { 77, 4.0, 53, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5158), null, null, false, false, null, "Dragonhead Stout", 3, null },
                    { 295, 4.5999999999999996, 53, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4087), null, null, false, false, null, "Dark Island", 17, null },
                    { 630, 5.0, 53, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(9252), null, null, false, false, null, "Red MacGregor", 43, null },
                    { 95, 4.0999999999999996, 65, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6160), null, null, false, false, null, "Trashy Blonde", 13, null },
                    { 439, 4.9000000000000004, 65, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(2129), null, null, false, false, null, "77 Lager", 18, null },
                    { 525, 5.0, 65, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(4395), null, null, false, false, null, "The Physics", 27, null },
                    { 973, 5.5, 65, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(1057), null, null, false, false, null, "Hop Rocker", 10, null },
                    { 124, 4.2000000000000002, 83, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7879), null, null, false, false, null, "Tribute Premium Cornish Ale", 16, null },
                    { 663, 5.0, 83, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(5360), null, null, false, false, null, "HSD Hicks Special Draught", 16, null },
                    { 205, 4.5, 135, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4717), null, null, false, false, null, "Fuller's ESB", 27, null },
                    { 377, 4.7999999999999998, 135, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(4034), null, null, false, false, null, "India Pale Ale", 14, null },
                    { 927, 5.4000000000000004, 135, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(3677), null, null, false, false, null, "London Porter", 15, null },
                    { 211, 4.5, 138, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5252), null, null, false, false, null, "Entire Stout", 3, null },
                    { 584, 5.0, 138, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(3354), null, null, false, false, null, "Thunder Storm", 10, null },
                    { 245, 4.5, 154, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8730), null, null, false, false, null, "Old Slug Porter", 15, null },
                    { 259, 4.5, 159, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9972), null, null, false, false, null, "St Peter's Organic Ale", 26, null },
                    { 738, 5.0999999999999996, 159, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(7890), null, null, false, false, null, "Old-Style Porter", 15, null },
                    { 333, 4.7000000000000002, 191, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7782), null, null, false, false, null, "Newcastle Brown Ale", 4, null },
                    { 475, 4.9000000000000004, 259, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(6789), null, null, false, false, null, "Mackeson XXX Stout", 40, null },
                    { 507, 5.0, 270, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(1230), null, null, false, false, null, "Bass Pale Ale", 26, null },
                    { 576, 5.0, 298, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(1541), null, null, false, false, null, "Wexford Irish Cream Ale", 42, null },
                    { 578, 5.0, 300, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(2090), null, null, false, false, null, "Nightmare", 15, null },
                    { 579, 5.0, 301, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(2316), null, null, false, false, null, "Pride of Romsey IPA", 41, null },
                    { 648, 5.0, 320, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(2265), null, null, false, false, null, "Taddy Porter", 15, null },
                    { 649, 5.0, 320, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(2481), null, null, false, false, null, "Organic Ale", 9, null },
                    { 650, 5.0, 320, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(2694), null, null, false, false, null, "Nut Brown Ale", 4, null },
                    { 718, 5.0999999999999996, 14, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(3735), null, null, false, false, null, "Pendle Witches Brew", 14, null },
                    { 651, 5.0, 320, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(2907), null, null, false, false, null, "Oatmeal Stout", 3, null },
                    { 15, 3.3999999999999999, 14, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1247), null, null, false, false, null, "Black Cat", 5, null },
                    { 425, 4.7999999999999998, 8, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(357), null, null, false, false, null, "FA", 13, null },
                    { 818, 5.2000000000000002, 377, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(2430), null, null, false, false, null, "Sweet Action", 42, null },
                    { 822, 5.2000000000000002, 378, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(3145), null, null, false, false, null, "American Pale Ale", 14, null },
                    { 894, 5.2999999999999998, 378, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(8452), null, null, false, false, null, "Thomas Hooker Irish Red Ale", 31, null },
                    { 835, 5.2000000000000002, 382, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(6001), null, null, false, false, null, "Manheim Red", 21, null },
                    { 859, 5.2999999999999998, 385, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(554), null, null, false, false, null, "Otis Alt", 4, null },
                    { 860, 5.2999999999999998, 386, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(822), null, null, false, false, null, "Elm City Schwarzbier", 19, null },
                    { 886, 5.2999999999999998, 389, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(6595), null, null, false, false, null, "Hopyard IPA", 41, null },
                    { 889, 5.2999999999999998, 390, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(7441), null, null, false, false, null, "Saint Arnold Brown Ale", 4, null },
                    { 893, 5.2999999999999998, 392, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(8175), null, null, false, false, null, "Three Steve Stout", 3, null },
                    { 899, 5.2999999999999998, 394, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(9463), null, null, false, false, null, "Summer Teeth", 49, null },
                    { 932, 5.4000000000000004, 399, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(4809), null, null, false, false, null, "Blue Bell Bitter", 13, null },
                    { 939, 5.4000000000000004, 401, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(6049), null, null, false, false, null, "Steel Toe Stout", 40, null },
                    { 949, 5.4000000000000004, 402, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(7531), null, null, false, false, null, "Princess of Darkness Porter", 15, null },
                    { 967, 5.5, 405, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(77), null, null, false, false, null, "Bad Penny", 4, null },
                    { 968, 5.5, 406, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(386), null, null, false, false, null, "Bonaventure Pale Ale", 14, null },
                    { 974, 5.5, 407, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(1340), null, null, false, false, null, "Bottle Conditioned Porter", 15, null },
                    { 975, 5.5, 407, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(1632), null, null, false, false, null, "Bridgeport India Pale Ale", 41, null },
                    { 976, 5.5, 408, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(1988), null, null, false, false, null, "Oktoberfest", 10, null },
                    { 982, 5.5, 413, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(3592), null, null, false, false, null, "Maduro Oatmeal Brown Ale", 4, null },
                    { 991, 5.5, 414, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(4881), null, null, false, false, null, "Duck-Rabbit Amber Ale", 21, null },
                    { 6, 3.2000000000000002, 5, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1009), null, null, false, false, null, "Nut Brown Ale", 4, null },
                    { 175, 4.5, 5, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1568), null, null, false, false, null, "Adnam's Suffolk Special Bitter", 27, null },
                    { 176, 4.5, 5, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1926), null, null, false, false, null, "Adnams Fisherman", 21, null },
                    { 957, 5.5, 5, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(8426), null, null, false, false, null, "Adnams Explorer", 16, null },
                    { 9, 3.2000000000000002, 8, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1101), null, null, false, false, null, "Dark Mild", 6, null },
                    { 32, 3.5, 8, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(2887), null, null, false, false, null, "IPA", 12, null },
                    { 91, 4.0, 8, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5942), null, null, false, false, null, "Finest Bitter", 16, null },
                    { 698, 5.0, 8, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(236), null, null, false, false, null, "2008 Culture Beer", 16, null },
                    { 692, 5.0, 336, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(9635), null, null, false, false, null, "Grozet Gooseberry and Wheat Ale", 25, null },
                    { 693, 5.0, 337, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(9876), null, null, false, false, null, "Duchy Originals Organic English Ale", 27, null },
                    { 714, 5.0999999999999996, 343, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(3227), null, null, false, false, null, "Sneck Lifter", 11, null },
                    { 529, 5.0, 284, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(5166), null, null, false, false, null, "Gueuze 1882", 24, null },
                    { 531, 5.0, 286, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(5562), null, null, false, false, null, "Tikka Gold", 14, null },
                    { 769, 5.2000000000000002, 286, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(2838), null, null, false, false, null, "Bruegel Amber Ale", 21, null },
                    { 542, 5.0, 292, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(7361), null, null, false, false, null, "Daas Organic Witte", 20, null },
                    { 768, 5.2000000000000002, 358, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(2538), null, null, false, false, null, "Stella Artois", 10, null },
                    { 790, 5.2000000000000002, 366, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(6839), null, null, false, false, null, "Jupiler", 18, null },
                    { 808, 5.2000000000000002, 373, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(9730), null, null, false, false, null, "Palm Speciale", 21, null },
                    { 977, 5.5, 409, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(2263), null, null, false, false, null, "Petrus Speciale", 14, null },
                    { 978, 5.5, 410, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(2543), null, null, false, false, null, "St. Bernardus Witbier", 20, null },
                    { 36, 3.7000000000000002, 30, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3044), null, null, false, false, null, "Breznak Dunkles/Dark", 11, null },
                    { 162, 4.4000000000000004, 110, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(743), null, null, false, false, null, "Pilsner Urquell", 18, null },
                    { 526, 5.0, 282, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(4591), null, null, false, false, null, "Budweiser Budvar (Czechvar)", 18, null },
                    { 640, 5.0, 316, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(710), null, null, false, false, null, "Staropramen", 18, null },
                    { 38, 3.7999999999999998, 32, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3122), null, null, false, false, null, "Atlas Lager", 10, null },
                    { 48, 3.8999999999999999, 38, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3522), null, null, false, false, null, "Caguama", 18, null },
                    { 50, 3.8999999999999999, 39, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3619), null, null, false, false, null, "Black Abbot / Schwarzer Abt", 19, null },
                    { 182, 4.5, 120, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2511), null, null, false, false, null, "Beck´s", 18, null },
                    { 265, 4.5, 162, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(600), null, null, false, false, null, "Alt", 4, null },
                    { 279, 4.5999999999999996, 169, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2446), null, null, false, false, null, "Alt", 4, null },
                    { 356, 4.7999999999999998, 202, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(487), null, null, false, false, null, "Premium Beer", 18, null },
                    { 367, 4.7999999999999998, 209, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(2155), null, null, false, false, null, "Hansa Pils", 18, null },
                    { 368, 4.7999999999999998, 209, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(2446), null, null, false, false, null, "Hansa Imported Dortmunder", 10, null },
                    { 372, 4.7999999999999998, 212, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(3320), null, null, false, false, null, "Flensburger Pilsner", 18, null },
                    { 385, 4.7999999999999998, 218, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(5011), null, null, false, false, null, "Weissbier", 23, null },
                    { 386, 4.7999999999999998, 219, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(5169), null, null, false, false, null, "Schwarzbier", 19, null },
                    { 402, 4.7999999999999998, 226, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(7258), null, null, false, false, null, "Schwarz Bier", 19, null },
                    { 403, 4.7999999999999998, 227, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(7412), null, null, false, false, null, "Alt", 4, null },
                    { 528, 5.0, 283, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(4967), null, null, false, false, null, "Achel Blond 5°", 7, null },
                    { 527, 5.0, 283, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(4778), null, null, false, false, null, "Achel Bruin 5°", 11, null },
                    { 522, 5.0, 279, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(3932), null, null, false, false, null, "Vigneronne", 24, null },
                    { 518, 5.0, 276, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(3138), null, null, false, false, null, "Brasserie de Brunehaut Bio Bière Blanche (Organic)", 20, null },
                    { 800, 5.2000000000000002, 370, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(8439), null, null, false, false, null, "Old Speckled Hen", 26, null },
                    { 814, 5.2000000000000002, 375, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(1164), null, null, false, false, null, "Ivanhoe", 14, null },
                    { 824, 5.2000000000000002, 380, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(3676), null, null, false, false, null, "Wells Bombardier English Bitter", 13, null },
                    { 825, 5.2000000000000002, 380, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(3932), null, null, false, false, null, "Wells Banana Bread Beer", 8, null },
                    { 826, 5.2000000000000002, 380, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(4209), null, null, false, false, null, "Young's Winter Warmer", 47, null },
                    { 853, 5.2999999999999998, 383, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(9190), null, null, false, false, null, "Strongbow Cider", 8, null },
                    { 856, 5.2999999999999998, 384, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(44), null, null, false, false, null, "Monkey Wrench Dark Ale", 15, null },
                    { 12, 3.2999999999999998, 11, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1173), null, null, false, false, null, "Stiegl Leicht", 1, null },
                    { 467, 4.9000000000000004, 11, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(5745), null, null, false, false, null, "Columbus Pils", 18, null },
                    { 468, 4.9000000000000004, 11, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(5786), null, null, false, false, null, "Stiegl Goldbräu", 34, null },
                    { 739, 5.0999999999999996, 11, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(7933), null, null, false, false, null, "Weizengold Hefefein", 23, null },
                    { 654, 5.0, 323, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(3569), null, null, false, false, null, "Stout", 3, null },
                    { 655, 5.0, 323, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(3802), null, null, false, false, null, "Weizen", 23, null },
                    { 423, 4.7999999999999998, 238, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(77), null, null, false, false, null, "Warsteiner Premium Verum", 18, null },
                    { 14, 3.2999999999999998, 13, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1222), null, null, false, false, null, "Kirin Light", 1, null },
                    { 625, 5.0, 311, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(8561), null, null, false, false, null, "Ise Kadoya Stout", 3, null },
                    { 699, 5.0, 338, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(483), null, null, false, false, null, "Koshihikari Echigo Beer", 45, null },
                    { 17, 3.5, 16, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1296), null, null, false, false, null, "Amstel Light", 1, null },
                    { 512, 5.0, 272, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(2032), null, null, false, false, null, "Tilburg's Dutch Brown Ale", 7, null },
                    { 577, 5.0, 299, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(1878), null, null, false, false, null, "Grolsch Premium Pilsner", 18, null },
                    { 869, 5.2999999999999998, 299, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(2476), null, null, false, false, null, "Grolsch Amber Ale", 33, null },
                    { 582, 5.0, 303, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(2819), null, null, false, false, null, "Heineken", 10, null },
                    { 18, 3.5, 17, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1326), null, null, false, false, null, "Petite Orval", 7, null },
                    { 187, 4.5, 124, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3129), null, null, false, false, null, "Mort Subite Gueuze Lambic", 24, null },
                    { 188, 4.5, 125, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3235), null, null, false, false, null, "St. Louis Gueuze", 24, null },
                    { 359, 4.7999999999999998, 205, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(1034), null, null, false, false, null, "Chimay Dorée", 7, null },
                    { 440, 4.9000000000000004, 242, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(2306), null, null, false, false, null, "Hoegaarden", 25, null },
                    { 485, 5.0, 263, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(8355), null, null, false, false, null, "Drie Fonteinen Kriek", 24, null },
                    { 25, 3.5, 23, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1553), null, null, false, false, null, "Hitachino Nest Sweet Stout", 3, null },
                    { 878, 5.2999999999999998, 173, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(4353), null, null, false, false, null, "5 Barrel Pale Ale", 26, null },
                    { 719, 5.0999999999999996, 173, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(3965), null, null, false, false, null, "Levity Amber Ale", 21, null },
                    { 398, 4.7999999999999998, 173, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(6808), null, null, false, false, null, "Cutthroat Porter", 15, null },
                    { 783, 5.2000000000000002, 49, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(5368), null, null, false, false, null, "Alt Ball and Chain", 33, null },
                    { 71, 4.0, 51, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4809), null, null, false, false, null, "Blackbeary Wheat", 25, null },
                    { 158, 4.4000000000000004, 51, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(364), null, null, false, false, null, "Harvest", 4, null },
                    { 289, 4.5999999999999996, 51, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3437), null, null, false, false, null, "Long Trail Ale", 33, null },
                    { 327, 4.7000000000000002, 51, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7161), null, null, false, false, null, "Belgian White", 20, null },
                    { 595, 5.0, 51, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4612), null, null, false, false, null, "Long Trail Hefeweizen", 23, null },
                    { 78, 4.0, 54, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5220), null, null, false, false, null, "Penn Weizen", 23, null },
                    { 79, 4.0, 54, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5277), null, null, false, false, null, "Penn Pilsner", 18, null },
                    { 80, 4.0, 54, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5330), null, null, false, false, null, "Penn Gold", 10, null },
                    { 238, 4.5, 54, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7955), null, null, false, false, null, "Penn Oktoberfest", 34, null },
                    { 81, 4.0, 55, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5384), null, null, false, false, null, "Summer Blonde", 9, null },
                    { 246, 4.5, 55, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8793), null, null, false, false, null, "River Horse Special Ale", 27, null },
                    { 643, 5.0, 55, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(1215), null, null, false, false, null, "River Horse Lager", 10, null },
                    { 82, 4.0, 56, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5447), null, null, false, false, null, "Barristers Bitter", 27, null },
                    { 248, 4.5, 56, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8977), null, null, false, false, null, "Abbey Extra", 35, null },
                    { 249, 4.5, 56, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9031), null, null, false, false, null, "Brewhouse Lager", 10, null },
                    { 938, 5.4000000000000004, 56, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(5755), null, null, false, false, null, "Sacramento Nut Brown Ale", 4, null },
                    { 83, 4.0, 57, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5501), null, null, false, false, null, "Del Rio Lager", 10, null },
                    { 252, 4.5, 57, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9259), null, null, false, false, null, "Yellowjacket Pale Ale", 14, null },
                    { 253, 4.5, 57, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9319), null, null, false, false, null, "Free Bike Amber", 21, null },
                    { 409, 4.7999999999999998, 57, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(8143), null, null, false, false, null, "Skagit Brown Ale", 4, null }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "Id", "ABV", "BreweryId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "IsUnlisted", "ModifiedOn", "Name", "StyleId", "UnlistedOn" },
                values: new object[,]
                {
                    { 891, 5.2999999999999998, 57, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(7836), null, null, false, false, null, "Skibbereen Stout", 3, null },
                    { 85, 4.0, 59, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5624), null, null, false, false, null, "King's Peak Porter", 15, null },
                    { 86, 4.0, 60, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5680), null, null, false, false, null, "Bartles and Lager", 10, null },
                    { 127, 4.2000000000000002, 60, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8055), null, null, false, false, null, "Hefeweizen", 23, null },
                    { 420, 4.7999999999999998, 60, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(9683), null, null, false, false, null, "Pale Pale Boss Ale", 14, null },
                    { 421, 4.7999999999999998, 60, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(9735), null, null, false, false, null, "Mullin Brown", 4, null },
                    { 562, 5.0, 49, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9397), null, null, false, false, null, "Black Betty Schwartzbier", 19, null },
                    { 87, 4.0, 61, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5738), null, null, false, false, null, "Wasatch 1st Amendment Lager", 10, null },
                    { 561, 5.0, 49, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9339), null, null, false, false, null, "Hefeweizen", 23, null },
                    { 559, 5.0, 49, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9228), null, null, false, false, null, "Oatmeal Stout", 29, null },
                    { 601, 5.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5495), null, null, false, false, null, "Wheatland Wheat", 14, null },
                    { 602, 5.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5546), null, null, false, false, null, "Sebewaing Beer", 14, null },
                    { 603, 5.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5591), null, null, false, false, null, "Nut Brown Ale", 4, null },
                    { 604, 5.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5638), null, null, false, false, null, "Big Mac", 10, null },
                    { 605, 5.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5688), null, null, false, false, null, "Sunset Amber Lager", 10, null },
                    { 797, 5.2000000000000002, 40, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(7792), null, null, false, false, null, "Superior Stout", 3, null },
                    { 55, 3.8999999999999999, 41, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4055), null, null, false, false, null, "Long Island Potato Stout", 3, null },
                    { 262, 4.5, 41, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(237), null, null, false, false, null, "Harborfields HefeWeizen", 23, null },
                    { 674, 5.0, 41, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(6847), null, null, false, false, null, "Old Walt Smoked Wit", 20, null },
                    { 57, 4.0, 42, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4133), null, null, false, false, null, "Wahoo Wheat Beer", 20, null },
                    { 506, 5.0, 42, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(1035), null, null, false, false, null, "Calico Amber Ale", 21, null },
                    { 60, 4.0, 44, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4272), null, null, false, false, null, "Lawnmower", 14, null },
                    { 547, 5.0, 44, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8075), null, null, false, false, null, "Shelter Pale Ale", 14, null },
                    { 779, 5.2000000000000002, 44, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(4539), null, null, false, false, null, "Chicory Stout", 3, null },
                    { 62, 4.0, 46, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4374), null, null, false, false, null, "Foster Child Australian Lager", 10, null },
                    { 63, 4.0, 47, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4432), null, null, false, false, null, "Sunshie Wit", 20, null },
                    { 111, 4.2000000000000002, 47, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7044), null, null, false, false, null, "Presque Isle Pilsner", 18, null },
                    { 552, 5.0, 47, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8746), null, null, false, false, null, "Derailed Black Cherry Ale", 8, null },
                    { 922, 5.4000000000000004, 47, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(3210), null, null, false, false, null, "Heritage Alt Beer", 33, null },
                    { 996, 5.5, 47, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(5605), null, null, false, false, null, "Mad Anthony's Ale", 14, null },
                    { 64, 4.0, 48, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4482), null, null, false, false, null, "Wild Salmon Pale Ale", 14, null },
                    { 202, 4.5, 48, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4517), null, null, false, false, null, "Whistling Pig Hefeweizen", 10, null },
                    { 318, 4.7000000000000002, 48, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6191), null, null, false, false, null, "Blind Pig Dunkelweizen", 10, null },
                    { 557, 5.0, 48, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9140), null, null, false, false, null, "Organic Amber Ale", 21, null },
                    { 68, 4.0, 49, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4661), null, null, false, false, null, "Arizona Peach", 24, null },
                    { 204, 4.5, 49, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4605), null, null, false, false, null, "8th Street Ale", 13, null },
                    { 448, 4.9000000000000004, 49, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3383), null, null, false, false, null, "Caulfield's Rye", 39, null },
                    { 560, 5.0, 49, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9278), null, null, false, false, null, "American-Style Brown Ale", 4, null },
                    { 88, 4.0, 62, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5804), null, null, false, false, null, "Valley Brew Pale Wheat", 25, null },
                    { 89, 4.0, 62, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5859), null, null, false, false, null, "Valley Berry Wheat", 8, null },
                    { 90, 4.0, 62, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5914), null, null, false, false, null, "Valley Brewing Apricot Ale", 8, null },
                    { 101, 4.0999999999999996, 70, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6498), null, null, false, false, null, "Avenue Ale", 14, null },
                    { 102, 4.2000000000000002, 71, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6557), null, null, false, false, null, "IC Light", 1, null },
                    { 145, 4.2999999999999998, 71, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9429), null, null, false, false, null, "Augustiner", 10, null },
                    { 213, 4.5, 71, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5426), null, null, false, false, null, "Iron City", 10, null },
                    { 108, 4.2000000000000002, 72, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6870), null, null, false, false, null, "Keystone Light", 1, null },
                    { 109, 4.2000000000000002, 72, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6928), null, null, false, false, null, "Coors Light", 1, null },
                    { 443, 4.9000000000000004, 72, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(2622), null, null, false, false, null, "Killian's Irish Red", 10, null },
                    { 540, 5.0, 72, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(7101), null, null, false, false, null, "Coors Extra Gold Lager", 10, null },
                    { 541, 5.0, 72, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(7161), null, null, false, false, null, "Coors Original", 1, null },
                    { 915, 5.4000000000000004, 72, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(2327), null, null, false, false, null, "Rising Moon Spring Ale", 21, null },
                    { 916, 5.4000000000000004, 72, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(2404), null, null, false, false, null, "Blue Moon Belgian White", 20, null },
                    { 110, 4.2000000000000002, 73, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6992), null, null, false, false, null, "Golden Treasure", 9, null },
                    { 917, 5.4000000000000004, 73, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(2480), null, null, false, false, null, "Calico Jack Amber Ale", 21, null },
                    { 112, 4.2000000000000002, 74, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7110), null, null, false, false, null, "Estes Park Gold", 9, null },
                    { 113, 4.2000000000000002, 74, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7174), null, null, false, false, null, "Long's Peak Raspberry Wheat", 8, null },
                    { 200, 4.5, 74, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4404), null, null, false, false, null, "Trail Ridge Red", 21, null },
                    { 201, 4.5, 74, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4468), null, null, false, false, null, "Staggering Elk", 10, null },
                    { 281, 4.5999999999999996, 74, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2718), null, null, false, false, null, "Estes Park Porter", 15, null },
                    { 553, 5.0, 74, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8809), null, null, false, false, null, "Samson Stout", 3, null },
                    { 115, 4.2000000000000002, 75, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7290), null, null, false, false, null, "Dundee Wheat Beer", 25, null },
                    { 209, 4.5, 75, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5040), null, null, false, false, null, "Honey Brown", 10, null },
                    { 786, 5.2000000000000002, 75, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(5754), null, null, false, false, null, "Genesee Cream Ale", 10, null },
                    { 870, 5.2999999999999998, 75, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(2564), null, null, false, false, null, "Dundee Pale Ale", 14, null },
                    { 116, 4.2000000000000002, 76, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7372), null, null, false, false, null, "Leinenkugel's Summer Shandy", 25, null },
                    { 286, 4.5999999999999996, 76, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3123), null, null, false, false, null, "Original", 10, null },
                    { 323, 4.7000000000000002, 76, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6805), null, null, false, false, null, "Leinenkugel's Berry Weiss", 8, null },
                    { 451, 4.9000000000000004, 76, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3795), null, null, false, false, null, "Fireside Nut Brown", 4, null },
                    { 821, 5.2000000000000002, 69, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(2870), null, null, false, false, null, "Church Brew Oktoberfest", 34, null },
                    { 470, 4.9000000000000004, 69, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(6021), null, null, false, false, null, "Pipe Organ Pale Ale", 14, null },
                    { 415, 4.7999999999999998, 69, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(9017), null, null, false, false, null, "Blast Furnace Oatmeal Stout", 29, null },
                    { 263, 4.5, 69, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(307), null, null, false, false, null, "Heavenly Hefeweizen", 23, null },
                    { 150, 4.2999999999999998, 62, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9826), null, null, false, false, null, "Ports Pale Ale", 14, null },
                    { 171, 4.4000000000000004, 62, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1381), null, null, false, false, null, "Valley Brewing London Tavern Ale", 5, null },
                    { 267, 4.5, 62, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(787), null, null, false, false, null, "Fat City Ale", 9, null },
                    { 92, 4.0999999999999996, 63, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6003), null, null, false, false, null, "Bud Ice Light", 10, null },
                    { 104, 4.2000000000000002, 63, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6651), null, null, false, false, null, "Michelob Ultra", 10, null },
                    { 105, 4.2000000000000002, 63, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6705), null, null, false, false, null, "Natural Light", 1, null },
                    { 106, 4.2000000000000002, 63, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6759), null, null, false, false, null, "Bud Light Lime", 1, null },
                    { 107, 4.2000000000000002, 63, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6812), null, null, false, false, null, "Bud Light", 1, null },
                    { 129, 4.2999999999999998, 63, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8183), null, null, false, false, null, "Michelob Light", 1, null },
                    { 130, 4.2999999999999998, 63, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8246), null, null, false, false, null, "Budweiser Select", 1, null },
                    { 430, 4.9000000000000004, 63, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(914), null, null, false, false, null, "Michelob Honey Lager", 10, null },
                    { 491, 5.0, 63, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9417), null, null, false, false, null, "Michelob Beer", 10, null },
                    { 492, 5.0, 63, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9477), null, null, false, false, null, "Michelob Black & Tan", 10, null },
                    { 600, 5.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5406), null, null, false, false, null, "Peninsula Porter", 15, null },
                    { 493, 5.0, 63, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9535), null, null, false, false, null, "Michelob Ultra Amber", 10, null },
                    { 495, 5.0, 63, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9639), null, null, false, false, null, "Budweiser", 1, null },
                    { 702, 5.0999999999999996, 63, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(826), null, null, false, false, null, "Budweiser American Ale", 21, null },
                    { 755, 5.2000000000000002, 63, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(139), null, null, false, false, null, "Shock Top", 20, null },
                    { 756, 5.2000000000000002, 63, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(203), null, null, false, false, null, "Michelob Amber Bock", 10, null },
                    { 844, 5.2999999999999998, 63, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(7858), null, null, false, false, null, "Michelob Hop Hound Amber Wheat", 25, null },
                    { 845, 5.2999999999999998, 63, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(7934), null, null, false, false, null, "Sun Dog Amber Wheat", 25, null },
                    { 905, 5.4000000000000004, 63, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(245), null, null, false, false, null, "Beach Bum Blonde Ale", 9, null },
                    { 962, 5.5, 63, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(9378), null, null, false, false, null, "Green Valley Stone Mill Pale Ale", 14, null },
                    { 963, 5.5, 63, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(9441), null, null, false, false, null, "Michelob Dunkel Weisse", 11, null },
                    { 964, 5.5, 63, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(9507), null, null, false, false, null, "Bud Ice", 10, null },
                    { 965, 5.5, 63, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(9602), null, null, false, false, null, "Jack's Pumpkin Spice Ale", 36, null },
                    { 99, 4.0999999999999996, 68, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6358), null, null, false, false, null, "Featherweight Lager", 10, null },
                    { 100, 4.0999999999999996, 69, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6433), null, null, false, false, null, "Celestial Gold", 18, null },
                    { 494, 5.0, 63, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(9587), null, null, false, false, null, "Bud Dry", 1, null },
                    { 452, 4.9000000000000004, 76, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3866), null, null, false, false, null, "Leinenkugel's Sunset Wheat", 20, null },
                    { 75, 4.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(5018), null, null, false, false, null, "Golden Pheasant", 10, null },
                    { 73, 4.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4890), null, null, false, false, null, "Bavarian Dark", 4, null },
                    { 331, 4.7000000000000002, 7, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7508), null, null, false, false, null, "Apricot Wheat", 25, null },
                    { 401, 4.7999999999999998, 7, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(7098), null, null, false, false, null, "Spruce Creek Lager", 10, null },
                    { 632, 5.0, 7, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(9496), null, null, false, false, null, "Mt. Nittany Pale Ale", 14, null },
                    { 633, 5.0, 7, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(9525), null, null, false, false, null, "Red Mo Ale", 14, null },
                    { 634, 5.0, 7, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(9572), null, null, false, false, null, "Slab Cabin IPA", 41, null },
                    { 805, 5.2000000000000002, 7, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(9385), null, null, false, false, null, "Arthur's Nugget Pale Ale", 14, null },
                    { 806, 5.2000000000000002, 7, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(9441), null, null, false, false, null, "Dubbel Ale", 38, null },
                    { 807, 5.2000000000000002, 7, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(9470), null, null, false, false, null, "Spring Creek Lager", 10, null },
                    { 10, 3.2000000000000002, 9, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1125), null, null, false, false, null, "Cliffhanger Light Ale", 1, null },
                    { 352, 4.7999999999999998, 9, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(126), null, null, false, false, null, "Antler Brown Ale", 4, null },
                    { 11, 3.2999999999999998, 10, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1152), null, null, false, false, null, "Small Beer", 5, null },
                    { 275, 4.5999999999999996, 10, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2023), null, null, false, false, null, "Anchor Summer Beer", 25, null },
                    { 961, 5.5, 10, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(9314), null, null, false, false, null, "Bock", 44, null },
                    { 13, 3.2999999999999998, 12, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1196), null, null, false, false, null, "Shut The Hell Up", 5, null },
                    { 152, 4.2999999999999998, 12, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9943), null, null, false, false, null, "Light Weight", 9, null },
                    { 672, 5.0, 12, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(6759), null, null, false, false, null, "Piston Bitter", 27, null },
                    { 673, 5.0, 12, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(6791), null, null, false, false, null, "Broken Spoke", 14, null },
                    { 745, 5.2000000000000002, 12, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(8934), null, null, false, false, null, "Holy Cow IPA", 41, null },
                    { 746, 5.2000000000000002, 12, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(8973), null, null, false, false, null, "Donovan's Red", 21, null },
                    { 951, 5.4000000000000004, 12, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(7611), null, null, false, false, null, "Pappy's Porter", 15, null },
                    { 16, 3.3999999999999999, 15, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1270), null, null, false, false, null, "Yuengling Lager Light", 1, null },
                    { 29, 3.5, 15, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1683), null, null, false, false, null, "Yuengling Premium Light", 1, null },
                    { 172, 4.4000000000000004, 15, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1410), null, null, false, false, null, "Yuengling Premium Beer", 10, null },
                    { 173, 4.4000000000000004, 15, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1439), null, null, false, false, null, "Yuengling Lager", 10, null },
                    { 343, 4.7000000000000002, 15, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8891), null, null, false, false, null, "Yuengling Porter", 15, null },
                    { 950, 5.4000000000000004, 15, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(7576), null, null, false, false, null, "Lord Chesterfield Ale", 14, null },
                    { 19, 3.5, 18, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1353), null, null, false, false, null, "Dark Mild", 5, null },
                    { 296, 4.5999999999999996, 7, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4107), null, null, false, false, null, "Black Mo Stout", 3, null },
                    { 20, 3.5, 19, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1385), null, null, false, false, null, "Huckleberry Ale", 8, null },
                    { 8, 3.2000000000000002, 7, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1079), null, null, false, false, null, "Arthur's Mild Ale", 5, null },
                    { 355, 4.7999999999999998, 6, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(338), null, null, false, false, null, "Raspberry Wheat", 25, null },
                    { 4, 2.8999999999999999, 1, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(963), null, null, false, false, null, "Coopers Premium Light", 1, null },
                    { 21, 3.5, 1, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1405), null, null, false, false, null, "Coopers Mild Ale", 5, null },
                    { 190, 4.5, 1, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3362), null, null, false, false, null, "Coopers Original Pale Ale", 26, null },
                    { 191, 4.5, 1, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3379), null, null, false, false, null, "Dark Ale", 5, null },
                    { 539, 5.0, 1, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(7039), null, null, false, false, null, "Coopers Premium Lager", 10, null },
                    { 288, 4.5999999999999996, 172, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3372), null, null, false, false, null, "Little Creatures Pilsner", 18, null },
                    { 303, 4.5999999999999996, 175, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4621), null, null, false, false, null, "New", 10, null },
                    { 653, 5.0, 322, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(3356), null, null, false, false, null, "Scharer's Lager", 10, null },
                    { 760, 5.2000000000000002, 354, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(805), null, null, false, false, null, "King Lager", 1, null },
                    { 2, 1.0, 2, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(770), null, null, false, false, null, "Estrella Levante Sin 0.0% Alcohol", 2, null },
                    { 300, 4.5999999999999996, 2, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4343), null, null, false, false, null, "Estrella Damm", 10, null },
                    { 408, 4.7999999999999998, 2, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(8092), null, null, false, false, null, "Estrella Levante Clasica", 2, null },
                    { 937, 5.4000000000000004, 2, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(5702), null, null, false, false, null, "Estrella Levante Especial", 2, null },
                    { 914, 5.4000000000000004, 396, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(2249), null, null, false, false, null, "Negra", 19, null },
                    { 3, 2.7999999999999998, 3, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(943), null, null, false, false, null, "Miller Genuine Draft 64", 1, null },
                    { 97, 4.0999999999999996, 3, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6235), null, null, false, false, null, "Miller Chill", 1, null },
                    { 117, 4.2000000000000002, 3, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7388), null, null, false, false, null, "Miller Lite", 1, null },
                    { 224, 4.5, 3, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6515), null, null, false, false, null, "Milwaukee's Best Light", 1, null },
                    { 225, 4.5, 3, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6530), null, null, false, false, null, "Milwaukees Best", 1, null },
                    { 307, 4.7000000000000002, 3, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4901), null, null, false, false, null, "Miller Genuine Draft", 1, null },
                    { 613, 5.0, 3, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(6608), null, null, false, false, null, "Red Dog", 1, null },
                    { 614, 5.0, 3, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(6625), null, null, false, false, null, "Miller High Life", 1, null },
                    { 5, 3.1000000000000001, 4, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(983), null, null, false, false, null, "Stud Service Stout", 3, null },
                    { 56, 3.8999999999999999, 4, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4079), null, null, false, false, null, "Rasberry Ale", 8, null },
                    { 676, 5.0, 4, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(7123), null, null, false, false, null, "Twisted Kilt Scotch Ale", 43, null },
                    { 677, 5.0, 4, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(7164), null, null, false, false, null, "Spiced Pumpkin Ale", 36, null },
                    { 7, 3.2000000000000002, 6, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1027), null, null, false, false, null, "Buck Naked", 1, null },
                    { 763, 5.2000000000000002, 6, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(1249), null, null, false, false, null, "Big Buck Beer", 14, null },
                    { 22, 3.5, 20, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1437), null, null, false, false, null, "Humbles Blond Ale", 9, null },
                    { 65, 4.0, 20, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4518), null, null, false, false, null, "Spitfire Best Bitter", 16, null },
                    { 66, 4.0, 20, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4564), null, null, false, false, null, "Heat Seeker Hefe", 23, null },
                    { 42, 3.7999999999999998, 29, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3289), null, null, false, false, null, ".38 Special Bitter", 16, null },
                    { 126, 4.2000000000000002, 29, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7986), null, null, false, false, null, "Sully's Irish Stout", 3, null },
                    { 669, 5.0, 29, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(6399), null, null, false, false, null, "Grateful Red", 21, null },
                    { 892, 5.2999999999999998, 29, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(7908), null, null, false, false, null, "Pirate's Porter", 11, null },
                    { 37, 3.7000000000000002, 31, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3076), null, null, false, false, null, "Locke Mountain Light", 1, null },
                    { 330, 4.7000000000000002, 31, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(7475), null, null, false, false, null, "Highway 22 Wheat", 25, null },
                    { 39, 3.7999999999999998, 33, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3156), null, null, false, false, null, "Piels Light", 1, null },
                    { 46, 3.7999999999999998, 33, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3428), null, null, false, false, null, "Lone Star Light", 1, null }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "Id", "ABV", "BreweryId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "IsUnlisted", "ModifiedOn", "Name", "StyleId", "UnlistedOn" },
                values: new object[,]
                {
                    { 103, 4.2000000000000002, 33, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6591), null, null, false, false, null, "Pabst Blue Ribbon Light", 1, null },
                    { 297, 4.5999999999999996, 33, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4148), null, null, false, false, null, "Old Milwaukee", 10, null },
                    { 298, 4.5999999999999996, 33, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4185), null, null, false, false, null, "Piels Draft", 1, null },
                    { 306, 4.7000000000000002, 33, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4884), null, null, false, false, null, "Lone Star", 10, null },
                    { 345, 4.7000000000000002, 33, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(9071), null, null, false, false, null, "Pabst Blue Ribbon", 1, null },
                    { 40, 3.7999999999999998, 34, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3202), null, null, false, false, null, "Mt. Takhoma Blonde Ale", 9, null },
                    { 114, 4.2000000000000002, 34, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7216), null, null, false, false, null, "Pinnacle Peak Pale Ale", 14, null },
                    { 41, 3.7999999999999998, 35, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3248), null, null, false, false, null, "Zone 7 Porter", 15, null },
                    { 54, 3.8999999999999999, 35, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4013), null, null, false, false, null, "Honey Wheat", 10, null },
                    { 882, 5.2999999999999998, 35, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(4980), null, null, false, false, null, "Pleasanton Pale", 14, null },
                    { 43, 3.7999999999999998, 36, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3329), null, null, false, false, null, "O! Gold Light Beer", 1, null },
                    { 168, 4.4000000000000004, 36, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1240), null, null, false, false, null, "Gold Coast Blonde Ale", 9, null },
                    { 169, 4.4000000000000004, 36, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1277), null, null, false, false, null, "Blackstone Stout", 3, null },
                    { 170, 4.4000000000000004, 36, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1328), null, null, false, false, null, "Honey-Raspberry Ale", 24, null },
                    { 341, 4.7000000000000002, 36, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8757), null, null, false, false, null, "American Wheat", 25, null },
                    { 685, 5.0, 36, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(8455), null, null, false, false, null, "Capitol Premium Pale Ale", 14, null },
                    { 51, 3.8999999999999999, 40, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3745), null, null, false, false, null, "Celis Raspberry", 7, null },
                    { 52, 3.8999999999999999, 40, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3847), null, null, false, false, null, "Celis White", 7, null },
                    { 53, 3.8999999999999999, 40, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3930), null, null, false, false, null, "Celis Pale Bock", 7, null },
                    { 35, 3.6000000000000001, 29, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3006), null, null, false, false, null, "Woody's Light", 14, null },
                    { 353, 4.7999999999999998, 28, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(165), null, null, false, false, null, "Blind Tiger Pale Ale", 14, null },
                    { 93, 4.0999999999999996, 28, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(6044), null, null, false, false, null, "Rust Belt Porter", 15, null },
                    { 58, 4.0, 28, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4177), null, null, false, false, null, "Whose Ear? Red Ale", 21, null },
                    { 67, 4.0, 20, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4596), null, null, false, false, null, "Catalina Common Lager", 10, null },
                    { 203, 4.5, 20, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4555), null, null, false, false, null, "First Flight Amber Ale", 21, null },
                    { 558, 5.0, 20, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9168), null, null, false, false, null, "Barnstormer Brown Ale", 4, null },
                    { 23, 3.5, 21, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1468), null, null, false, false, null, "1916 Irish Stout", 3, null },
                    { 49, 3.8999999999999999, 21, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3560), null, null, false, false, null, "Jack of Spades Schwarzbier", 19, null },
                    { 156, 4.4000000000000004, 21, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(230), null, null, false, false, null, "Bugeater Brown Ale", 4, null },
                    { 283, 4.5999999999999996, 21, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2882), null, null, false, false, null, "Princess of Darkness Porter", 15, null },
                    { 564, 5.0, 21, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9585), null, null, false, false, null, "Impromptu Pale Ale", 14, null },
                    { 24, 3.5, 22, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1524), null, null, false, false, null, "Boys of Summer Wheat", 10, null },
                    { 26, 3.5, 24, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1591), null, null, false, false, null, "Light Lager", 10, null },
                    { 72, 4.0, 24, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4850), null, null, false, false, null, "Unicorn Amber Ale", 21, null },
                    { 27, 3.5, 25, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1624), null, null, false, false, null, "Sea Dog Blue Paw Wheat", 8, null },
                    { 28, 3.5, 25, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1659), null, null, false, false, null, "Sea Dog Apricot Wheat", 8, null },
                    { 74, 4.0, 40, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(4956), null, null, false, false, null, "Hamtramck", 10, null },
                    { 250, 4.5, 25, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9073), null, null, false, false, null, "Sea Dog Raspberry Wheat Ale", 25, null },
                    { 31, 3.5, 26, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(2862), null, null, false, false, null, "Dos Perros", 4, null },
                    { 44, 3.7999999999999998, 26, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3362), null, null, false, false, null, "Onward Stout", 3, null },
                    { 45, 3.7999999999999998, 26, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(3394), null, null, false, false, null, "Onward Stout", 3, null },
                    { 696, 5.0, 26, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(155), null, null, false, false, null, "Hefeweizen", 23, null },
                    { 697, 5.0, 26, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(203), null, null, false, false, null, "Hefeweizen", 23, null },
                    { 33, 3.6000000000000001, 27, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(2926), null, null, false, false, null, "Bitter American", 13, null },
                    { 483, 5.0, 27, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(8134), null, null, false, false, null, "South Park Blonde", 9, null },
                    { 484, 5.0, 27, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(8166), null, null, false, false, null, "563 Stout", 3, null },
                    { 748, 5.2000000000000002, 27, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(9268), null, null, false, false, null, "Potrero ESB", 13, null },
                    { 749, 5.2000000000000002, 27, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(9312), null, null, false, false, null, "Amendment Pale Ale", 14, null },
                    { 952, 5.5, 27, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(7657), null, null, false, false, null, "General Pippo's Porter", 15, null },
                    { 953, 5.5, 27, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(7706), null, null, false, false, null, "Watermelon Wheat", 24, null },
                    { 34, 3.6000000000000001, 28, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(2968), null, null, false, false, null, "Flat Belly American Wheat", 10, null },
                    { 30, 3.5, 26, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(1721), null, null, false, false, null, "Dos Perros", 11, null },
                    { 453, 4.9000000000000004, 76, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3931), null, null, false, false, null, "Leinenkugel's Honey Weiss", 10, null },
                    { 454, 4.9000000000000004, 76, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(3997), null, null, false, false, null, "Red Lager", 10, null },
                    { 455, 4.9000000000000004, 76, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(4070), null, null, false, false, null, "Creamy Dark", 10, null },
                    { 784, 5.2000000000000002, 136, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(5472), null, null, false, false, null, "Paddle Out Stout", 3, null },
                    { 212, 4.5, 139, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5362), null, null, false, false, null, "Gold Rush", 14, null },
                    { 586, 5.0, 139, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(3684), null, null, false, false, null, "Red Nectar", 21, null },
                    { 214, 4.5, 140, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5535), null, null, false, false, null, "Chazz Cat Rye", 10, null },
                    { 588, 5.0, 140, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(3953), null, null, false, false, null, "Shining Star Pale Ale", 14, null },
                    { 217, 4.5, 141, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5833), null, null, false, false, null, "Brown Aged Girl", 33, null },
                    { 388, 4.7999999999999998, 141, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(5419), null, null, false, false, null, "Midnight Wit", 20, null },
                    { 218, 4.5, 142, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5943), null, null, false, false, null, "Lionshead", 18, null },
                    { 219, 4.5, 143, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6056), null, null, false, false, null, "Hocus Pocus", 25, null },
                    { 220, 4.5, 143, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6160), null, null, false, false, null, "Wacko", 8, null },
                    { 290, 4.5999999999999996, 143, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3557), null, null, false, false, null, "Odd Notion Spring 08", 31, null },
                    { 291, 4.5999999999999996, 143, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3664), null, null, false, false, null, "Howl", 10, null },
                    { 597, 5.0, 143, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(4935), null, null, false, false, null, "Single Chair Ale", 14, null },
                    { 792, 5.2000000000000002, 143, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(7053), null, null, false, false, null, "Circus Boy", 23, null },
                    { 875, 5.2999999999999998, 143, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(3915), null, null, false, false, null, "Feast of Fools", 8, null },
                    { 221, 4.5, 144, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6266), null, null, false, false, null, "Middle Ages Apricot Ale", 8, null },
                    { 292, 4.5999999999999996, 144, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3771), null, null, false, false, null, "Middle Ages Raspberry Ale", 8, null },
                    { 390, 4.7999999999999998, 144, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(5692), null, null, false, false, null, "Swallow Wit", 20, null },
                    { 606, 5.0, 144, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5825), null, null, false, false, null, "Highlander 80/-", 43, null },
                    { 607, 5.0, 144, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5939), null, null, false, false, null, "Syracuse Pale Ale", 14, null },
                    { 877, 5.2999999999999998, 144, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(4210), null, null, false, false, null, "Beast Bitter", 27, null },
                    { 222, 4.5, 145, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6376), null, null, false, false, null, "Rondy Brew 2009", 14, null },
                    { 223, 4.5, 145, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6492), null, null, false, false, null, "Old Whiskers", 23, null },
                    { 608, 5.0, 145, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(6073), null, null, false, false, null, "Earth - Belgian Style Chocolate Milk Stout", 40, null },
                    { 609, 5.0, 145, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(6191), null, null, false, false, null, "Arctic Rhino Coffee Porter", 15, null },
                    { 610, 5.0, 145, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(6309), null, null, false, false, null, "Oosik Amber Ale", 21, null },
                    { 611, 5.0, 145, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(6423), null, null, false, false, null, "Kodiak Brown Nut Brown Ale", 4, null },
                    { 380, 4.7999999999999998, 136, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(4445), null, null, false, false, null, "Mavericks Amber Ale", 21, null },
                    { 612, 5.0, 145, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(6586), null, null, false, false, null, "Greed", 35, null },
                    { 285, 4.5999999999999996, 136, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3058), null, null, false, false, null, "Pillar Point Pale Ale", 14, null },
                    { 995, 5.5, 134, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(5551), null, null, false, false, null, "IPA", 41, null },
                    { 183, 4.5, 121, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2616), null, null, false, false, null, "Oatmeal Stout", 29, null },
                    { 516, 5.0, 121, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(2753), null, null, false, false, null, "Fresh Hops Pale Ale", 14, null },
                    { 700, 5.0, 121, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(601), null, null, false, false, null, "Oktoberfest", 34, null },
                    { 851, 5.2999999999999998, 121, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(8780), null, null, false, false, null, "Toasted Lager", 10, null },
                    { 184, 4.5, 122, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2713), null, null, false, false, null, "Colonel Paris Pale Ale", 14, null },
                    { 192, 4.5, 127, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3500), null, null, false, false, null, "Cricket's Nocturne", 32, null },
                    { 362, 4.7999999999999998, 127, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(1451), null, null, false, false, null, "Fall Festivus Ale", 21, null },
                    { 986, 5.5, 127, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(4089), null, null, false, false, null, "Colonel Blide's Cask Ale", 16, null },
                    { 194, 4.5, 129, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3704), null, null, false, false, null, "Geary's Pale Ale", 13, null },
                    { 195, 4.5, 130, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3800), null, null, false, false, null, "Tres Blueberry Stout", 3, null },
                    { 196, 4.5, 131, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(3903), null, null, false, false, null, "Cascade Golden Ale", 14, null },
                    { 363, 4.7999999999999998, 131, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(1547), null, null, false, false, null, "Buzzsaw Brown", 4, null },
                    { 543, 5.0, 131, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(7461), null, null, false, false, null, "Mirrorpond Pale Ale", 14, null },
                    { 544, 5.0, 131, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(7562), null, null, false, false, null, "Twilight Ale", 14, null },
                    { 776, 5.2000000000000002, 131, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(4295), null, null, false, false, null, "Black Butte Porter", 15, null },
                    { 777, 5.2000000000000002, 131, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(4406), null, null, false, false, null, "Green Lakes Organic Ale", 21, null },
                    { 857, 5.2999999999999998, 131, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(199), null, null, false, false, null, "Bachelor ESB", 27, null },
                    { 918, 5.4000000000000004, 131, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(2601), null, null, false, false, null, "Bachelor Bitter", 16, null },
                    { 919, 5.4000000000000004, 131, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(2716), null, null, false, false, null, "Cinder Cone Red", 21, null },
                    { 988, 5.5, 131, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(4343), null, null, false, false, null, "Hop Trip Fresh Hop Pale Ale", 14, null },
                    { 197, 4.5, 132, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4011), null, null, false, false, null, "Breath of the Dragon English Bitter", 26, null },
                    { 548, 5.0, 132, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8183), null, null, false, false, null, "Erik the Red", 21, null },
                    { 549, 5.0, 132, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8288), null, null, false, false, null, "Crooked Door", 21, null },
                    { 706, 5.0999999999999996, 132, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(1491), null, null, false, false, null, "Nagelweiss", 25, null },
                    { 198, 4.5, 133, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4119), null, null, false, false, null, "East End Witte", 20, null },
                    { 199, 4.5, 134, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4312), null, null, false, false, null, "Luna Weizen", 10, null },
                    { 994, 5.5, 134, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(5425), null, null, false, false, null, "Alembic Pale", 14, null },
                    { 208, 4.5, 136, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4976), null, null, false, false, null, "Sandy Beach Blonde Hefeweizen", 23, null },
                    { 798, 5.2000000000000002, 145, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(7911), null, null, false, false, null, "Fahrwasser Fairway Pilsner", 18, null },
                    { 226, 4.5, 146, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6638), null, null, false, false, null, "Bright Spot Golden Ale", 9, null },
                    { 293, 4.5999999999999996, 146, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3888), null, null, false, false, null, "West Bank Pub Ale", 26, null },
                    { 690, 5.0, 164, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(9209), null, null, false, false, null, "Blanche", 20, null },
                    { 948, 5.4000000000000004, 164, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(7258), null, null, false, false, null, "AutumnFest", 34, null },
                    { 269, 4.5, 165, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(1043), null, null, false, false, null, "St. Charles ESB", 27, null },
                    { 424, 4.7999999999999998, 165, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(304), null, null, false, false, null, "Wixa Weiss", 25, null },
                    { 694, 5.0, 165, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(13), null, null, false, false, null, "Two Guns Pilsner", 18, null },
                    { 827, 5.2000000000000002, 165, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(4349), null, null, false, false, null, "Railyard Ale", 34, null },
                    { 898, 5.2999999999999998, 165, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(9125), null, null, false, false, null, "B3K Schwarzbier", 19, null },
                    { 271, 4.5, 166, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(1331), null, null, false, false, null, "Doc's Hard Apple Cider", 8, null },
                    { 272, 4.5999999999999996, 167, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(1577), null, null, false, false, null, "Puffers Smoked Porter", 15, null },
                    { 273, 4.5999999999999996, 167, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(1860), null, null, false, false, null, "Righteous Red", 21, null },
                    { 274, 4.5999999999999996, 167, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(1983), null, null, false, false, null, "Bankers Gold", 9, null },
                    { 348, 4.7999999999999998, 167, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(9492), null, null, false, false, null, "Patriots Pilsner", 18, null },
                    { 428, 4.9000000000000004, 167, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(723), null, null, false, false, null, "Raspberry Brown Ale", 4, null },
                    { 429, 4.9000000000000004, 167, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(854), null, null, false, false, null, "Workingman's Wheat", 23, null },
                    { 754, 5.2000000000000002, 167, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(70), null, null, false, false, null, "Honey Pilsner", 18, null },
                    { 831, 5.2000000000000002, 167, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(5293), null, null, false, false, null, "North Pleasant Pale Ale", 14, null },
                    { 832, 5.2000000000000002, 167, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(5416), null, null, false, false, null, "Massatucky Brown", 4, null },
                    { 843, 5.2999999999999998, 167, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(7786), null, null, false, false, null, "Cascade IPA", 41, null },
                    { 900, 5.2999999999999998, 167, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(9602), null, null, false, false, null, "Amherst ESB", 27, null },
                    { 903, 5.4000000000000004, 167, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(45), null, null, false, false, null, "Lewmeister Oktoberfest", 34, null },
                    { 904, 5.4000000000000004, 167, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(180), null, null, false, false, null, "Heather Ale", 22, null },
                    { 960, 5.5, 167, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(9262), null, null, false, false, null, "Graduation Ale", 14, null },
                    { 277, 4.5999999999999996, 168, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2232), null, null, false, false, null, "Grain Belt", 10, null },
                    { 282, 4.5999999999999996, 170, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2843), null, null, false, false, null, "Firestole Pale 31", 14, null },
                    { 556, 5.0, 170, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9085), null, null, false, false, null, "Double Barrel Ale", 21, null },
                    { 287, 4.5999999999999996, 171, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(3243), null, null, false, false, null, "Rolling Rock", 1, null },
                    { 294, 4.5999999999999996, 173, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4022), null, null, false, false, null, "Easy Street Wheat", 25, null },
                    { 689, 5.0, 164, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(9077), null, null, false, false, null, "Golf", 23, null },
                    { 268, 4.5, 164, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(913), null, null, false, false, null, "House Ale", 21, null },
                    { 266, 4.5, 163, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(729), null, null, false, false, null, "Upland Wheat Ale", 25, null },
                    { 945, 5.4000000000000004, 161, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(6846), null, null, false, false, null, "Hathor Red Lager", 10, null },
                    { 227, 4.5, 147, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6749), null, null, false, false, null, "Schmitz Pale Ale No. 5", 14, null },
                    { 228, 4.5, 147, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6864), null, null, false, false, null, "Dells Chief Amber Ale", 21, null },
                    { 229, 4.5, 147, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(6972), null, null, false, false, null, "Pilsner", 10, null },
                    { 623, 5.0, 147, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(8130), null, null, false, false, null, "Weissbier", 23, null },
                    { 234, 4.5, 149, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7554), null, null, false, false, null, "Horseshoe Hefeweizen", 23, null },
                    { 235, 4.5, 149, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7665), null, null, false, false, null, "Forty-Niner Gold Lager", 10, null },
                    { 236, 4.5, 150, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7773), null, null, false, false, null, "California Gold", 10, null },
                    { 399, 4.7999999999999998, 150, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(6921), null, null, false, false, null, "Paradise Pale Ale", 14, null },
                    { 628, 5.0, 150, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(8947), null, null, false, false, null, "Sunset Amber Ale", 21, null },
                    { 237, 4.5, 151, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(7889), null, null, false, false, null, "Orlio Seasonal Black Lager", 19, null },
                    { 934, 5.4000000000000004, 151, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(5132), null, null, false, false, null, "Orlio Seasonal India Pale Ale", 41, null },
                    { 239, 4.5, 152, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8068), null, null, false, false, null, "Kenziger", 14, null },
                    { 637, 5.0, 152, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(139), null, null, false, false, null, "Walt Wit", 20, null },
                    { 508, 5.0, 119, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(1319), null, null, false, false, null, "Headbangerz Brown Ale", 4, null },
                    { 638, 5.0, 152, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(270), null, null, false, false, null, "Rowhouse Red", 30, null },
                    { 242, 4.5, 153, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8383), null, null, false, false, null, "Rahr's Red", 21, null },
                    { 243, 4.5, 153, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8503), null, null, false, false, null, "Ugly Pug", 19, null },
                    { 244, 4.5, 153, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8616), null, null, false, false, null, "Blonde Lager", 10, null },
                    { 247, 4.5, 155, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8909), null, null, false, false, null, "Aud Blonde", 9, null },
                    { 251, 4.5, 156, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9202), null, null, false, false, null, "Pumpkinhead Ale", 36, null },
                    { 734, 5.0999999999999996, 156, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(7104), null, null, false, false, null, "Export Ale", 14, null },
                    { 255, 4.5, 157, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9513), null, null, false, false, null, "Hop Sun", 25, null },
                    { 256, 4.5, 157, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9645), null, null, false, false, null, "Raspberry Wheat", 25, null },
                    { 465, 4.9000000000000004, 157, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(5626), null, null, false, false, null, "Chautauqua Brew", 21, null },
                    { 660, 5.0, 157, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(4871), null, null, false, false, null, "422 Pale Wheat Ale", 25, null },
                    { 819, 5.2000000000000002, 157, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(2557), null, null, false, false, null, "Southern Tier Porter", 15, null }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "Id", "ABV", "BreweryId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "IsUnlisted", "ModifiedOn", "Name", "StyleId", "UnlistedOn" },
                values: new object[,]
                {
                    { 258, 4.5, 158, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9846), null, null, false, false, null, "Tropical Mango", 14, null },
                    { 264, 4.5, 161, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(487), null, null, false, false, null, "Yorkshire Stout", 3, null },
                    { 241, 4.5, 153, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8263), null, null, false, false, null, "Buffalo Butt", 21, null },
                    { 181, 4.5, 119, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2413), null, null, false, false, null, "Hammerin' Ale", 21, null },
                    { 435, 4.9000000000000004, 118, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(1587), null, null, false, false, null, "RedLegg Ale", 21, null },
                    { 278, 4.5999999999999996, 118, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2326), null, null, false, false, null, "Cumberland Pale Ale", 14, null },
                    { 555, 5.0, 93, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8956), null, null, false, false, null, "Saranac Mountain Ale", 8, null },
                    { 709, 5.0999999999999996, 93, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(2101), null, null, false, false, null, "Saranac Golden Pilsner", 18, null },
                    { 862, 5.2999999999999998, 93, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(1084), null, null, false, false, null, "Saranac Belgian White", 20, null },
                    { 863, 5.2999999999999998, 93, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(1172), null, null, false, false, null, "Saranac Black Forest", 19, null },
                    { 864, 5.2999999999999998, 93, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(1321), null, null, false, false, null, "Saranac Season's Best", 10, null },
                    { 923, 5.4000000000000004, 93, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(3293), null, null, false, false, null, "Saranac Caramel Porter", 15, null },
                    { 924, 5.4000000000000004, 93, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(3371), null, null, false, false, null, "Saranac Nut Brown Ale", 4, null },
                    { 925, 5.4000000000000004, 93, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(3466), null, null, false, false, null, "Saranac Octoberfest", 34, null },
                    { 926, 5.4000000000000004, 93, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(3563), null, null, false, false, null, "Saranac Pumpkin Ale", 36, null },
                    { 997, 5.5, 93, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(5906), null, null, false, false, null, "Saranac Extra Special Bitter", 27, null },
                    { 998, 5.5, 93, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(6011), null, null, false, false, null, "Saranac Single Malt", 43, null },
                    { 999, 5.5, 93, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(6094), null, null, false, false, null, "Saranac Pale Ale", 14, null },
                    { 1000, 5.5, 93, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(6197), null, null, false, false, null, "Roggen Bock", 44, null },
                    { 140, 4.2999999999999998, 94, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9037), null, null, false, false, null, "Gordon Biersch Schwarzbier", 19, null },
                    { 141, 4.2999999999999998, 94, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9120), null, null, false, false, null, "Schwarzbier", 19, null },
                    { 206, 4.5, 94, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(4793), null, null, false, false, null, "Premium Light Lager", 10, null },
                    { 142, 4.2999999999999998, 95, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9201), null, null, false, false, null, "Pioneer Peak Porter", 15, null },
                    { 143, 4.2999999999999998, 96, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9278), null, null, false, false, null, "Harpoon Brown Session Ale", 4, null },
                    { 381, 4.7999999999999998, 96, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(4534), null, null, false, false, null, "Harpoon UFO Hefeweizen", 23, null },
                    { 382, 4.7999999999999998, 96, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(4611), null, null, false, false, null, "UFO Raspberry Hefeweizen", 8, null },
                    { 580, 5.0, 96, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(2400), null, null, false, false, null, "Harpoon Ale", 14, null },
                    { 144, 4.2999999999999998, 97, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9364), null, null, false, false, null, "Smashing Berry Ale", 8, null },
                    { 146, 4.2999999999999998, 98, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9522), null, null, false, false, null, "Bam Noire", 30, null },
                    { 215, 4.5, 98, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5628), null, null, false, false, null, "Bam Bière", 30, null },
                    { 216, 4.5, 98, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(5715), null, null, false, false, null, "Weizen Bam Bière", 25, null },
                    { 324, 4.7000000000000002, 98, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6897), null, null, false, false, null, "E.S. Bam", 30, null },
                    { 384, 4.7999999999999998, 98, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(4851), null, null, false, false, null, "Calabaza Blanca", 7, null },
                    { 554, 5.0, 93, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(8881), null, null, false, false, null, "Saranac Mocha Stout", 3, null },
                    { 371, 4.7999999999999998, 93, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(3169), null, null, false, false, null, "Saranac Lager", 10, null },
                    { 317, 4.7000000000000002, 93, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6142), null, null, false, false, null, "Pomegranate Wheat", 8, null },
                    { 316, 4.7000000000000002, 93, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(6068), null, null, false, false, null, "Saranac Summer Ale", 25, null },
                    { 791, 5.2000000000000002, 76, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(6935), null, null, false, false, null, "1888 Bock", 44, null },
                    { 118, 4.2000000000000002, 77, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7454), null, null, false, false, null, "Wild Country Raspberry Wheat", 8, null },
                    { 119, 4.2000000000000002, 78, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7523), null, null, false, false, null, "Skinny Dip", 9, null },
                    { 394, 4.7999999999999998, 78, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(6370), null, null, false, false, null, "Blue Paddle Pilsener", 18, null },
                    { 395, 4.7999999999999998, 78, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(6450), null, null, false, false, null, "Sunshine Wheat", 25, null },
                    { 120, 4.2000000000000002, 79, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7592), null, null, false, false, null, "Totally Naked", 10, null },
                    { 396, 4.7999999999999998, 79, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(6525), null, null, false, false, null, "Spotted Cow", 21, null },
                    { 121, 4.2000000000000002, 80, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7662), null, null, false, false, null, "Ring Tail Pale Ale", 14, null },
                    { 240, 4.5, 80, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(8142), null, null, false, false, null, "Cascade Razberry Wheat", 8, null },
                    { 811, 5.2000000000000002, 80, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(235), null, null, false, false, null, "Celtic Copper Ale", 21, null },
                    { 122, 4.2000000000000002, 81, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7727), null, null, false, false, null, "Nut Brown Ale", 4, null },
                    { 254, 4.5, 81, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9389), null, null, false, false, null, "Honey Pilsner", 10, null },
                    { 302, 4.5999999999999996, 81, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4497), null, null, false, false, null, "Red Lager", 10, null },
                    { 147, 4.2999999999999998, 99, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9603), null, null, false, false, null, "Old Seattle Lager", 10, null },
                    { 123, 4.2000000000000002, 82, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(7805), null, null, false, false, null, "Hefe Weiss", 23, null },
                    { 662, 5.0, 82, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(5257), null, null, false, false, null, "Special Amber", 10, null },
                    { 128, 4.2000000000000002, 85, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8128), null, null, false, false, null, "Bistro Blonde", 9, null },
                    { 427, 4.7999999999999998, 85, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(607), null, null, false, false, null, "Amber Waves", 21, null },
                    { 687, 5.0, 85, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(8708), null, null, false, false, null, "Padawan Pale Ale", 14, null },
                    { 133, 4.2999999999999998, 87, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8485), null, null, false, false, null, "Cambridge Amber", 21, null },
                    { 441, 4.9000000000000004, 87, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(2383), null, null, false, false, null, "Tall Tale Pale Ale", 14, null },
                    { 135, 4.2999999999999998, 89, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8628), null, null, false, false, null, "Nine Men Ale", 9, null },
                    { 280, 4.5999999999999996, 89, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2629), null, null, false, false, null, "Strike Out Stout", 3, null },
                    { 985, 5.5, 89, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(3984), null, null, false, false, null, "Old Slugger Pale Ale", 14, null },
                    { 136, 4.2999999999999998, 90, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8705), null, null, false, false, null, "Lighthouse Ale", 14, null },
                    { 364, 4.7999999999999998, 90, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(1617), null, null, false, false, null, "Steamer Glide Stout", 3, null },
                    { 138, 4.2999999999999998, 92, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8868), null, null, false, false, null, "Hefeweizen", 23, null },
                    { 139, 4.2999999999999998, 93, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(8955), null, null, false, false, null, "Saranac Oatmeal Stout", 29, null },
                    { 257, 4.5, 82, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(9713), null, null, false, false, null, "Sprecher Pub Ale", 4, null },
                    { 830, 5.2000000000000002, 381, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(5165), null, null, false, false, null, "Oatmeal Stout", 3, null },
                    { 598, 5.0, 99, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(5026), null, null, false, false, null, "Islander Pale Ale", 14, null },
                    { 795, 5.2000000000000002, 99, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(7487), null, null, false, false, null, "Clipper Gold Hefeweizen", 10, null },
                    { 166, 4.4000000000000004, 113, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1112), null, null, false, false, null, "Levitation Ale", 21, null },
                    { 942, 5.4000000000000004, 113, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(6391), null, null, false, false, null, "Stone Pale Ale", 14, null },
                    { 167, 4.4000000000000004, 114, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1198), null, null, false, false, null, "Rugged Trail Ale", 4, null },
                    { 418, 4.7999999999999998, 114, new DateTime(2020, 10, 28, 5, 23, 41, 401, DateTimeKind.Utc).AddTicks(9462), null, null, false, false, null, "Dreamweaver", 25, null },
                    { 471, 4.9000000000000004, 114, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(6122), null, null, false, false, null, "Scratch #11 2008", 16, null },
                    { 472, 4.9000000000000004, 114, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(6229), null, null, false, false, null, "Scratch #13 2008", 34, null },
                    { 678, 5.0, 114, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(7269), null, null, false, false, null, "Bavarian Lager", 10, null },
                    { 743, 5.0999999999999996, 114, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(8638), null, null, false, false, null, "Scratch #6 2007", 10, null },
                    { 895, 5.2999999999999998, 114, new DateTime(2020, 10, 28, 5, 23, 41, 409, DateTimeKind.Utc).AddTicks(8552), null, null, false, false, null, "Sunshine Pils", 18, null },
                    { 946, 5.4000000000000004, 114, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(6943), null, null, false, false, null, "Troegs Pale Ale", 14, null },
                    { 174, 4.4000000000000004, 115, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1532), null, null, false, false, null, "Pale Ale", 14, null },
                    { 177, 4.5, 116, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2019), null, null, false, false, null, "Mountain Lager", 10, null },
                    { 178, 4.5, 116, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2119), null, null, false, false, null, "Water Gap Wheat", 25, null },
                    { 276, 4.5999999999999996, 116, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(2109), null, null, false, false, null, "Susquehanna Stout", 3, null },
                    { 349, 4.7999999999999998, 116, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(9711), null, null, false, false, null, "Celtic Knot Irish Red", 21, null },
                    { 350, 4.7999999999999998, 116, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(9910), null, null, false, false, null, "Peregrine Pilsner", 18, null },
                    { 431, 4.9000000000000004, 116, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(1010), null, null, false, false, null, "Purist Pale Ale", 14, null },
                    { 757, 5.2000000000000002, 116, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(323), null, null, false, false, null, "Jolly Scot Scottish Ale", 43, null },
                    { 758, 5.2000000000000002, 116, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(457), null, null, false, false, null, "Kipona Fest", 34, null },
                    { 846, 5.2999999999999998, 116, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(8027), null, null, false, false, null, "Trail Blaze Organic Brown Ale", 4, null },
                    { 847, 5.2999999999999998, 116, new DateTime(2020, 10, 28, 5, 23, 41, 408, DateTimeKind.Utc).AddTicks(8130), null, null, false, false, null, "Hinterland Hefe Weizen", 23, null },
                    { 906, 5.4000000000000004, 116, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(344), null, null, false, false, null, "Pennypacker Porter", 15, null },
                    { 179, 4.5, 117, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2220), null, null, false, false, null, "Dirty Blond", 25, null },
                    { 500, 5.0, 117, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(141), null, null, false, false, null, "Rost", 21, null },
                    { 501, 5.0, 117, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(234), null, null, false, false, null, "Hell", 10, null },
                    { 759, 5.2000000000000002, 117, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(554), null, null, false, false, null, "Dunkel", 10, null },
                    { 180, 4.5, 118, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2313), null, null, false, false, null, "Duveneck's Dortmunder", 10, null },
                    { 941, 5.4000000000000004, 112, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(6293), null, null, false, false, null, "Shiner Hefeweizen", 23, null },
                    { 466, 4.9000000000000004, 112, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(5715), null, null, false, false, null, "Shiner Bohemian Black Lager", 10, null },
                    { 165, 4.4000000000000004, 112, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(1017), null, null, false, false, null, "Shiner Blonde", 10, null },
                    { 164, 4.4000000000000004, 112, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(929), null, null, false, false, null, "Shiner Bock", 10, null },
                    { 148, 4.2999999999999998, 100, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9680), null, null, false, false, null, "Nut Brown", 4, null },
                    { 301, 4.5999999999999996, 100, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4426), null, null, false, false, null, "Hefe Weizen", 10, null },
                    { 733, 5.0999999999999996, 100, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(6980), null, null, false, false, null, "Amber Ale", 4, null },
                    { 149, 4.2999999999999998, 101, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9766), null, null, false, false, null, "Weissbier", 10, null },
                    { 680, 5.0, 101, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(7581), null, null, false, false, null, "N.W. Pale Ale", 14, null },
                    { 681, 5.0, 101, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(7689), null, null, false, false, null, "Bock", 44, null },
                    { 151, 4.2999999999999998, 102, new DateTime(2020, 10, 28, 5, 23, 41, 398, DateTimeKind.Utc).AddTicks(9916), null, null, false, false, null, "Yards Brawler", 26, null },
                    { 270, 4.5, 102, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(1120), null, null, false, false, null, "Yards Love Stout", 3, null },
                    { 304, 4.5999999999999996, 102, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(4705), null, null, false, false, null, "Yards Philadelphia Pale Ale", 14, null },
                    { 342, 4.7000000000000002, 102, new DateTime(2020, 10, 28, 5, 23, 41, 400, DateTimeKind.Utc).AddTicks(8850), null, null, false, false, null, "Yards Saison", 30, null },
                    { 695, 5.0, 102, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(109), null, null, false, false, null, "Poor Richard's Tavern Spruce Ale", 22, null },
                    { 153, 4.4000000000000004, 103, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(31), null, null, false, false, null, "Bouldevard ZŌN", 20, null },
                    { 185, 4.5, 103, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(2806), null, null, false, false, null, "Boulevard Unfiltered Wheat", 25, null },
                    { 794, 5.2000000000000002, 99, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(7402), null, null, false, false, null, "Flagship Red Alt Ale", 21, null },
                    { 438, 4.9000000000000004, 103, new DateTime(2020, 10, 28, 5, 23, 41, 402, DateTimeKind.Utc).AddTicks(2056), null, null, false, false, null, "Boulevard Dry Stout", 3, null },
                    { 910, 5.4000000000000004, 103, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(1298), null, null, false, false, null, "Bully! Porter", 15, null },
                    { 911, 5.4000000000000004, 103, new DateTime(2020, 10, 28, 5, 23, 41, 410, DateTimeKind.Utc).AddTicks(1392), null, null, false, false, null, "Boulevard Pale Ale", 14, null },
                    { 154, 4.4000000000000004, 104, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(116), null, null, false, false, null, "Silk Lady", 14, null },
                    { 778, 5.2000000000000002, 104, new DateTime(2020, 10, 28, 5, 23, 41, 407, DateTimeKind.Utc).AddTicks(4491), null, null, false, false, null, "Danger Ale", 4, null },
                    { 989, 5.5, 104, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(4451), null, null, false, false, null, "IPA", 41, null },
                    { 155, 4.4000000000000004, 105, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(199), null, null, false, false, null, "Lighthouse Ale", 10, null },
                    { 157, 4.4000000000000004, 106, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(314), null, null, false, false, null, "Northern Light Lager", 10, null },
                    { 565, 5.0, 106, new DateTime(2020, 10, 28, 5, 23, 41, 403, DateTimeKind.Utc).AddTicks(9668), null, null, false, false, null, "Broad Axe Stout", 3, null },
                    { 711, 5.0999999999999996, 106, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(2456), null, null, false, false, null, "Duke IPA", 41, null },
                    { 160, 4.4000000000000004, 108, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(564), null, null, false, false, null, "Scrimshaw Pilsner Style Beer", 18, null },
                    { 626, 5.0, 108, new DateTime(2020, 10, 28, 5, 23, 41, 404, DateTimeKind.Utc).AddTicks(8653), null, null, false, false, null, "Acme California Pale Ale", 14, null },
                    { 163, 4.4000000000000004, 111, new DateTime(2020, 10, 28, 5, 23, 41, 399, DateTimeKind.Utc).AddTicks(840), null, null, false, false, null, "Unfiltered Wheat Beer", 25, null },
                    { 659, 5.0, 111, new DateTime(2020, 10, 28, 5, 23, 41, 405, DateTimeKind.Utc).AddTicks(4741), null, null, false, false, null, "Summerfest", 18, null },
                    { 703, 5.0999999999999996, 103, new DateTime(2020, 10, 28, 5, 23, 41, 406, DateTimeKind.Utc).AddTicks(909), null, null, false, false, null, "Lunar Ale", 4, null },
                    { 981, 5.5, 412, new DateTime(2020, 10, 28, 5, 23, 41, 411, DateTimeKind.Utc).AddTicks(3318), null, null, false, false, null, "Carnegie Stark-Porter", 15, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { 33, 1 });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { 34, 2 });

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 80);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 81);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 82);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 83);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 84);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 85);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 86);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 87);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 88);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 89);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 90);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 91);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 92);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 93);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 94);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 95);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 96);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 97);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 98);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 99);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 100);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 101);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 102);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 103);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 104);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 105);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 106);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 107);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 108);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 109);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 110);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 111);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 112);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 113);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 114);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 115);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 116);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 117);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 118);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 119);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 120);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 121);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 122);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 123);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 124);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 126);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 127);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 128);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 129);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 130);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 131);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 132);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 133);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 134);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 135);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 136);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 137);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 138);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 139);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 140);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 141);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 142);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 143);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 144);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 145);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 146);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 147);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 148);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 149);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 150);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 151);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 152);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 153);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 154);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 155);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 156);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 157);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 158);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 159);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 160);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 161);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 162);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 163);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 164);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 165);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 166);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 167);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 168);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 169);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 170);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 171);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 172);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 173);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 174);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 175);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 176);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 177);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 178);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 179);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 180);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 181);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 182);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 183);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 184);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 185);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 186);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 187);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 188);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 189);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 190);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 191);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 192);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 193);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 194);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 195);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 196);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 197);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 198);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 199);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 200);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 201);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 202);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 203);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 204);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 205);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 206);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 207);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 208);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 209);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 210);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 211);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 212);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 213);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 214);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 215);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 216);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 217);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 218);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 219);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 220);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 221);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 222);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 223);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 224);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 225);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 226);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 227);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 228);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 229);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 230);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 231);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 232);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 233);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 234);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 235);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 236);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 237);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 238);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 239);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 240);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 241);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 242);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 243);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 244);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 245);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 246);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 247);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 248);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 249);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 250);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 251);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 252);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 253);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 254);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 255);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 256);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 257);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 258);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 259);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 260);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 261);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 262);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 263);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 264);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 265);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 266);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 267);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 268);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 269);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 270);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 271);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 272);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 273);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 274);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 275);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 276);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 277);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 278);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 279);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 280);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 281);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 282);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 283);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 284);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 285);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 286);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 287);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 288);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 289);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 290);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 291);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 292);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 293);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 294);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 295);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 296);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 297);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 298);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 299);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 300);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 301);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 302);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 303);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 304);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 305);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 306);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 307);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 308);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 309);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 310);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 311);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 312);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 313);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 314);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 315);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 316);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 317);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 318);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 319);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 320);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 321);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 322);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 323);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 324);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 325);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 326);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 327);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 328);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 329);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 330);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 331);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 332);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 333);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 334);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 335);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 336);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 337);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 338);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 339);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 340);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 341);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 342);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 343);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 344);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 345);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 346);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 347);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 348);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 349);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 350);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 351);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 352);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 353);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 354);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 355);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 356);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 357);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 358);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 359);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 360);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 361);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 362);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 363);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 364);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 365);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 366);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 367);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 368);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 369);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 370);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 371);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 372);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 373);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 374);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 375);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 376);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 377);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 378);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 379);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 380);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 381);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 382);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 383);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 384);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 385);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 386);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 387);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 388);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 389);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 390);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 391);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 392);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 393);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 394);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 395);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 396);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 397);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 398);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 399);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 400);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 401);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 402);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 403);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 404);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 405);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 406);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 407);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 408);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 409);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 410);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 411);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 412);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 413);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 414);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 415);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 416);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 417);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 418);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 419);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 420);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 421);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 422);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 423);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 424);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 425);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 426);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 427);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 428);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 429);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 430);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 431);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 432);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 433);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 434);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 435);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 436);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 437);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 438);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 439);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 440);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 441);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 442);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 443);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 444);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 445);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 446);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 447);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 448);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 449);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 450);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 451);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 452);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 453);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 454);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 455);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 456);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 457);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 458);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 459);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 460);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 461);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 462);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 463);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 464);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 465);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 466);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 467);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 468);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 469);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 470);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 471);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 472);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 473);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 474);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 475);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 476);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 477);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 478);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 479);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 480);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 481);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 482);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 483);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 484);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 485);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 486);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 487);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 488);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 489);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 490);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 491);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 492);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 493);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 494);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 495);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 496);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 497);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 498);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 499);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 500);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 501);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 502);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 503);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 504);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 505);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 506);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 507);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 508);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 509);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 510);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 511);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 512);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 513);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 514);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 515);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 516);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 517);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 518);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 519);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 520);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 521);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 522);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 523);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 524);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 525);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 526);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 527);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 528);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 529);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 530);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 531);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 532);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 533);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 534);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 535);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 536);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 537);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 538);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 539);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 540);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 541);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 542);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 543);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 544);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 545);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 546);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 547);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 548);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 549);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 550);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 551);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 552);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 553);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 554);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 555);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 556);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 557);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 558);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 559);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 560);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 561);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 562);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 563);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 564);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 565);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 566);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 567);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 568);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 569);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 570);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 571);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 572);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 573);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 574);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 575);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 576);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 577);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 578);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 579);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 580);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 581);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 582);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 583);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 584);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 585);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 586);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 587);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 588);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 589);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 590);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 591);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 592);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 593);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 594);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 595);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 596);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 597);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 598);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 599);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 600);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 601);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 602);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 603);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 604);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 605);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 606);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 607);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 608);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 609);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 610);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 611);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 612);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 613);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 614);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 615);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 616);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 617);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 618);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 619);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 620);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 621);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 622);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 623);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 624);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 625);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 626);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 627);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 628);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 629);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 630);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 631);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 632);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 633);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 634);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 635);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 636);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 637);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 638);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 639);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 640);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 641);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 642);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 643);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 644);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 645);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 646);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 647);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 648);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 649);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 650);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 651);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 652);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 653);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 654);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 655);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 656);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 657);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 658);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 659);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 660);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 661);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 662);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 663);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 664);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 665);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 666);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 667);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 668);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 669);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 670);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 671);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 672);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 673);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 674);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 675);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 676);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 677);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 678);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 679);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 680);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 681);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 682);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 683);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 684);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 685);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 686);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 687);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 688);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 689);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 690);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 691);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 692);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 693);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 694);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 695);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 696);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 697);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 698);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 699);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 700);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 701);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 702);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 703);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 704);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 705);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 706);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 707);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 708);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 709);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 710);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 711);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 712);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 713);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 714);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 715);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 716);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 717);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 718);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 719);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 720);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 721);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 722);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 723);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 724);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 725);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 726);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 727);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 728);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 729);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 730);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 731);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 732);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 733);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 734);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 735);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 736);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 737);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 738);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 739);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 740);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 741);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 742);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 743);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 744);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 745);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 746);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 747);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 748);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 749);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 750);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 751);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 752);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 753);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 754);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 755);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 756);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 757);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 758);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 759);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 760);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 761);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 762);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 763);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 764);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 765);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 766);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 767);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 768);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 769);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 770);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 771);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 772);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 773);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 774);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 775);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 776);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 777);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 778);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 779);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 780);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 781);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 782);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 783);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 784);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 785);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 786);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 787);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 788);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 789);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 790);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 791);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 792);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 793);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 794);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 795);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 796);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 797);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 798);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 799);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 800);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 801);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 802);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 803);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 804);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 805);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 806);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 807);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 808);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 809);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 810);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 811);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 812);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 813);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 814);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 815);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 816);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 817);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 818);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 819);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 820);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 821);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 822);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 823);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 824);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 825);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 826);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 827);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 828);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 829);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 830);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 831);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 832);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 833);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 834);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 835);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 836);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 837);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 838);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 839);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 840);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 841);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 842);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 843);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 844);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 845);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 846);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 847);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 848);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 849);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 850);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 851);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 852);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 853);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 854);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 855);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 856);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 857);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 858);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 859);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 860);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 861);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 862);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 863);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 864);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 865);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 866);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 867);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 868);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 869);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 870);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 871);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 872);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 873);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 874);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 875);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 876);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 877);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 878);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 879);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 880);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 881);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 882);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 883);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 884);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 885);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 886);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 887);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 888);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 889);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 890);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 891);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 892);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 893);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 894);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 895);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 896);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 897);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 898);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 899);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 900);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 901);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 902);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 903);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 904);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 905);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 906);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 907);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 908);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 909);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 910);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 911);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 912);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 913);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 914);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 915);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 916);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 917);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 918);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 919);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 920);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 921);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 922);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 923);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 924);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 925);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 926);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 927);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 928);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 929);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 930);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 931);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 932);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 933);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 934);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 935);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 936);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 937);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 938);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 939);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 940);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 941);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 942);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 943);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 944);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 945);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 946);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 947);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 948);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 949);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 950);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 951);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 952);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 953);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 954);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 955);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 956);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 957);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 958);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 959);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 960);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 961);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 962);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 963);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 964);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 965);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 966);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 967);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 968);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 969);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 970);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 971);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 972);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 973);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 974);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 975);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 976);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 977);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 978);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 979);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 980);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 981);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 982);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 983);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 984);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 985);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 986);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 987);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 988);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 989);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 990);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 991);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 992);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 993);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 994);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 995);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 996);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 997);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 998);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 999);

            migrationBuilder.DeleteData(
                table: "Beers",
                keyColumn: "Id",
                keyValue: 1000);

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 80);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 81);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 82);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 83);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 84);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 85);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 86);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 87);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 88);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 89);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 90);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 91);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 92);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 93);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 94);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 95);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 96);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 97);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 98);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 99);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 100);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 101);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 102);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 103);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 104);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 105);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 106);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 107);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 108);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 109);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 110);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 111);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 112);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 113);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 114);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 115);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 116);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 117);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 118);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 119);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 120);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 121);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 122);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 123);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 124);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 126);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 127);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 128);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 129);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 130);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 131);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 132);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 133);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 134);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 135);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 136);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 137);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 138);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 139);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 140);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 141);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 142);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 143);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 144);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 145);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 146);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 147);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 148);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 149);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 150);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 151);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 152);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 153);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 154);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 155);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 156);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 157);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 158);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 159);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 160);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 161);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 162);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 163);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 164);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 165);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 166);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 167);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 168);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 169);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 170);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 171);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 172);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 173);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 174);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 175);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 176);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 177);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 178);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 179);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 180);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 181);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 182);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 183);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 184);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 185);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 186);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 187);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 188);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 189);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 190);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 191);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 192);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 193);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 194);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 195);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 196);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 197);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 198);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 199);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 200);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 201);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 202);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 203);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 204);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 205);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 206);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 207);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 208);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 209);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 210);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 211);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 212);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 213);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 214);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 215);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 216);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 217);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 218);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 219);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 220);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 221);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 222);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 223);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 224);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 225);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 226);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 227);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 228);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 229);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 230);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 231);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 232);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 233);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 234);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 235);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 236);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 237);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 238);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 239);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 240);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 241);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 242);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 243);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 244);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 245);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 246);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 247);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 248);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 249);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 250);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 251);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 252);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 253);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 254);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 255);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 256);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 257);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 258);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 259);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 260);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 261);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 262);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 263);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 264);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 265);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 266);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 267);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 268);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 269);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 270);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 271);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 272);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 273);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 274);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 275);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 276);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 277);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 278);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 279);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 280);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 281);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 282);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 283);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 284);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 285);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 286);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 287);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 288);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 289);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 290);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 291);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 292);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 293);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 294);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 295);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 296);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 297);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 298);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 299);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 300);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 301);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 302);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 303);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 304);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 305);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 306);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 307);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 308);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 309);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 310);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 311);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 312);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 313);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 314);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 315);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 316);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 317);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 318);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 319);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 320);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 321);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 322);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 323);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 324);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 325);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 326);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 327);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 328);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 329);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 330);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 331);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 332);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 333);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 334);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 335);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 336);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 337);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 338);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 339);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 340);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 341);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 342);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 343);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 344);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 345);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 346);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 347);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 348);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 349);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 350);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 351);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 352);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 353);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 354);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 355);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 356);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 357);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 358);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 359);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 360);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 361);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 362);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 363);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 364);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 365);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 366);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 367);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 368);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 369);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 370);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 371);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 372);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 373);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 374);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 375);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 376);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 377);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 378);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 379);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 380);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 381);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 382);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 383);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 384);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 385);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 386);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 387);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 388);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 389);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 390);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 391);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 392);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 393);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 394);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 395);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 396);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 397);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 398);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 399);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 400);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 401);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 402);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 403);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 404);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 405);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 406);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 407);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 408);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 409);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 410);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 411);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 412);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 413);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 414);

            migrationBuilder.DeleteData(
                table: "Breweries",
                keyColumn: "Id",
                keyValue: 415);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Styles",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 33);
        }
    }
}
