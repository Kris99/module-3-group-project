﻿using BeerOverflow.Data.Models;
using BeerOverflow.Data.Seeding;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Context
{
    public class BeerOverflowContext : IdentityDbContext<User, Role, int>
    {
        public BeerOverflowContext()
        {

        }

        public BeerOverflowContext(DbContextOptions<BeerOverflowContext> options) : base(options)
        {
                
        }

        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Style> Styles { get; set; }
        public DbSet<BeerReview> Reviews { get; set; }
        public DbSet<BeerRating> Ratings { get; set; }
        public DbSet<WishlistItem> Wishlists { get; set; }
        public DbSet<DranklistItem> DrankLists { get; set; }
        public DbSet<ReviewLike> ReviewLikes { get; set; }
        public DbSet<ReviewDislike> ReviewDislikes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BeerRating>().HasKey(r => new { r.BeerId, r.UserId });
            modelBuilder.Entity<BeerReview>().HasKey(r => new { r.BeerId, r.UserId });
            modelBuilder.Entity<WishlistItem>().HasKey(wl => new { wl.BeerId, wl.UserId });
            modelBuilder.Entity<DranklistItem>().HasKey(dl => new { dl.BeerId, dl.UserId });
            modelBuilder.Entity<ReviewLike>().HasKey(l => new { l.BeerReviewId, l.UserId });
            modelBuilder.Entity<ReviewDislike>().HasKey(d => new { d.BeerReviewId, d.UserId });

            //already seeded.
            //modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }



        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=BeerOverflow12;Trusted_Connection=True;");
        //
        //}

    }
}
