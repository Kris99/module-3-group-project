﻿using BeerOverflow.Data.Models.Abstract;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using System.Text;

namespace BeerOverflow.Data.Models
{
    public class User : IdentityUser<int>
    {
        [Required, MinLength(3), MaxLength(50)]
        public string FirstName { get; set; }
        [Required, MinLength(3), MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        public bool IsBanned { get; set; } = false;
        public DateTime? BanExpirationDate { get; set; }
        public ICollection<BeerReview> Reviews { get; set; } = new List<BeerReview>(); //Reviewed beers collection
        public ICollection<BeerRating> Ratings { get; set; } = new List<BeerRating>(); //Rated beers collection
        public ICollection<WishlistItem> Wishlists { get; set; } = new List<WishlistItem>(); // Favorites collection
        public ICollection<DranklistItem> DrankList { get; set; } = new List<DranklistItem>(); //Drank list collection
    }
}
