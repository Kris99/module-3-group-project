﻿using BeerOverflow.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Data.Models
{
    public class Beer : Entity
    {
        [Key]
        public int Id { get; set; }

        [Required, MinLength(3), MaxLength(70)]
        public string Name { get; set; }

        [Required, Range(0, 25)]
        public double ABV { get; set; }
        public bool IsUnlisted { get; set; }
        public DateTime? UnlistedOn { get; set; }

        public int BreweryId { get; set; }

        [Required]
        public Brewery Brewery { get; set; }

        [MinLength(15), MaxLength(200)]
        public string Description { get; set; }

        public int StyleId { get; set; }
        [Required]
        public Style Style { get; set; }

        public ICollection<BeerReview> Reviews { get; set; } = new List<BeerReview>();
        public ICollection<BeerRating> Ratings { get; set; } = new List<BeerRating>();
    }
}
