﻿using BeerOverflow.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Data.Models
{
    public class Brewery : Entity
    {
        [Key]
        public int Id { get; set; }
        [Required, MinLength(3), MaxLength(150)]
        public string Name { get; set; }
        public int CountryId { get; set; }
        [Required]
        public Country Country { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
