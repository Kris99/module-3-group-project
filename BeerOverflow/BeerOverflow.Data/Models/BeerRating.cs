﻿using BeerOverflow.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Data.Models
{
    public class BeerRating : Feedback
    {
        [Key]
        public int Id { get; set; }
        [Required, Range(0,10)]
        public int Rating { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        //public int BeerId { get; set; } //inherited from Feedback abstract class
        //public Beer Beer { get; set; }
        //public int UserId { get; set; }
        //public User User { get; set; }
    }
}
