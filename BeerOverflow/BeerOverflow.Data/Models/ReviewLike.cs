﻿using BeerOverflow.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Data.Models
{
    public class ReviewLike : Feedback
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Like { get; set; }
        public int BeerReviewId { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        //public int BeerId { get; set; } //inherited from Feedback abstract class
        //public Beer Beer { get; set; }
        //public int UserId { get; set; }
        //public User User { get; set; }
    }
}
