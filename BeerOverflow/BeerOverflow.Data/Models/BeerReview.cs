﻿using BeerOverflow.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Data.Models
{
    public class BeerReview : Feedback
    {
        [Key]
        public int Id { get; set; }

        [Required, MinLength(10), MaxLength(500)]
        public string Review { get; set; }
        public bool IsLiked { get; set; } = false;
        public bool IsDisliked { get; set; } = false;
        public bool IsFlagged { get; set; }
        public bool IsDeleted { get; set; }
        public int Likes { get; set; } = 0;
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }

        //public int BeerId { get; set; } //inherited from Feedback abstract class
        //public Beer Beer { get; set; }
        //public int UserId { get; set; }
        //public User User { get; set; }
    }
}
