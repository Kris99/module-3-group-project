﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Models.Abstract
{
    public abstract class Feedback
    {
        public int BeerId { get; set; }
        public Beer Beer { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
