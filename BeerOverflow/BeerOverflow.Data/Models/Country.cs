﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using BeerOverflow.Data.Models.Abstract;

namespace BeerOverflow.Data.Models
{
    public class Country : Entity
    {
        [Key]
        public int Id { get; set; }
        [Required, MinLength(3), MaxLength(100)]
        public string Name { get; set; }
        public ICollection<Brewery> Breweries { get; set; } = new List<Brewery>();
    }
}
