﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using BeerOverflow.Data.Models.Abstract;

namespace BeerOverflow.Data.Models
{
    public class Style : Entity
    {
        [Key]
        public int Id { get; set; }

        [Required, MinLength(3), MaxLength(129)]
        public string Name { get; set; }
        
        public ICollection<Beer> Beers { get; set; }
        
    }
}
