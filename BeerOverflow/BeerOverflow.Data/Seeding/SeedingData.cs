﻿using BeerOverflow.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverflow.Data.Seeding
{
    public static class SeedingData
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var data = LoadData();

            var countries = CreateCountryModels(data);
            modelBuilder.Entity<Country>().HasData(countries);

            var styles = CreateStyleModels(data);
            modelBuilder.Entity<Style>().HasData(styles);

            var breweries = CreateBreweryModels(data, countries);
            modelBuilder.Entity<Brewery>().HasData(breweries);

            var beers = CreateBeerModels(data, styles, breweries);
            modelBuilder.Entity<Beer>().HasData(beers);

            var users = CreateUserModels();
            modelBuilder.Entity<User>().HasData(users);

            modelBuilder.Entity<Role>().HasData(
                new Role
                {
                    Id = 1,
                    Name = "Admin",
                    NormalizedName = "ADMIN",                   
                },
                new Role 
                { 
                    Id = 2, 
                    Name = "User", 
                    NormalizedName = "USER"
                });

            modelBuilder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = users[0].Id
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = users[1].Id
                });
        }

        private static List<Dictionary<string, string>> LoadData()
        {
            var beers = new List<Dictionary<string, string>>();
            var path = @".\Seed\Beers.csv";
            using (TextFieldParser parser = new TextFieldParser(path))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");

                while (!parser.EndOfData)
                {
                    var beer = new Dictionary<string, string>();
                    string[] fields = parser.ReadFields();

                    beer.Add("Name", fields[0]);
                    beer.Add("ABV", fields[1]);
                    beer.Add("Style", fields[2]);
                    beer.Add("Brewery", fields[3]);
                    beer.Add("Country", fields[4]);

                    beers.Add(beer);
                }
            }

            return beers;
        }

        private static List<Country> CreateCountryModels(List<Dictionary<string, string>> data)
        {
            var countryNames = GetUniqueColumnValues(data, column: "Country");
            var counter = 1;

            var countries = new List<Country>();

            foreach (var name in countryNames)
            {
                countries.Add(new Country() { Id = counter++, Name = name });
            }

            return countries;
        }

        private static List<Style> CreateStyleModels(List<Dictionary<string, string>> rawBeers)
        {
            var uniqueStyles = GetUniqueColumnValues(rawBeers, column: "Style");
            var counter = 1;
            var styles = new List<Style>();

            foreach (var name in uniqueStyles)
            {
                styles.Add(new Style() { Id = counter++, Name = name });
            }

            return styles;
        }

        private static List<Brewery> CreateBreweryModels(List<Dictionary<string, string>> rawBeers, List<Country> countries)
        {
            var uniqueBreweryNames = new HashSet<string>();
            var breweries = new List<Brewery>();
            var counter = 1;

            foreach (var beer in rawBeers)
            {
                if (!uniqueBreweryNames.Contains(beer["Brewery"]))
                {
                    var country = countries.Where(country => country.Name == beer["Country"]).First();
                    var brewery = new Brewery()
                    {
                        Id = counter++,
                        Name = beer["Brewery"],
                        CountryId = country.Id,
                    };

                    breweries.Add(brewery);

                    uniqueBreweryNames.Add(beer["Brewery"]);
                }
            }

            return breweries;
        }

        private static List<Beer> CreateBeerModels(List<Dictionary<string, string>> rawBeers, List<Style> styles, List<Brewery> breweries)
        {
            var beers = new List<Beer>();
            var counter = 1;

            foreach (var rawBeer in rawBeers)
            {
                var style = styles.Where(style => style.Name == rawBeer["Style"]).First();
                var brewery = breweries.Where(brewery => brewery.Name == rawBeer["Brewery"]).First();
                var abv = double.Parse(rawBeer["ABV"]);

                var beer = new Beer()
                {
                    Id = counter++,
                    Name = rawBeer["Name"],
                    ABV = Math.Round(abv, 1),
                    StyleId = style.Id,
                    BreweryId = brewery.Id,
                };

                beers.Add(beer);
            }

            return beers;
        }

        private static HashSet<string> GetUniqueColumnValues(List<Dictionary<string, string>> table, string column)
        {
            var uniqueValues = new HashSet<string>();

            foreach (var row in table)
            {
                uniqueValues.Add(row[column]);
            }

            return uniqueValues;
        }
        private static User[] CreateUserModels()
        {
            var hasher = new PasswordHasher<User>();
            User[] users = new User[2];

            var admin = new User
            {
                Id = 33,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                UserName = "Krisko",
                NormalizedUserName = "KRISKO",
                Email = "kris.dimitrow99@abv.bg",
                NormalizedEmail = "KRIS.DIMITROW99@ABV.BG",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            admin.PasswordHash = hasher.HashPassword(admin, "1234abcd");
            users[0] = admin;

            var user = new User
            {
                Id = 34,
                UserName = "Hellyz",
                FirstName = "Georgi",
                LastName = "Georgiev",
                NormalizedUserName = "HELLYZ",
                Email = "g.georgiev@gmail.com",
                NormalizedEmail = "G.GEORGIEV@GMAIL.COM",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            user.PasswordHash = hasher.HashPassword(user, "12345678");
            users[1] = user;

            return users;
        }
    }
}
