﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.StyleServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Create_Should_InsertStyle()
        {
            var options = Utils.GetOptions(nameof(Create_Should_InsertStyle));
            var service = new Mock<IStyleService>();

            var style = new Style
            {
                Id = 16238,
                Name = "Pils",
                IsDeleted = false
            };

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new StyleService(actContext);
                var styleDTO = new StyleDTO(style.Id, style.Name);
                var result = sut.CreateStyleAsync(styleDTO).Result;

                //Assert
                Assert.AreEqual(style.Name, result.Name);
                Assert.AreEqual(style.Id, result.Id);
            }
        }
    }
}
