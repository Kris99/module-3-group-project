﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.StyleServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void GetAllStyles_Should_ReturnAllStyles()
        {
            var options = Utils.GetOptions(nameof(GetAllStyles_Should_ReturnAllStyles));
            var service = new Mock<IStyleService>();

            var style = new Style
            {
                Id = 16238,
                Name = "Pils",
                IsDeleted = false
            };

            var style2 = new Style
            {
                Id = 162328,
                Name = "Weiss",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Add(style);
                context.Add(style2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new StyleService(actContext);
                var styleDTO = new StyleDTO(style.Id, style.Name);
                var result = sut.GetAllStyles().Result;

                //Assert
                Assert.AreEqual(style.Name, result.First().Name);
                Assert.AreEqual(style2.Name, result.Last().Name);
            }
        }

        [TestMethod]
        public void GetStyle_Should_ReturnStyle()
        {
            var options = Utils.GetOptions(nameof(GetStyle_Should_ReturnStyle));
            var service = new Mock<IStyleService>();

            var style = new Style
            {
                Id = 162338,
                Name = "Pils",
                IsDeleted = false
            };

            var style2 = new Style
            {
                Id = 1624328,
                Name = "Weiss",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Add(style);
                context.Add(style2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new StyleService(actContext);
                var styleDTO = new StyleDTO(style.Id, style.Name);
                var result = sut.GetStyleAsync(style.Id).Result;

                //Assert
                Assert.AreEqual(style.Name, result.Name);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void GetStyle_Should_ThrowAnException()
        {
            var options = Utils.GetOptions(nameof(GetStyle_Should_ThrowAnException));
            var service = new Mock<IStyleService>();

            var style = new Style
            {
                Id = 165338,
                Name = "Pils",
                IsDeleted = false
            };

            var style2 = new Style
            {
                Id = 162432328,
                Name = "Weiss",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Add(style);
                context.Add(style2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new StyleService(actContext);
                var styleDTO = new StyleDTO(style.Id, style.Name);
                var result = sut.GetStyleAsync(style.Id+1).Result;

                //Assert
                Assert.AreEqual(style.Name, result.Name);
            }
        }
    }
}
