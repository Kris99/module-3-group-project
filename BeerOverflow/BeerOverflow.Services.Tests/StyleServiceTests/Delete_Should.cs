﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.StyleServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void Delete_Should_RemoveStyle()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_RemoveStyle));
            var service = new Mock<IStyleService>();

            var style = new Style
            {
                Id = 16238,
                Name = "Pils",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Add(style);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new StyleService(actContext);
                var styleDTO = new StyleDTO(style.Id, style.Name);
                var result = sut.DeleteStyleAsync(style.Id);

                //Assert
                Assert.IsTrue(actContext.Styles.First().IsDeleted);
            }
        }

        [TestMethod]
        public void Delete_Should_ThrowAnException()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_ThrowAnException));
            var service = new Mock<IStyleService>();

            var style = new Style
            {
                Id = 162328,
                Name = "Pils",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Add(style);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new StyleService(actContext);
                var styleDTO = new StyleDTO(style.Id, style.Name);
                var result = sut.DeleteStyleAsync(style.Id + 1);

                //Assert
                Assert.IsNotNull(result.Exception);
            }
        }
    }
}
