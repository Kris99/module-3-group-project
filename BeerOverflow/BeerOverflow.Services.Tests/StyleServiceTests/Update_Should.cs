﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.StyleServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void Update_Should_Return_UpdatedStyle()
        {
            var options = Utils.GetOptions(nameof(Update_Should_Return_UpdatedStyle));
            var service = new Mock<IStyleService>();

            var style = new Style
            {
                Id = 162382,
                Name = "Pils",
                IsDeleted = false
            };

            var style2 = new Style
            {
                Id = 16232,
                Name = "Weiss",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Add(style);
                context.Add(style2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new StyleService(actContext);
                var result = sut.UpdateStyleAsync(style.Id, "Denmark").Result;

                //Assert
                Assert.AreEqual(result.Name, "Denmark");
                Assert.AreEqual(result.Id, style.Id);
            }
        }
    }
}
