﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.UserServiceTests
{
    [TestClass]
    public class LoginUser_Should
    {
        [TestMethod]
        public void LoginUser_Should_ReturnTrue()
        {
            //var options = Utils.GetOptions(nameof(LoginUser_Should_ReturnTrue));
            //var service = new Mock<IUserService>();
            //var mapper = new Mock<IMapper>();
            //
            //var userManager = new Mock<UserManager<User>>();
            //var signInManager = new Mock<SignInManagerMock>();
            //
            //var user = new User
            //{
            //    Id = 471637841,
            //    FirstName = "Kristian",
            //    LastName = "Dimitrov",
            //    Email = "kris.dimitrov99@gmail.com",
            //    UserName = "Krisko"
            //};
            //
            //var userDTO = new UserDTO(user);
            //
            //var user2 = new User
            //{
            //    Id = 87645890,
            //    FirstName = "Mario",
            //    LastName = "Dimitrov",
            //    Email = "mariok1996@abv.bg",
            //    UserName = "Mariochkata"
            //};
            //
            //using (var arrangeContext = new BeerOverflowContext(options))
            //{
            //    arrangeContext.Users.Add(user);
            //    arrangeContext.Users.Add(user2);
            //    arrangeContext.SaveChanges();
            //}
            //
            ////Act
            //using (var actContext = new BeerOverflowContext(options))
            //{
            //    var sut = new UserService(actContext, mapper.Object, userManager.Object, signInManager.Object);
            //    var result = sut.LoginUserAsync(userDTO).Result;
            //
            //    //Assert
            //    Assert.IsTrue(result);
            //}
        }

        [TestMethod]
        [DataRow(128379, 812793)]
        public void AddToDranklist_Should_ReturnFalse(int beerId, int userId)
        {
            //var options = Utils.GetOptions(nameof(LoginUser_Should_ReturnTrue));
            //var service = new Mock<IUserService>();
            //var mapper = new Mock<IMapper>();
            //
            //var userManager = new Mock<UserManager<User>>();
            //var signInManager = new Mock<SignInManagerMock>();
            //
            //var user = new User
            //{
            //    Id = 471637841,
            //    FirstName = "Kristian",
            //    LastName = "Dimitrov",
            //    Email = "kris.dimitrov99@gmail.com",
            //    UserName = "Krisko"
            //};
            //
            //var userDTO = new UserDTO(user);
            //
            //var user2 = new User
            //{
            //    Id = 87645890,
            //    FirstName = "Mario",
            //    LastName = "Dimitrov",
            //    Email = "mariok1996@abv.bg",
            //    UserName = "Mariochkata"
            //};
            //
            //using (var arrangeContext = new BeerOverflowContext(options))
            //{
            //    arrangeContext.Users.Add(user);
            //    arrangeContext.Users.Add(user2);
            //    arrangeContext.SaveChanges();
            //}
            //
            ////Act
            //using (var actContext = new BeerOverflowContext(options))
            //{
            //    var sut = new UserService(actContext, mapper.Object, userManager.Object, signInManager.Object);
            //    var result = sut.LoginUserAsync(userDTO).Result;
            //
            //    //Assert
            //    Assert.IsTrue(result);
            //}
        }
    }
}
