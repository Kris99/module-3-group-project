﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.UserServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void Update_Should_Return_UpdatedUser()
        {
            var options = Utils.GetOptions(nameof(Update_Should_Return_UpdatedUser));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = 78234691,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = 78234691,
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new UserService(actContext, mapper.Object);
                var result = sut.UpdateUserAsync(user2);

                //Assert
                Assert.IsInstanceOfType(result, typeof(Task<UserDTO>));
                Assert.AreNotEqual(result.Result.FirstName, user.FirstName);
                Assert.AreEqual(result.Result.FirstName, user2.FirstName);
                Assert.AreNotEqual(result.Result.Email, user.Email);
                Assert.AreEqual(result.Result.Email, user2.Email);
                Assert.AreNotEqual(result.Result.UserName, user.UserName);
                Assert.AreEqual(result.Result.UserName, user2.UserName);

            }
        }

        [TestMethod]
        public void Update_Should_ReturnNULL()
        {
            var options = Utils.GetOptions(nameof(Update_Should_ReturnNULL));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = 19283701,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = 1378429,
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new UserService(actContext, mapper.Object);
                var result = sut.DeleteUserAsync(1378433292);

                //Assert
                Assert.AreEqual(result.Result, null);
                Assert.AreEqual(actContext.Users.First().FirstName, user.FirstName);
                Assert.AreEqual(actContext.Users.Last().Email, user.Email);
            }
        }
    }
}
