﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.UserServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void GetAllUsers_Should_Return_AllUsers()
        {
            var options = Utils.GetOptions(nameof(GetAllUsers_Should_Return_AllUsers));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = 187236981,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov@gmail.com",
                UserName = "Krisko"           
            };

            var user2 = new User
            {
                Id = 128367912,
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new UserService(actContext, mapper.Object);
                var result = sut.GetAllUsers();

                //Assert
                Assert.IsInstanceOfType(result, typeof(Task<IEnumerable<UserDTO>>));
                Assert.AreEqual(result.Result.First().Id, user.Id);
                Assert.AreEqual(result.Result.Last().Id, user2.Id);
                Assert.AreEqual(result.Result.First().FirstName, user.FirstName);
                Assert.AreEqual(result.Result.First().LastName, user2.LastName);
            }
        }

        [TestMethod]
        public void GetUserById_Should_Return_TheUser()
        {
            var options = Utils.GetOptions(nameof(GetAllUsers_Should_Return_AllUsers));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = 135135141,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = 1235253141,
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new UserService(actContext, mapper.Object);
                var result = sut.GetUserById(1235253141);

                //Assert
                Assert.IsInstanceOfType(result, typeof(Task<UserDTO>));
                Assert.AreEqual(result.Result.Id, user2.Id);
                Assert.AreEqual(result.Result.FirstName, user2.FirstName);
                Assert.AreEqual(result.Result.LastName, user2.LastName);
                Assert.AreEqual(result.Result.Email, user2.Email);
                Assert.AreEqual(result.Result.UserName, user2.UserName);
            }
        }

        [TestMethod]
        public void GetUserById_Should_Return_NULL()
        {
            var options = Utils.GetOptions(nameof(GetAllUsers_Should_Return_AllUsers));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = 1273678,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = 172391,
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new UserService(actContext, mapper.Object);
                var result = sut.GetUserById(12345678);

                //Assert
                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
