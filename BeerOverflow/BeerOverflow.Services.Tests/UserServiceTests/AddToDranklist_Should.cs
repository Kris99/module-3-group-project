﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.UserServiceTests
{
    [TestClass]
    public class AddToDranklist_Should
    {
        [TestMethod]
        public void AddToDranklist_Should_ReturnTrue()
        {
            var options = Utils.GetOptions(nameof(AddToDranklist_Should_ReturnTrue));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();
            
            var user = new User
            {
                Id = 135135141,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };
            
            var user2 = new User
            {
                Id = 1235253141,
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };
            var beer = new Beer
            {
                Id = 1,
                Name = "Zagorka",

            };

            var item = new WishlistItem
            {
                Id = 1,
                BeerId = 1,
                UserId = 135135141
            };


            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.Beers.Add(beer);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new UserService(actContext, mapper.Object);
                var result = sut.AddBeerToDranklistAsync(1, 135135141).Result;

                //Assert
                Assert.IsTrue(result);
                Assert.IsNotNull(actContext.DrankLists);
                Assert.AreEqual(actContext.DrankLists.First().UserId, user.Id);
            }
        }

        [TestMethod]
        [DataRow(128379, 812793)]
        public void AddToDranklist_Should_ReturnFalse(int beerId, int userId)
        {
            var options = Utils.GetOptions(nameof(AddToDranklist_Should_ReturnFalse));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = 2737189,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var beer = new Beer
            {
                Id = 89,
                Name = "Zagorka",

            };

            var item = new WishlistItem
            {
                Id = 1,
                BeerId = 1,
                UserId = user.Id
            };


            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Beers.Add(beer);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new UserService(actContext, mapper.Object);
                var result = sut.AddBeerToDranklistAsync(beerId, userId).Result;

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
