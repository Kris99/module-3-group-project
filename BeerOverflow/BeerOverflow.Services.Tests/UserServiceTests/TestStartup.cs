﻿using BeerOverflow.Data.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.Tests
{

    //this is not being used
    public class TestStartup : Startup
    {
        public TestStartup(IWebHostEnvironment hostingEnvironment,  IConfiguration configuration)
            : base(configuration)
        {
        }

        public void ConfigureTestServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            var mock = new SignInManagerMock(default, default, default, default, default, default, default);

            var descriptior = new ServiceDescriptor(typeof(SignInManager<User>), mock);

            services.Replace(descriptior);
        }
    }
}
