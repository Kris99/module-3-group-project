﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.UserServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Create_Should_InsertUser()
        {
            var options = Utils.GetOptions(nameof(Create_Should_InsertUser));
            var service = new Mock<IUserService>();
            var mapper = new Mock<IMapper>();

            var user = new User
            {
                Id = 135135141,
                FirstName = "Kristian",
                LastName = "Dimitrov",
                Email = "kris.dimitrov99@gmail.com",
                UserName = "Krisko"
            };

            var user2 = new User
            {
                Id = 1235253141,
                FirstName = "Mario",
                LastName = "Dimitrov",
                Email = "mariok1996@abv.bg",
                UserName = "Mariochkata"
            };

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new UserService(actContext, mapper.Object);
                var userDTO = new UserDTO(user);
                var result = sut.CreateUserAsyncTest(userDTO);

                //Assert
                Assert.IsInstanceOfType(result, typeof(Task<bool>));
                Assert.AreEqual(actContext.Users.Count(), 1);
                Assert.AreEqual(user.Id, actContext.Users.First().Id);
                Assert.AreEqual(user.FirstName, actContext.Users.First().FirstName);
                Assert.AreEqual(user.LastName, actContext.Users.First().LastName);
                Assert.AreEqual(user.Email, actContext.Users.First().Email);
                Assert.AreEqual(user.UserName, actContext.Users.First().UserName);
            }
        }
    }
}
