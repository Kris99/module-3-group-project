﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void GetAllCountries_Should_Return_AllCountries()
        {
            var options = Utils.GetOptions(nameof(GetAllCountries_Should_Return_AllCountries));
            var service = new Mock<ICountryService>();

            var country = new Country
            {
                Id = 2131381229,
                Name = "Germany",
                IsDeleted = false
            };

            var country2 = new Country
            {
                Id = 2112781229,
                Name = "Bulgaria",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Countries.Add(country);
                context.Countries.Add(country2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(actContext);
                var result = sut.GetAllCountries();

                Assert.AreEqual(2, result.Count());
                Assert.IsInstanceOfType(result.First(), typeof(CountryDTO));
            }

        }

        [TestMethod]
        public void GetCountryById_Should_ReturnCorrectCountry()
        {
            var options = Utils.GetOptions(nameof(GetCountryById_Should_ReturnCorrectCountry));
            var service = new Mock<ICountryService>();

            var country = new Country
            {
                Id = 2131381229,
                Name = "Germany",
                IsDeleted = false
            };

            var country2 = new Country
            {
                Id = 2112781229,
                Name = "Bulgaria",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Countries.Add(country);
                context.Countries.Add(country2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(actContext);
                var result = sut.GetCountryAsync(country.Id);

                Assert.AreEqual(result.Result.Id, country.Id);
                Assert.AreEqual(result.Result.Name, country.Name);
            }

        }
        [TestMethod]
        public void GetCountryById_Should_ThrowAnException()
        {
            var options = Utils.GetOptions(nameof(GetCountryById_Should_ThrowAnException));
            var service = new Mock<ICountryService>();

            var country = new Country
            {
                Id = 2131381229,
                Name = "Germany",
                IsDeleted = false
            };

            var country2 = new Country
            {
                Id = 2112781229,
                Name = "Bulgaria",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Countries.Add(country);
                context.Countries.Add(country2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(actContext);
                var result = sut.GetCountryAsync(country.Id + 2342);

            }

        }
    }
}
