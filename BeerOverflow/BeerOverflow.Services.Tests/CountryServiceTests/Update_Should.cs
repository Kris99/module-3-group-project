﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void Update_Should_Return_UpdatedCountry()
        {
            var options = Utils.GetOptions(nameof(Update_Should_Return_UpdatedCountry));
            var service = new Mock<ICountryService>();

            var country = new Country
            {
                Id = 2131381329,
                Name = "Germany",
                IsDeleted = false
            };

            var country2 = new Country
            {
                Id = 2112721229,
                Name = "Bulgaria",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Countries.Add(country);
                context.Countries.Add(country2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(actContext);
                var result = sut.UpdateCountryAsync(country.Id, "Denmark");

                Assert.AreEqual(result.Result.Id, country.Id);
                Assert.AreNotEqual(result.Result.Name, country.Name);
            }

        }
    }
}
