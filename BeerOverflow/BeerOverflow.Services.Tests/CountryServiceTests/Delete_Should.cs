﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void Delete_Should_RemoveCountry()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_RemoveCountry));
            var service = new Mock<ICountryService>();

            var country = new Country
            {
                Id = 213781299,
                Name = "Germany",
                IsDeleted = false
            };

            using(var context = new BeerOverflowContext(options))
            {
                context.Countries.Add(country);
                context.SaveChanges();
            }


            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(actContext);
                var countryDTO = new CountryDTO(country.Id, country.Name);
                var result = sut.DeleteCountryAsync(country.Id);

                //Assert
                Assert.IsTrue(actContext.Countries.First().IsDeleted);
            }

        }

        [TestMethod]
        public void Delete_Should_ThrowAnException()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_ThrowAnException));
            var service = new Mock<ICountryService>();

            var country = new Country
            {
                Id = 213781229,
                Name = "Germany",
                IsDeleted = false
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Countries.Add(country);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(actContext);
                var countryDTO = new CountryDTO(country.Id, country.Name);
                var result = sut.DeleteCountryAsync(country.Id + 1);

                Assert.IsNotNull(result.Exception);
            }

        }
    }
}
