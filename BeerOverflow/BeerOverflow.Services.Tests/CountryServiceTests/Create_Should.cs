﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.CountryServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Create_Should_InsertCountry()
        {
            var options = Utils.GetOptions(nameof(Create_Should_InsertCountry));
            var service = new Mock<ICountryService>();

            var country = new Country
            {
                Id = 21378125,
                Name = "Germany"
            };


            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(actContext);
                var countryDTO = new CountryDTO(country.Id, country.Name);
                var result = sut.CreateCountryAsync(countryDTO);

                //Assert
                Assert.AreEqual(1, actContext.Countries.Count());
                Assert.AreEqual(country.Name, result.Result.Name);
            }
        
        }
    }
}
