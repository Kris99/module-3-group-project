﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Create_Should_InsertBrewery()
        {
            var options = Utils.GetOptions(nameof(Create_Should_InsertBrewery));
            var service = new Mock<IBreweryService>();

            var country = new Country
            {
                Id = 21378129,
                Name = "Germany"
            };
            var brewery = new Brewery
            {
                Id = 135135141,
                Name = "Stöckl",
                CountryId = 21378129,
            };

            var berwery2 = new Brewery
            {
                Id = 12432567,
                Name = "Zagorka"
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Countries.Add(country);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(actContext);
                var breweryDTO = new BreweryDTO(brewery.Id, brewery.Name, brewery.CountryId, country.Name);
                var result = sut.CreateBreweryAsync(breweryDTO);

                //Assert
                Assert.AreEqual(1, actContext.Breweries.Count());
            }
        }
    }
}
