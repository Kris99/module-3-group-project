﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void Delete_Should_RemoveBrewery()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_RemoveBrewery));
            var service = new Mock<IBreweryService>();

            var country = new Country
            {
                Id = 21378129,
                Name = "Germany"
            };
            var brewery = new Brewery
            {
                Id = 3456765,
                Name = "Stöckl",
                CountryId = 21378129,
            };

            var berwery2 = new Brewery
            {
                Id = 12432567,
                Name = "Zagorka"
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Breweries.Add(brewery);
                context.Countries.Add(country);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(actContext);
                var breweryDTO = new BreweryDTO(brewery.Id, brewery.Name, brewery.CountryId, country.Name);
                var result = sut.DeleteBreweryAsync(brewery.Id);

                //Assert
                Assert.AreEqual(1, actContext.Breweries.Count());
            }
        }

        [TestMethod]
        public void Delete_Should_NotRemoveBrewery()
        {
            var options = Utils.GetOptions(nameof(Delete_Should_NotRemoveBrewery));
            var service = new Mock<IBreweryService>();

            var country = new Country
            {
                Id = 5253422,
                Name = "Germany"
            };
            var brewery = new Brewery
            {
                Id = 3456765,
                Name = "Stöckl",
                CountryId = 21378129,
                IsDeleted = false
            };

            var berwery2 = new Brewery
            {
                Id = 12432567,
                Name = "Zagorka"
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Breweries.Add(brewery);
                context.Countries.Add(country);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(actContext);
                var breweryDTO = new BreweryDTO(brewery.Id, brewery.Name, brewery.CountryId, country.Name);
                var result = sut.DeleteBreweryAsync(brewery.Id + 1);

                Assert.AreEqual(1, actContext.Breweries.Count());
            }
        }

    }
}
