﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void Get_Should_Return_CorrectBrewery()
        {
            var options = Utils.GetOptions(nameof(Get_Should_Return_CorrectBrewery));
            var service = new Mock<IBreweryService>();

            var country = new Country
            {
                Id = 21378129,
                Name = "Germany"
            };
            var brewery = new Brewery
            {
                Id = 3456765,
                Name = "Stöckl",
                CountryId = 21378129,
            };

            var berwery2 = new Brewery
            {
                Id = 12432567,
                Name = "Zagorka"
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Breweries.Add(brewery);
                context.Countries.Add(country);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(actContext);
                var breweryDTO = new BreweryDTO(brewery.Id, brewery.Name, brewery.CountryId, country.Name);
                var result = sut.GetBreweryAsync(brewery.Id).Result;

                //Assert
                Assert.AreEqual(brewery.Id, result.Id);
                Assert.AreEqual(country.Id, result.CountryId);
            }
        }

        [TestMethod]
        public void GetAllBreweries_Should_ReturnBreweries()
        {
            var options = Utils.GetOptions(nameof(GetAllBreweries_Should_ReturnBreweries));
            var service = new Mock<IBreweryService>();

            var country = new Country
            {
                Id = 21378129,
                Name = "Germany"
            };
            var brewery = new Brewery
            {
                Id = 3456765,
                Name = "Stöckl",
                CountryId = 21378129,
            };

            var berwery2 = new Brewery
            {
                Id = 12432567,
                Name = "Zagorka",
                CountryId = country.Id
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Breweries.Add(brewery);
                context.Breweries.Add(berwery2);
                context.Countries.Add(country);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(actContext);
                var result = sut.GetAllBreweries().Result;

                //Assert
                Assert.AreEqual(brewery.Id, result.Last().Id);
                Assert.AreEqual(country.Id, result.Last().CountryId);
                Assert.AreEqual(berwery2.Id, result.First().Id);
            }
        }     
    }
}
