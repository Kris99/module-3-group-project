﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BreweryServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void Update_Should_ReturnUpdatedBrewery()
        {
            var options = Utils.GetOptions(nameof(Update_Should_ReturnUpdatedBrewery));
            var service = new Mock<IBreweryService>();

            var country = new Country
            {
                Id = 31378889,
                Name = "Germany"
            };
            var country2 = new Country
            {
                Id = 21374199,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Id = 3456735,
                Name = "Stöckl",
                CountryId = 21378129,
                Country = new Country
                {
                    Id = country.Id + 1,
                    Name = country.Name
                }
            };

            var berwery2 = new Brewery
            {
                Id = 12432167,
                Name = "Zagorka",
                CountryId = country.Id
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Breweries.Add(brewery);
                context.Breweries.Add(berwery2);
                context.Countries.Add(country);
                context.Countries.Add(country2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(actContext);
                var result = sut.UpdateBreweryAsync(brewery.Id, "Shumensko", country.Id).Result;

                //Assert
                Assert.AreEqual("Shumensko", result.Name);
                Assert.AreEqual(country.Id, result.CountryId);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void Update_Should_ThrowAnException()
        {
            var options = Utils.GetOptions(nameof(Update_Should_ThrowAnException));
            var service = new Mock<IBreweryService>();

            var country = new Country
            {
                Id = 31378889,
                Name = "Germany"
            };
            var country2 = new Country
            {
                Id = 21374199,
                Name = "Bulgaria"
            };
            var brewery = new Brewery
            {
                Id = 3456735,
                Name = "Stöckl",
                CountryId = 21378129,
                Country = new Country
                {
                    Id = country.Id + 1,
                    Name = country.Name
                }
            };

            var berwery2 = new Brewery
            {
                Id = 12432167,
                Name = "Zagorka",
                CountryId = country.Id
            };

            using (var context = new BeerOverflowContext(options))
            {
                context.Breweries.Add(brewery);
                context.Breweries.Add(berwery2);
                context.Countries.Add(country);
                context.Countries.Add(country2);
                context.SaveChanges();
            }

            //Act
            using (var actContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(actContext);
                var result = sut.UpdateBreweryAsync(brewery.Id + 3, "Shumensko", country.Id).Result;

                //Assert
            }
        }
    }
}
