﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void ReturnCorrectBeer_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeer_WhenParamsAreValid));

            var firstBeerReview = new BeerReview
            {
                Id = 41,
                BeerId = 2,
                UserId = 3,
                Review = "Definitely check out this beer",
                Likes = 1
            };

            var secondBeerReview = new BeerReview
            {
                Id = 42,
                BeerId = 2,
                UserId = 4,
                Review = "Refreshening taste",
                Likes = 3
            };

            var updatedBeerReviewDTO = new BeerReviewDTO
            {
                Review = "Completeness",
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Reviews.Add(firstBeerReview);
                arrangeContext.Reviews.Add(secondBeerReview);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);

                var result = sut.UpdateBeerReviewAsync(41, updatedBeerReviewDTO).Result;

                Assert.AreEqual(updatedBeerReviewDTO.Review, result.Review);
            }
        }

        [TestMethod]
        public void ReturnNullWhenBeerNotFound()
        {
            var options = Utils.GetOptions(nameof(ReturnNullWhenBeerNotFound));

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            var updatedBeerReviewDTO = new BeerReviewDTO
            {
                Review = "Completeness",
            };

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);
                var result = sut.UpdateBeerReviewAsync(156, updatedBeerReviewDTO);
                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
