﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void ReturnCorrectBeerReview_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeerReview_WhenParamsAreValid));

            var beer = new Beer
            {
                Id = 26,
                BreweryId = 27,
                StyleId = 28,
                ABV = 3.4,
                Description = "Comes from far away"
            };
            
            beer.Reviews = new List<BeerReview>();

            var beerReviewDTO = new BeerReviewDTO()
            {
                BeerId = 26,
                UserId = 27,
                Review = "Awesome taste!",
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Beers.Add(beer);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);

                var result = sut.CreateBeerReviewAsync(beerReviewDTO);

                Assert.IsInstanceOfType(result, typeof(Task<BeerReviewDTO>));
                Assert.AreEqual(assertContext.Reviews.Count(), 1);
                Assert.AreEqual(result.Result.BeerId, beerReviewDTO.BeerId);
                Assert.AreEqual(result.Result.UserId, beerReviewDTO.UserId);
                Assert.AreEqual(result.Result.Review, beerReviewDTO.Review);
            }
        }

        [TestMethod]
        public void ReturnNull_WhenUserAlreadyReviewedThisBeer()
        {
            var options = Utils.GetOptions(nameof(ReturnNull_WhenUserAlreadyReviewedThisBeer));

            var beer = new Beer
            {
                Id = 26,
                BreweryId = 27,
                StyleId = 28,
                ABV = 3.4,
                Description = "Comes from far away"
            };

            beer.Reviews = new List<BeerReview>();

            var firstBeerReview = new BeerReview()
            {
                BeerId = 26,
                UserId = 27,
                Review = "Awesome taste!",
            };

            var secondBeerReviewDTO = new BeerReviewDTO()
            {
                BeerId = 26,
                UserId = 27,
                Review = "Horrible taste!",
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Beers.Add(beer);
                arrangeContext.Reviews.Add(firstBeerReview);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);
                var result = sut.CreateBeerReviewAsync(secondBeerReviewDTO);

                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
