﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class GetDislikes_Should
    {
        [TestMethod]
        public void GetDislikes_Should_ReturnDislikes()
        {
            var options = Utils.GetOptions(nameof(GetDislikes_Should_ReturnDislikes));

            var beer = new Beer
            {
                Id = 30,
                BreweryId = 31,
                StyleId = 245,
                ABV = 3.4,
                Description = "Comes from far away"
            };

            beer.Reviews = new List<BeerReview>();

            var beerReview = new BeerReview()
            {
                BeerId = 30,
                UserId = 12,
                Review = "Awesome taste!",
                IsLiked = false,
                IsDisliked = false,
                Likes = -30
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Beers.Add(beer);
                arrangeContext.Reviews.Add(beerReview);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);

                var result = sut.GetBeerReviewLikesAsync(beerReview.BeerId, beerReview.UserId).Result;

                Assert.IsTrue(result == -30);
            }
        }
    }
}
