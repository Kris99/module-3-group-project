﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class Flag_Should
    {
        [TestMethod]
        public void FlagBeerReview_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(FlagBeerReview_WhenParamsAreValid));

            var beerReview = new BeerReview
            {
                Id = 33,
                BeerId = 2,
                UserId = 3,
                Review = "Definitely check out this beer",
                Likes = 1
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Reviews.Add(beerReview);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);

                var result = sut.FlagBeerReviewAsync(33).Result;

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public void ReturnFalseWhenBeerReviewIsNotFound()
        {
            var options = Utils.GetOptions(nameof(ReturnFalseWhenBeerReviewIsNotFound));

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);
                var result = sut.FlagBeerReviewAsync(156);
                Assert.IsFalse(result.Result);
            }
        }
    }
}
