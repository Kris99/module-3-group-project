﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class Dislike_Should
    {
        [TestMethod]
        public void DislikeReview_Should_Work()
        {
            var options = Utils.GetOptions(nameof(DislikeReview_Should_Work));

            var beer = new Beer
            {
                Id = 27,
                BreweryId = 28,
                StyleId = 29,
                ABV = 3.4,
                Description = "Comes from far away"
            };

            beer.Reviews = new List<BeerReview>();

            var beerReview = new BeerReview()
            {
                BeerId = 27,
                UserId = 28,
                Review = "Awesome taste!",
                IsLiked = false,
                IsDisliked = false
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Beers.Add(beer);
                arrangeContext.Reviews.Add(beerReview);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);

                var result = sut.DislikeBeerReviewAsync(beerReview.BeerId, beerReview.UserId).Result;

                Assert.IsTrue(assertContext.Reviews.First().IsDisliked);
                Assert.IsTrue(result);
            }
        }


        [TestMethod]
        public void DislikeReview_Should_ReturnFalse()
        {
            var options = Utils.GetOptions(nameof(DislikeReview_Should_ReturnFalse));

            var beer = new Beer
            {
                Id = 28,
                BreweryId = 29,
                StyleId = 30,
                ABV = 3.4,
                Description = "Comes from far away"
            };

            beer.Reviews = new List<BeerReview>();

            var beerReview = new BeerReview()
            {
                BeerId = 28,
                UserId = 29,
                Review = "Awesome taste!",
                IsLiked = false,
                IsDisliked = true
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Beers.Add(beer);
                arrangeContext.Reviews.Add(beerReview);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);

                var result = sut.DislikeBeerReviewAsync(beerReview.BeerId, beerReview.UserId).Result;

                Assert.IsFalse(result);
            }
        }
    }
}
