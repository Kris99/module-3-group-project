﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        [TestMethod]
        public void ReturnCorrectBeerReview_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeerReview_WhenParamsAreValid));

            var firstBeerReview = new BeerReview
            {
                Id = 31,
                BeerId = 2,
                UserId = 3,
                Review = "Definitely check out this beer",
                Likes = 1
            };

            var secondBeerReview = new BeerReview
            {
                Id = 32,
                BeerId = 2,
                UserId = 4,
                Review = "Refreshening taste",
                Likes = 3
            };

            var beerReviewDTO = new BeerReviewDTO
            {
                Id = 31,
                BeerId = 2,
                UserId = 3,
                Review = "Definitely check out this beer",
                Likes = 1
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Reviews.Add(firstBeerReview);
                arrangeContext.Reviews.Add(secondBeerReview);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);

                var result = sut.GetBeerReviewByIdAsync(31).Result;

                Assert.AreEqual(beerReviewDTO.Id, result.Id);
                Assert.AreEqual(beerReviewDTO.BeerId, result.BeerId);
                Assert.AreEqual(beerReviewDTO.UserId, result.UserId);
                Assert.AreEqual(beerReviewDTO.Review, result.Review);
                Assert.AreEqual(beerReviewDTO.Likes, result.Likes);
            }
        }

        [TestMethod]
        public void ReturnNullWhenBeerReviewNotFound()
        {
            var options = Utils.GetOptions(nameof(ReturnNullWhenBeerReviewNotFound));

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);
                var result = sut.GetBeerReviewByIdAsync(156);
                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
