﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class GetAll_Should
    {
        [TestMethod]
        public void ReturnCorrectBeerReviews_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeerReviews_WhenParamsAreValid));

            var beers = Utils.SeedBeers().ToList();

            var firstBeerReview = new BeerReview
            {
                Id = 35,
                BeerId = 2,
                UserId = 3,
                Review = "Definitely check out this beer",
                Likes = 1
            };

            var secondBeerReview = new BeerReview
            {
                Id = 36,
                BeerId = 2,
                UserId = 4,
                Review = "Refreshening taste",
                Likes = 3
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Reviews.Add(firstBeerReview);
                arrangeContext.Reviews.Add(secondBeerReview);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);

                var result = sut.GetBeerReviewsAsync(2).Result;
                var resultList = result.ToList();

                Assert.AreEqual(2, result.Count());
                Assert.AreEqual(resultList[0].Review, firstBeerReview.Review);
                Assert.AreEqual(resultList[1].Review, secondBeerReview.Review);
            }
        }

        [TestMethod]
        public void ReturnNullWhenBeerReviewsNotFound()
        {
            var options = Utils.GetOptions(nameof(ReturnNullWhenBeerReviewsNotFound));

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                assertContext.RemoveRange();
                assertContext.SaveChanges();
                var sut = new BeerReviewService(assertContext, dateTimeProviderMock.Object);
                var result = sut.GetBeerReviewsAsync(157);

                Assert.AreEqual(result.Result, null);
            }
        }



    }
}
