﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void ReturnCorrectBeer_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeer_WhenParamsAreValid));

            var beerDTO = new BeerDTO
            {
                Id = 3,
                Name = "Heineken",
                Description = "Belgian style",
                BreweryId = 4,
                ABV = 2.3,
                StyleId = 1
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.CreateBeerAsync(beerDTO);

                Assert.IsInstanceOfType(result, typeof(Task<BeerDTO>));
                Assert.AreEqual(assertContext.Beers.Count(), 1);
                Assert.AreEqual(beerDTO.Id, result.Result.Id);
                Assert.AreEqual(beerDTO.Name, result.Result.Name);
                Assert.AreEqual(beerDTO.Description, result.Result.Description);
                Assert.AreEqual(beerDTO.BreweryId, result.Result.BreweryId);
                Assert.AreEqual(beerDTO.ABV, result.Result.ABV);
                Assert.AreEqual(beerDTO.StyleId, result.Result.StyleId);
            }
        }
    }
}
