﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Tests.BeerServiceTests
{
    [TestClass]
    public class GetAll_Should
    {
        [TestMethod]
        public void ReturnCorrectBeers_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeers_WhenParamsAreValid));

            var heineken = new Beer
            {
                Id = 101,
                Name = "Heineken",
                Description = "Belgian style",
                BreweryId = 4,
                ABV = 2.3,
                StyleId = 1
            };

            var zagorka = new Beer
            {
                Id = 102,
                Name = "Zagorka",
                Description = "Awesome taste for people who love beer",
                BreweryId = 1,
                ABV = 4.2,
                StyleId = 1
            };

            var heinekenDTO = new BeerDTO
            {
                Id = 101,
                Name = "Heineken",
                Description = "Belgian style",
                BreweryId = 4,
                ABV = 2.3,
                StyleId = 1
            };

            var zagorkaDTO = new BeerDTO
            {
                Id = 102,
                Name = "Zagorka",
                Description = "Awesome taste for people who love beer",
                BreweryId = 1,
                ABV = 4.2,
                StyleId = 1
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Beers.Add(heineken);
                arrangeContext.Beers.Add(zagorka);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.GetAllBeersAsync().Result;
                var resultList = result.ToList();

                Assert.AreEqual(resultList[0].Name, heinekenDTO.Name);
                Assert.AreEqual(resultList[1].Name, zagorkaDTO.Name);
            }
        }

        [TestMethod]
        public void ReturnNullWhenBeersNotFound()
        {
            var options = Utils.GetOptions(nameof(ReturnNullWhenBeersNotFound));

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                assertContext.RemoveRange();
                assertContext.SaveChanges();
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);
                var result = sut.GetAllBeersAsync();
                
                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
