﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Tests.BeerServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void ReturnCorrectBeer_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeer_WhenParamsAreValid));

            Beer firstBeer = new Beer
            {
                Id = 10,
                Name = "Zagorka",
                Description = "Awesome taste for people who love beer",
                BreweryId = 1,
                ABV = 4.2,
                StyleId = 1,
            };
            Beer secondBeer = new Beer
            {
                Id = 11,
                Name = "Amstel",
                Description = "Unique beer with centuries of tradition from Amsterdam",
                BreweryId = 2,
                ABV = 4.7,
                StyleId = 2,
            };

            var updatedBeerDTO = new BeerDTO
            {
                Name = "Zagorka",
                Description = "Awesome taste!",
                ABV = 2.5,
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Beers.Add(firstBeer);
                arrangeContext.Beers.Add(secondBeer);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.UpdateBeerAsync(11, updatedBeerDTO).Result;

                Assert.AreEqual(updatedBeerDTO.Name, result.Name);
                Assert.AreEqual(updatedBeerDTO.Description, result.Description);
                Assert.AreEqual(updatedBeerDTO.ABV, result.ABV);
            }
        }

        [TestMethod]
        public void ReturnNullWhenBeerNotFound()
        {
            var options = Utils.GetOptions(nameof(ReturnNullWhenBeerNotFound));

            var beerDTO = new BeerDTO
            {
                Name = "Heineken",
                Description = "Unique Belgian style",
                BreweryId = 4,
                ABV = 2.3,
                StyleId = 1
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);
                var result = sut.UpdateBeerAsync(156, beerDTO);
                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
