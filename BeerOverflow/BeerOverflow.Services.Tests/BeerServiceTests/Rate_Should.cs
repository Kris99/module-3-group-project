﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Tests.BeerServiceTests
{
    [TestClass]
    public class Rate_Should
    {
        [TestMethod]
        public void ReturnCorrectBeerRating_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeerRating_WhenParamsAreValid));

            var beerRatingDTO = new BeerRatingDTO
            {
                Id = 3,
                BeerId = 17,
                UserId = 18,
                Rating = 5
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.RateBeerAsync(beerRatingDTO);

                Assert.IsInstanceOfType(result, typeof(Task<BeerRatingDTO>));
                Assert.AreEqual(assertContext.Ratings.Count(), 1);
                Assert.AreEqual(beerRatingDTO.BeerId, result.Result.BeerId);
                Assert.AreEqual(beerRatingDTO.UserId, result.Result.UserId);
                Assert.AreEqual(beerRatingDTO.Rating, result.Result.Rating);
            }
        }

        [TestMethod]
        public void ReturnNull_WhenUserAlreadyRatedThisBeer()
        {
            var options = Utils.GetOptions(nameof(ReturnNull_WhenUserAlreadyRatedThisBeer));

            var oldBeerRating = new BeerRating
            {
                Id = 19,
                BeerId = 174,
                UserId = 185,
                Rating = 5
            };

            var newBeerRatingDTO = new BeerRatingDTO
            {
                Id = 20,
                BeerId = 174,
                UserId = 185,
                Rating = 6
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Ratings.Add(oldBeerRating);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);
                var result = sut.RateBeerAsync(newBeerRatingDTO);

                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
