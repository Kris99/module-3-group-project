﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Tests.BeerServiceTests
{
    [TestClass]
    public class GetBeerRating_Should
    {
        [TestMethod]
        public void ReturnCorrectBeerRating_WhenTwoUsersRatedTheBeer()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeerRating_WhenTwoUsersRatedTheBeer));

            var firstBeerRating = new BeerRating
            {
                Id = 23,
                BeerId = 17,
                UserId = 18,
                Rating = 5
            };

            var secondBeerRating = new BeerRating
            {
                Id = 24,
                BeerId = 17,
                UserId = 19,
                Rating = 9
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Ratings.Add(firstBeerRating);
                arrangeContext.Ratings.Add(secondBeerRating);
                arrangeContext.SaveChanges();
            }

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.GetBeerRatingAsync(17).Result;

                Assert.AreEqual(result, 7.0);
            }
        }

        [TestMethod]
        public void ReturnMinusOne_WhenTheBeerHasNoRatings()
        {
            var options = Utils.GetOptions(nameof(ReturnMinusOne_WhenTheBeerHasNoRatings));

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.GetBeerRatingAsync(20).Result;

                Assert.AreEqual(result, -1.0);
            }
        }
    }
}
