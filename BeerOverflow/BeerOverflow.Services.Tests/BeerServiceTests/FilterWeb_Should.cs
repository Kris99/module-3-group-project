﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Tests.BeerServiceTests
{
    [TestClass]
    public class FilterWeb_Should
    {
        [TestMethod]
        public void FilterByStyleAndReturnCorrectBeers_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(FilterByStyleAndReturnCorrectBeers_WhenParamsAreValid));

            var heineken = new Beer
            {
                Id = 51,
                Name = "Heineken",
                Description = "Belgian style",
                BreweryId = 4,
                ABV = 2.3,
                StyleId = 1
            };

            var zagorka = new Beer
            {
                Id = 52,
                Name = "Zagorka",
                Description = "Awesome taste for people who love beer",
                BreweryId = 1,
                ABV = 4.2,
                StyleId = 1
            };

            var amstel = new Beer
            {
                Id = 53,
                Name = "Amstel",
                Description = "Unique beer with centuries of tradition from Amsterdam",
                BreweryId = 2,
                ABV = 4.7,
                StyleId = 2
            };

            var schofferhofer = new Beer
            {
                Id = 54,
                Name = "Schofferhofer",
                Description = "Unique beer produced in a fine brewery in Germany",
                BreweryId = 3,
                ABV = 2.3,
                StyleId = 2,
            };

            var firstStyle = new Style
            {
                Id = 1,
                Name = "dark"
            };

            var secondStyle = new Style
            {
                Id = 2,
                Name = "light"
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Beers.Add(heineken);
                arrangeContext.Beers.Add(zagorka);
                arrangeContext.Beers.Add(amstel);
                arrangeContext.Beers.Add(schofferhofer);
                arrangeContext.Styles.Add(firstStyle);
                arrangeContext.Styles.Add(secondStyle);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.FilterBeersWebAsync(-1, 1).Result;
                var resultList = result.ToList();

                Assert.AreEqual(2, result.Count());
                Assert.AreEqual(resultList[1].Name, zagorka.Name);
                Assert.AreEqual(resultList[0].Name, heineken.Name);
            }
        }
    }
}
