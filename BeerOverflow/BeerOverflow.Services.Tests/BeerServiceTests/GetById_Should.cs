﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Tests.BeerServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        [TestMethod]
        public void ReturnCorrectBeer_WhenParamsAreValid()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeer_WhenParamsAreValid));
            
            var beers = Utils.SeedBeers().ToList();

            Beer firstBeer = new Beer
            {
                Id = 12,
                Name = "Zagorka",
                Description = "Awesome taste for people who love beer",
                BreweryId = 1,
                ABV = 4.2,
                StyleId = 1,
            };
            Beer secondBeer = new Beer
            {
                Id = 13,
                Name = "Amstel",
                Description = "Unique beer with centuries of tradition from Amsterdam",
                BreweryId = 2,
                ABV = 4.7,
                StyleId = 2,
            };

            var beerDTO = new BeerDTO
            {
                Id = 13,
                Name = "Amstel",
                Description = "Unique beer with centuries of tradition from Amsterdam",
                BreweryId = 2,
                ABV = 4.7,
                StyleId = 2,
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.RemoveRange();
                arrangeContext.SaveChanges();
                arrangeContext.Beers.Add(firstBeer);
                arrangeContext.Beers.Add(secondBeer);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.GetBeerByIdAsync(13).Result;

                Assert.AreEqual(beerDTO.Id, result.Id);
                Assert.AreEqual(beerDTO.Name, result.Name);
                Assert.AreEqual(beerDTO.Description, result.Description);
                Assert.AreEqual(beerDTO.BreweryId, result.BreweryId);
                Assert.AreEqual(beerDTO.ABV, result.ABV);
                Assert.AreEqual(beerDTO.StyleId, result.StyleId);
            }
        }

        [TestMethod]
        public void ReturnNullWhenBeerNotFound()
        {
            var options = Utils.GetOptions(nameof(ReturnNullWhenBeerNotFound));

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);
                var result = sut.GetBeerByIdAsync(156);
                Assert.AreEqual(result.Result, null);
            }
        }
    }
}
