﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers;
using BeerOverflow.Service.Providers.Contracts;
using BeerOverflow.Service.Services;
using Beers.ServicesTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Tests.BeerServiceTests
{
    [TestClass]
    public class GetBeerByRatingAPI
    {
        [TestMethod]
        public void ReturnCorrectBeersSortedByRating()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeersSortedByRating));

            var firstRating = new BeerRating
            {
                Id = 1,
                BeerId = 60,
                UserId = 1,
                Rating = 10
            };

            var secondRating = new BeerRating
            {
                Id = 2,
                BeerId = 61,
                UserId = 2,
                Rating = 8
            };

            var thirdRating = new BeerRating
            {
                Id = 3,
                BeerId = 62,
                UserId = 3,
                Rating = 5
            };

            var heineken = new Beer
            {
                Id = 60,
                Name = "Heineken",
                Description = "Belgian style",
                BreweryId = 4,
                ABV = 2.3,
                StyleId = 1,
                Ratings = { firstRating }
            };
        
            var zagorka = new Beer
            {
                Id = 61,
                Name = "Zagorka",
                Description = "Awesome taste for people who love beer",
                BreweryId = 1,
                ABV = 4.2,
                StyleId = 1,
                Ratings = { secondRating }
            };

            var amstel = new Beer
            {
                Id = 62,
                Name = "Amstel",
                Description = "Unique beer with centuries of tradition from Amsterdam",
                BreweryId = 2,
                ABV = 4.7,
                StyleId = 2,
                Ratings = { thirdRating }
            };

            var dateTimeProviderMock = new Mock<IDateTimeProvider>();

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Beers.Add(heineken);
                arrangeContext.Beers.Add(zagorka);
                arrangeContext.Beers.Add(amstel);
                arrangeContext.Ratings.Add(firstRating);
                arrangeContext.Ratings.Add(secondRating);
                arrangeContext.Ratings.Add(thirdRating);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(assertContext, dateTimeProviderMock.Object);

                var result = sut.GetBeerByRating();
                var resultList = result.ToList();

                Assert.AreEqual(3, result.Count());
                Assert.AreEqual(resultList[0].Id, heineken.Id);
                Assert.AreEqual(resultList[1].Id, zagorka.Id);
                Assert.AreEqual(resultList[2].Id, amstel.Id);
            }
        }
    }
}
