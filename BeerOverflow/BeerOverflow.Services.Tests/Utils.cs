using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Beers.ServicesTests
{
    public class Utils
    {
        public static DbContextOptions<BeerOverflowContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<BeerOverflowContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        public static IEnumerable<Beer> SeedBeers()
        {
            return new Beer[]
            {
                new Beer
            {
                Id = 1,
                Name = "Zagorka",
                Description = "Awesome taste for people who love beer",
                BreweryId = 1,
                ABV = 4.2,
                StyleId = 1
            },
                new Beer
            {
                Id = 2,
                Name = "Amstel",
                Description = "Unique beer with centuries of tradition from Amsterdam",
                BreweryId = 2,
                ABV = 4.7,
                StyleId = 2
             },
                new Beer
            {
                Id = 3,
                Name = "Heineken",
                Description = "Belgian style",
                BreweryId = 4,
                ABV = 2.3,
                StyleId = 1
             },
                new Beer
            {
                Id = 4,
                Name = "Schofferhofer",
                Description = "Unique beer produced in a fine brewery in Germany",
                BreweryId = 3,
                ABV = 2.3,
                StyleId = 2
             },
                new Beer
            {
                Id = 5,
                Name = "Budweiser",
                Description = "Special beer for special people",
                BreweryId = 2,
                ABV = 2.7,
                StyleId = 2
             }
            };
        }
    }
}