﻿using BeerOverflow.Data.Models;
using BeerOverflow.Service.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Service.Contracts
{
    public interface IBeerService
    {
        Task<BeerDTO> CreateBeerAsync(BeerDTO beerDTO);
        Task<BeerDTO> GetBeerByIdAsync(int id);
        Task<IEnumerable<BeerDTO>> GetAllBeersAsync();
        Task<BeerDTO> UpdateBeerAsync(int id, BeerDTO beerDTO);
        Task<bool> DeleteBeerAsync(int id);
        Task<BeerRatingDTO> RateBeerAsync(BeerRatingDTO beerRatingDTO);
        Task<double> GetBeerRatingAsync(int beerId);
        Task<bool> UnlistBeerAsync(int id);
        Task<IEnumerable<BeerDTO>> FilterBeersAsync(string criteria, string filter);
        Task<IEnumerable<BeerDTO>> FilterBeersWebAsync(int filteredCountry, int filteredStyle);
        IEnumerable<BeerDTO> SortBeersAsync(string criteria);
    }
}
