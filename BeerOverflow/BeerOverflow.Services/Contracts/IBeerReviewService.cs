﻿using BeerOverflow.Service.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Service.Contracts
{
    public interface IBeerReviewService
    {
        Task<BeerReviewDTO> CreateBeerReviewAsync(BeerReviewDTO beerReviewDTO);
        Task<BeerReviewDTO> GetBeerReviewByIdAsync(int beerReviewId);
        Task<IEnumerable<BeerReviewDTO>> GetBeerReviewsAsync(int beerId);
        Task<BeerReviewDTO> UpdateBeerReviewAsync(int beerReviewId, BeerReviewDTO beerReviewDTO);
        Task<bool> DeleteBeerReviewAsync(int beerReviewId);
        Task<bool> LikeBeerReviewAsync(int? beerId, int? userId);
        Task<int> GetBeerReviewLikesAsync(int? beerReviewId, int? userReviewId);
        Task<bool> DislikeBeerReviewAsync(int? beerId, int? userId);
        Task<int> GetBeerReviewDislikesAsync(int beerReviewId);
        Task<bool> FlagBeerReviewAsync(int beerReviewId);
    }
}
