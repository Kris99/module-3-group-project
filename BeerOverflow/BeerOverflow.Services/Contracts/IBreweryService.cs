﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BeerOverflow.Service.DTO;

namespace BeerOverflow.Service.Contracts
{
    public interface IBreweryService
    {
        Task<BreweryDTO> GetBreweryAsync(int id);
        Task<BreweryDTO> UpdateBreweryAsync(BreweryDTO breweryDTO);
        Task<BreweryDTO> UpdateBreweryAsync(int id, string name, int countryId);
        Task<BreweryDTO> CreateBreweryAsync(BreweryDTO breweryDTO);
        Task DeleteBreweryAsync(int id);
        Task<IEnumerable<BreweryDTO>> GetAllBreweries();
    }
}
