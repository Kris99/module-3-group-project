﻿using BeerOverflow.Data.Models;
using BeerOverflow.Service.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Service.Contracts
{
    public interface IUserService
    {
         Task<bool> CreateUserAsync(UserDTO user);
         Task<bool> RegisterUserAsync(UserDTO userDTO);
         Task<bool> LoginUserAsync(UserDTO userDTO);
         void LogoutUserAsync();
         void BanUser(int? id);
         void UnbanUser(int? id);
         Task<UserDTO> GetUserById(int? id);
         Task<IEnumerable<UserDTO>> GetAllUsers();
         Task<UserDTO> UpdateUserAsync(User user);
         Task<UserDTO> DeleteUserAsync(int? id);
         Task<bool> AddBeerToWishlistAsync(int? beerID, int? userID);
         Task<bool> AddBeerToDranklistAsync(int? beerID, int? userID);
         IEnumerable<Beer> GetWishlist(int userID);
         IEnumerable<Beer> GetDranklist(int userID);
         Task<bool> CreateUserAsyncTest(UserDTO userDTO);
         IDictionary<Beer, string> GetUsersReviews(int? userID);



    }
}
