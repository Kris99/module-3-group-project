﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BeerOverflow.Service.DTO;

namespace BeerOverflow.Service.Contracts
{
    public interface IStyleService
    {
        Task<StyleDTO> GetStyleAsync(int id);
        Task<StyleDTO> UpdateStyleAsync(int id, string newName);
        Task<StyleDTO> UpdateStyleAsync(StyleDTO styleDTO);
        Task<StyleDTO> CreateStyleAsync(StyleDTO styleDTO);
        Task DeleteStyleAsync(int id);
        Task<IEnumerable<StyleDTO>> GetAllStyles();
    }
}
