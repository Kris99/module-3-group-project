﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BeerOverflow.Service.DTO;

namespace BeerOverflow.Service.Contracts
{
    public interface ICountryService
    {
        Task<CountryDTO> GetCountryAsync(int id);
        Task<CountryDTO> UpdateCountryAsync(int id, string newName);
        Task<CountryDTO> CreateCountryAsync(CountryDTO countryDTO);
        Task DeleteCountryAsync(int id);
        public IEnumerable<CountryDTO> GetAllCountries();
    }
}
