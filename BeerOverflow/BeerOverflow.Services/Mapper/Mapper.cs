﻿using AutoMapper;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Service.Mapper
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
            CreateMap<StyleDTO, Style>();
            CreateMap<Style, StyleDTO>();
            CreateMap<Beer, BeerDTO>();
            CreateMap<BeerDTO, Beer>();

        }
    }
}
