﻿using BeerOverflow.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeerOverflow.Service.DTO
{
    public class BeerDTO
    {
        [DisplayName("ID")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double ABV { get; set; }
        [DisplayName("Style ID")]
        public int StyleId { get; set; }
        [DisplayName("Style name")]
        public string StyleName { get; set; }
        public Style Style { get; set; }
        [DisplayName("Brewery ID")]
        public int BreweryId { get; set; }
        public Brewery Brewery { get; set; }
        [DisplayName("Brewery name")]
        public string BreweryName { get; set; }
        [DisplayName("Is Unlisted")]
        public bool IsUnlisted { get; set; }
        [DisplayName("Is Deleted")]
        public bool IsDeleted { get; set; }
        public ICollection<BeerReview> Reviews { get; set; } = new List<BeerReview>();
        public ICollection<BeerRating> Ratings { get; set; } = new List<BeerRating>();
    }
}
