﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeerOverflow.Service.DTO
{
    public class BreweryDTO
    {
        public BreweryDTO(int id, string name, int countryId, string countryName)
        {
            this.Id = id;
            this.Name = name;
            this.CountryId = countryId;
            this.CountryName = countryName;
        }
        
        public BreweryDTO()
        {
        }

        public string Name { get; set; }
        [DisplayName("ID")]
        public int Id { get; set; }
        [DisplayName("Country Name")]
        public string CountryName { get; set; }
        [DisplayName("Country ID")]
        public int CountryId { get; set; }
    }
}
