﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeerOverflow.Service.DTO
{
    public class ReviewDislikeDTO
    {
        [DisplayName("ID")]
        public int Id { get; set; }
        public int Dislike { get; set; }
        public int BeerReviewId { get; set; }
        [DisplayName("Beer ID")]
        public int BeerId { get; set; }
        //public Beer Beer { get; set; }
        [DisplayName("User ID")]
        public int UserId { get; set; }
        //public User User { get; set; }
    }
}
