﻿using BeerOverflow.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeerOverflow.Service.DTO
{
    public class UserDTO
    {
        public UserDTO()
        {

        }

        public UserDTO(User user)
        {
            this.Id = user.Id;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Email = user.Email;
            this.UserName = user.UserName;
            this.Reviews = user.Reviews;
            this.Ratings = user.Ratings;
            this.Wishlist = user.Wishlists;
            this.Dranklist = user.DrankList;
            this.LockoutEnabled = user.LockoutEnabled;
            this.IsBanned = user.IsBanned;
            this.BanExpirationDate = user.BanExpirationDate;
        }

        [DisplayName("ID")]
        public int Id { get; set; }
        [DisplayName("First name")]
        public string FirstName { get; set; }
        [DisplayName("Last name")]
        public string LastName { get; set; }
        public string Email { get; set; }
        [DisplayName("Username")]
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsBanned { get; set; }
        public DateTime? BanExpirationDate { get; set; }
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }
        public bool LockoutEnabled { get; set; }
        public ICollection<BeerReview> Reviews { get; set; }
        public ICollection<BeerRating> Ratings { get; set; }
        public ICollection<WishlistItem> Wishlist { get; set; }
        public ICollection<DranklistItem> Dranklist { get; set; }
    }
}
