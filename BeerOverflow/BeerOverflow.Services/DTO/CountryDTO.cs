﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace BeerOverflow.Service.DTO
{
    public class CountryDTO
    {
        public CountryDTO(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public CountryDTO()
        {
        }

        [DisplayName("ID")]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
