﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeerOverflow.Service.DTO
{
    public class StyleDTO
    {
        public StyleDTO(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public StyleDTO()
        {
        }

        [DisplayName("ID")]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
