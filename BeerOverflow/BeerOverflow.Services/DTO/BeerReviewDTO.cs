﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeerOverflow.Service.DTO
{
    public class BeerReviewDTO
    {
        [DisplayName("ID")]
        public int Id { get; set; }
        public string Review { get; set; }
        [DisplayName("Liked")]
        public bool IsLiked { get; set; }
        [DisplayName("Disliked")]
        public bool IsDisliked { get; set; }
        [DisplayName("Flagged")]
        public bool IsFlagged { get; set; }
        [DisplayName("Deleted")]
        public bool IsDeleted { get; set; }
        [DisplayName("Beer ID")]
        public int BeerId { get; set; }
        public int Likes { get; set; }
        //public Beer Beer { get; set; }
        [DisplayName("User ID")]
        public int UserId { get; set; }
        //public User User { get; set; }
    }
}
