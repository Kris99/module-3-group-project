﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Service.Providers.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
