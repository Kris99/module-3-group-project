﻿using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Service.Services
{
    public class BeerReviewService : IBeerReviewService
    {
        private readonly BeerOverflowContext _context;
        private readonly IDateTimeProvider dateTimeProvider;

        public BeerReviewService(BeerOverflowContext context, IDateTimeProvider dateTimeProvider)
        {
            _context = context;
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public async Task<BeerReviewDTO> CreateBeerReviewAsync(BeerReviewDTO beerReviewDTO)
        {
            var beerReview = new BeerReview()
            {
                Id = _context.Reviews.Count() + 1,
                BeerId = beerReviewDTO.BeerId,
                UserId = beerReviewDTO.UserId,
                Review = beerReviewDTO.Review,
                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            var beerReviews = _context.Reviews.ToList();

            if(beerReviews.Any(br => br.UserId == beerReview.UserId && br.BeerId == beerReview.BeerId))
            {
                return null;
            }

            _context.Beers.FirstOrDefault(b => b.Id == beerReview.BeerId).Reviews.Add(beerReview);

            await this._context.Reviews.AddAsync(beerReview);
            await this._context.SaveChangesAsync();

            return beerReviewDTO;
        }

        public async Task<BeerReviewDTO> GetBeerReviewByIdAsync(int beerReviewId)
        {
            var beerReview = await this._context.Reviews
                .Where(beerReview => beerReview.Id == beerReviewId && beerReview.IsDeleted == false)
                .FirstOrDefaultAsync();

            if (beerReview == null)
            {
                return null;
            }

            var beerReviewDTO = new BeerReviewDTO
            {
                Id = beerReview.Id,
                BeerId = beerReview.BeerId,
                UserId = beerReview.UserId,
                Review = beerReview.Review,
                Likes = beerReview.Likes
            };

            return beerReviewDTO;
        }

        public async Task<IEnumerable<BeerReviewDTO>> GetBeerReviewsAsync(int beerId)
        {
            var beerReviews = await this._context.Reviews
            .Where(x => (x.BeerId == beerId) && (x.IsDeleted == false))
            .Select(beerReview => new BeerReviewDTO
            {
                Id = beerReview.Id,
                BeerId = beerReview.BeerId,
                UserId = beerReview.UserId,
                Review = beerReview.Review,
                Likes = beerReview.Likes
            }).ToListAsync();

            if (beerReviews.Count == 0)
            {
                return null;
            }

            return beerReviews;
        }

        public async Task<bool> DeleteBeerReviewAsync(int beerReviewId)
        {
            try
            {
                var beerReview = await this._context.Reviews
                    .Where(x => x.Id == beerReviewId && x.IsDeleted == false)
                    .FirstOrDefaultAsync();

                beerReview.IsDeleted = true;
                beerReview.DeletedOn = this.dateTimeProvider.GetDateTime();

                await this._context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<BeerReviewDTO> UpdateBeerReviewAsync(int beerReviewId, BeerReviewDTO beerReviewDTO)
        {
            var beerReview = await this._context.Reviews.Where(x => x.Id == beerReviewId && x.IsDeleted == false)
                    .FirstOrDefaultAsync();

            if (beerReview == null)
            {
                return null;
            }

            beerReview.Review = beerReviewDTO.Review;
            beerReview.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await this._context.SaveChangesAsync();

            return beerReviewDTO;
        }

        public async Task<bool> FlagBeerReviewAsync(int beerReviewId)
        {
            try
            {
                var beer = await this._context.Reviews
                    .Where(x => x.Id == beerReviewId) //(x => x.IsFlagged == false)
                    .FirstOrDefaultAsync();

                beer.IsFlagged = true;

                await this._context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> LikeBeerReviewAsync(int? beerId, int? userId)
        {
            var review = await _context.Reviews.FirstOrDefaultAsync(r => r.UserId == userId && r.BeerId == beerId);
            if(review == null || review.IsLiked == true)
            {
                return false;
            }

            _context.Reviews.First(r => r.UserId == userId && r.BeerId == beerId).IsLiked = true;
            _context.Reviews.First(r => r.UserId == userId && r.BeerId == beerId).Likes += 1;
            await this._context.SaveChangesAsync();

            return true;
        }

        public async Task<int> GetBeerReviewLikesAsync(int? beerId, int? userId)
        {
            var result = _context.Reviews.FirstOrDefault(r => r.BeerId == beerId && r.UserId == userId).Likes;

            return result;
        }

        public async Task<bool> DislikeBeerReviewAsync(int? beerId, int? userId)
        {
            var review = await _context.Reviews.FirstOrDefaultAsync(r => r.UserId == userId && r.BeerId == beerId);
            if (review == null || review.IsDisliked == true)
            {
                return false;
            }

            _context.Reviews.First(r => r.UserId == userId && r.BeerId == beerId).IsDisliked = true;
            _context.Reviews.First(r => r.UserId == userId && r.BeerId == beerId).Likes -= 1;
            await this._context.SaveChangesAsync();

            return true;
        }

        public async Task<int> GetBeerReviewDislikesAsync(int beerReviewId)
        {
            var reviewDislikes = await this._context.ReviewDislikes.Where(x => x.BeerReviewId == beerReviewId).CountAsync();

            return reviewDislikes;
        }
    }
}
