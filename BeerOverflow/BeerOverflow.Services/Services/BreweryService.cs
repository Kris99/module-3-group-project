﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.EntityFrameworkCore;

namespace BeerOverflow.Service.Services
{
    public class BreweryService : IBreweryService
    {
        private readonly BeerOverflowContext _context;

        public BreweryService(BeerOverflowContext context)
        {
            this._context = context;
        }

        public async Task<BreweryDTO> CreateBreweryAsync(BreweryDTO breweryDTO)
        {
            var brewery = new Brewery
            {
                Name = breweryDTO.Name,
                CountryId = breweryDTO.CountryId,
            };
            
            await _context.Breweries.AddAsync(brewery);
            await _context.SaveChangesAsync();
           
            return breweryDTO;
        }
        
        public async Task<IEnumerable<BreweryDTO>> GetAllBreweries()
        {
            var breweries = await _context.Breweries.
              Include(b => b.Country).
              Where(b => b.IsDeleted == false).
              Select(b => new BreweryDTO(b.Id, b.Name, b.CountryId, b.Country.Name)).ToListAsync();

            breweries = breweries.OrderByDescending(x => x.Id).ToList();

            return breweries;
        }


        public async Task<BreweryDTO> GetBreweryAsync(int breweryId)
        {
            var brewery = await this._context.Breweries.Include(b => b.Country).FirstOrDefaultAsync(b => b.Id == breweryId && b.IsDeleted == false);
            
            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            var breweryDTO = new BreweryDTO(brewery.Id, brewery.Name, brewery.CountryId, brewery.Country.Name);

            return breweryDTO;
        }

        public async Task DeleteBreweryAsync(int id)
        {
            var brewery = await this._context.Breweries.FirstOrDefaultAsync(b => b.Id == id && b.IsDeleted == false);

            if (id == 0 || brewery == null)
            {
                throw new ArgumentNullException();
            }

            brewery.IsDeleted = true;
            brewery.DeletedOn = DateTime.Now;

            this._context.Update(brewery);
            await _context.SaveChangesAsync();
        }

        public async Task<BreweryDTO> UpdateBreweryAsync(int id, string newName, int newCountryId)
        {
            var brewery = await _context.Breweries.FirstOrDefaultAsync(brewery => brewery.Id == id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            brewery.Name = newName;
            brewery.CountryId = newCountryId;

            var country = await _context.Countries.FirstOrDefaultAsync(c => c.Id == newCountryId);
            var breweryDTO = new BreweryDTO(brewery.Id, brewery.Name, brewery.CountryId,  country.Name);

            _context.Breweries.Update(brewery);
            await _context.SaveChangesAsync();

            return breweryDTO;
        }
        
        public async Task<BreweryDTO> UpdateBreweryAsync(BreweryDTO breweryDTO)
        {
            var brewery = await _context.Breweries.Include(b=>b.Country).FirstOrDefaultAsync(brewery => brewery.Id == breweryDTO.Id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            brewery.Name = breweryDTO.Name;
            brewery.CountryId = breweryDTO.CountryId;

            breweryDTO = new BreweryDTO(brewery.Id, brewery.Name, brewery.CountryId, brewery.Country.Name);

            _context.Breweries.Update(brewery);
            await _context.SaveChangesAsync();

            return breweryDTO;
        }
    }
}
