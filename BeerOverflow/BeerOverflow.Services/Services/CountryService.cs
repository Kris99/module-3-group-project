﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.EntityFrameworkCore;

namespace BeerOverflow.Service.Services
{
    public class CountryService : ICountryService
    {
        private readonly BeerOverflowContext _context;
        public CountryService(BeerOverflowContext context)
        {
            this._context = context;
        }
        public async Task<CountryDTO> CreateCountryAsync(CountryDTO countryDTO)
        {
            var country = new Country
            {
                Id = countryDTO.Id,
                Name = countryDTO.Name,
            };

            await _context.Countries.AddAsync(country);
            await _context.SaveChangesAsync();

            return countryDTO;
        }

        public async Task<CountryDTO> GetCountryAsync(int countryId)
        {
            var country = await this._context.Countries.FirstOrDefaultAsync(c => c.Id == countryId && c.IsDeleted == false);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            var countryDTO = new CountryDTO(country.Id, country.Name);

            await _context.SaveChangesAsync();

            return countryDTO;
        }

        public IEnumerable<CountryDTO> GetAllCountries()
        {
            var countries = _context.Countries.
              Include(c => c).
              Where(c => c.IsDeleted == false).
              Select(c => new CountryDTO(c.Id, c.Name));

            return countries;
        }

        public async Task DeleteCountryAsync(int id)
        {
            var country = await this._context.Countries.FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false);

            if (id == 0 || country == null)
            {
                throw new ArgumentNullException();
            }

            country.IsDeleted = true;
            country.DeletedOn = DateTime.Now;

            this._context.Update(country);
            await _context.SaveChangesAsync();
        }

        public async Task<CountryDTO> UpdateCountryAsync(int id, string newName)
        {
            var country = await _context.Countries.FirstOrDefaultAsync(c => c.Id == id && c.IsDeleted == false);

            country.Name = newName;
            
            this._context.Update(country);
            await _context.SaveChangesAsync();

            var countryDTO = new CountryDTO(country.Id, country.Name);
            country.ModifiedOn = DateTime.Now;

            return countryDTO;
        }
    }
}
