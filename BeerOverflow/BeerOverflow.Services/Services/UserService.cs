﻿using AutoMapper;
using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Service.Services
{
    public class UserService : IUserService
    {
        private readonly BeerOverflowContext context;
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;

        public UserService(BeerOverflowContext context, IMapper mapper,
                            UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.context = context;
            this.mapper = mapper;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public UserService(BeerOverflowContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<bool> CreateUserAsync(UserDTO userDTO)
        {
            User user = mapper.Map<User>(userDTO);

            var result = await userManager.CreateAsync(user, userDTO.Password);

            if(result.Succeeded)
            {
                await context.Users.AddAsync(user);
                context.Update(user);
                await context.SaveChangesAsync(); 
            }
         
            return result.Succeeded;
        }

        public async Task<bool> RegisterUserAsync(UserDTO userDTO)
        {
            User user = mapper.Map<User>(userDTO);

            var result = await userManager.CreateAsync(user, userDTO.Password);
        
            if (result.Succeeded)
            {
                await userManager.AddToRoleAsync(user, "User");

                await signInManager.SignInAsync(user, isPersistent: true);

                await context.Users.AddAsync(user);
                context.Update(user);
                await context.SaveChangesAsync();  

            }

            return result.Succeeded;
        }

        public async Task<bool> LoginUserAsync(UserDTO userDTO)
        {
            var result = await signInManager.PasswordSignInAsync(userDTO.UserName,
                userDTO.Password, isPersistent: true, false);

            return result.Succeeded;
        }
        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            return await context.Users.Select(u => new UserDTO(u)).ToListAsync();
        }

        public async Task<UserDTO> GetUserById(int? id)
        {
            UserDTO user = await context.Users.Where(u => u.Id == id)
                                                .Select(u => new UserDTO(u))
                                                .FirstOrDefaultAsync();
            if (user == null)
                return null;

            return user;
        }


        public async Task<UserDTO> UpdateUserAsync(User user1)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == user1.Id);

            if(user == null)
            {
                return null;
            }

            user.FirstName = user1.FirstName;
            user.LastName = user1.LastName;
            user.UserName = user1.UserName;
            user.Email = user1.Email;

            context.Update(user);
            await context.SaveChangesAsync();

            var userDTO = new UserDTO(user);

            return userDTO;
        }

        public async Task<UserDTO> DeleteUserAsync(int? id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
                return null;

            context.Users.Remove(user);
            await context.SaveChangesAsync();

            var userDTO = new UserDTO(user);

            return userDTO;
        }

        public async Task<bool> AddBeerToWishlistAsync(int? beerId, int? userID)
        {
            var beer = await context.Beers.FirstOrDefaultAsync(b => b.Id == beerId); //not sure if i have to async here
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == userID);

            if (beer == null || user == null)
                return false;

            WishlistItem item = new WishlistItem
            {
                Id = context.Wishlists.Count() + 1,  
                BeerId = beer.Id,
                Beer = beer,
                UserId = user.Id,
                User = user
            };

            var list = GetWishlist(user.Id).ToList();

            if(list.Any(b => b.Id == item.BeerId))
            {
                return false;
            }

            await context.Wishlists.AddAsync(item);
            await context.SaveChangesAsync();

            return true;
            
        }

        public async Task<bool> AddBeerToDranklistAsync(int? beerID, int? userID)
        {
            var beer = await context.Beers.FirstOrDefaultAsync(b => b.Id == beerID);
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == userID);

            if(beer == null || user == null)
            {
                return false;
            }

            DranklistItem item = new DranklistItem
            {
                Id = context.DrankLists.Count() + 1,  
                BeerId = beer.Id,
                Beer = beer,
                UserId = user.Id,
                User = user
            };

            var list = GetDranklist(user.Id).ToList();

            if (list.Any(b => b.Id == item.BeerId))
            {
                return false;
            }

            await context.DrankLists.AddAsync(item);
            await context.SaveChangesAsync();

            return true;
        }

        public IEnumerable<Beer> GetWishlist(int userID)
        {
            var wishlist = context.Wishlists.Where(ws => ws.UserId == userID);

            if (wishlist == null)
                return null;

            var beerList = new List<Beer>();

            foreach (var item in wishlist)
            {
                var beer = context.Beers.First(b => b.Id == item.BeerId);
                beerList.Add(beer);
            }

            return beerList;
        }

        public IEnumerable<Beer> GetDranklist(int userID)
        {
            var dranklist = context.DrankLists.Where(ws => ws.UserId == userID);

            if (dranklist == null)
                return null;

            var beerList = new List<Beer>();

            foreach (var item in dranklist)
            {
                var beer = context.Beers.First(b => b.Id == item.BeerId);
                beerList.Add(beer);
            }

            return beerList;
        }

        public async void LogoutUserAsync()
        {
            await signInManager.SignOutAsync();
        }

        public void BanUser(int? id) // i don't know why with asynching it wasn't working
        {
            var user = context.Users.FirstOrDefault(u => u.Id == id);

            user.IsBanned = true;
            user.BanExpirationDate = DateTime.Now.AddDays(7);

            context.SaveChanges();
        }

        public void UnbanUser(int? id)
        {
            var user = context.Users.FirstOrDefault(u => u.Id == id);

            user.IsBanned = false;
            user.BanExpirationDate = null;

            context.SaveChanges();
        }

        public async Task<bool> CreateUserAsyncTest(UserDTO userDTO)
        {
            User user = new User
            {
                Id = userDTO.Id,
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                Email = userDTO.Email,
                UserName = userDTO.UserName
            };

            await context.Users.AddAsync(user);
            await context.SaveChangesAsync();  

            return true;
        }

        public IDictionary<Beer, string> GetUsersReviews(int? userID)
        {
            var reviews = context.Reviews.Where(r => r.UserId == userID);

            var dictionary = new Dictionary<Beer, string>();

            foreach (var review in reviews)
            {
                dictionary.Add(review.Beer, review.Review);
            }

            return dictionary;
        }
    }
}
