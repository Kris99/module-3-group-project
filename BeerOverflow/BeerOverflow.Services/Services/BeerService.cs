﻿using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using BeerOverflow.Service.Providers.Contracts;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Service.Services
{
    public class BeerService : IBeerService
    {
        private readonly BeerOverflowContext _context;
        private readonly IDateTimeProvider dateTimeProvider;

        public BeerService(BeerOverflowContext context, IDateTimeProvider dateTimeProvider)
        {
            _context = context;
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public async Task<BeerDTO> GetBeerByIdAsync(int id)
        
        {
            var beer = await this._context.Beers
                .Where(beer => beer.IsDeleted == false && beer.IsUnlisted == false)
                .FirstOrDefaultAsync(beer => beer.Id == id);

            if (beer == null)
            {
                return null;
            }

            var beerDTO = new BeerDTO
            {
                Id = beer.Id,
                Name = beer.Name,
                Description = beer.Description,
                ABV = beer.ABV,
                BreweryId = beer.BreweryId,
                StyleId = beer.StyleId
            };

            return beerDTO;
        }

        public async Task<IEnumerable<BeerDTO>> GetAllBeersAsync()
        {
            var beers = await this._context.Beers
                .Where(beer => (beer.IsDeleted == false) && (beer.IsUnlisted == false))
                .Select(beer => new BeerDTO
                {
                    Id = beer.Id,
                    Name = beer.Name,
                    Description = beer.Description,
                    ABV = beer.ABV,
                    BreweryId = beer.BreweryId,
                    StyleId = beer.StyleId
                })
                .ToListAsync();

            if (beers.Count == 0)
            {
                return null;
            }

            return beers;
        }

        public async Task<BeerDTO> CreateBeerAsync(BeerDTO beerDTO)
        {
            var beer = new Beer()
            {
                Name = beerDTO.Name,
                Description = beerDTO.Description,
                BreweryId = beerDTO.BreweryId,
                ABV = beerDTO.ABV,
                StyleId = beerDTO.StyleId,
                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            await this._context.Beers.AddAsync(beer);
            await this._context.SaveChangesAsync();

            return beerDTO;
        }

        public async Task<BeerDTO> UpdateBeerAsync(int id, BeerDTO beerDTO)
        {
            var beer = await this._context.Beers.Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (beer == null)
            {
                return null;
            }

            beer.Name = beerDTO.Name;
            beer.Description = beerDTO.Description;
            beer.ABV = beerDTO.ABV;
            beer.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await this._context.SaveChangesAsync();

            return beerDTO;
        }

        public async Task<bool> DeleteBeerAsync(int id)
        {
            try
            {
                var beer = await this._context.Beers
                    .Where(x => x.IsDeleted == false)
                    .FirstOrDefaultAsync(x => x.Id == id);

                beer.IsDeleted = true;
                beer.DeletedOn = this.dateTimeProvider.GetDateTime();

                await this._context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UnlistBeerAsync(int id)
        {
            try
            {
                var beer = await this._context.Beers
                    .Where(x => x.IsDeleted == false && x.IsUnlisted == false)
                    .FirstOrDefaultAsync(x => x.Id == id);

                beer.IsUnlisted = true;
                beer.UnlistedOn = this.dateTimeProvider.GetDateTime();

                await this._context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IEnumerable<BeerDTO>> FilterBeersWebAsync(int filterByCountry, int filterByStyle)
        {
            if(filterByCountry == -1 & filterByStyle == -1)
            {
                return GetAllBeersAsync().Result;
            }
            else if(filterByCountry == -1 & filterByStyle != -1)
            {
                List<Beer> beers = await this._context.Beers
                    .Where(x => x.IsDeleted == false && x.IsUnlisted == false && x.StyleId == filterByStyle)
                    .ToListAsync();

                var beersDTO = beers.Select(beer => new BeerDTO
                {
                    Id = beer.Id,
                    Name = beer.Name,
                    Description = beer.Description,
                    ABV = beer.ABV,
                    BreweryId = beer.BreweryId,
                    StyleId = beer.StyleId
                }).ToList();

                return beersDTO;
            }
            else if(filterByCountry != -1 & filterByStyle == -1)
            {
                List<Beer> beers = await this._context.Beers
                    .Where(x => x.IsDeleted == false && x.IsUnlisted == false && x.Brewery.Country.Id == filterByCountry)
                    .ToListAsync();

                var beersDTO = beers.Select(beer => new BeerDTO
                {
                    Id = beer.Id,
                    Name = beer.Name,
                    Description = beer.Description,
                    ABV = beer.ABV,
                    BreweryId = beer.BreweryId,
                    StyleId = beer.StyleId
                }).ToList();

                return beersDTO;
            }
            else
            {
                List<Beer> beers = await this._context.Beers
                    .Where(x => x.IsDeleted == false && x.IsUnlisted == false 
                    && x.Brewery.Country.Id == filterByCountry && x.StyleId == filterByStyle)
                    .ToListAsync();

                var beersDTO = beers.Select(beer => new BeerDTO
                {
                    Id = beer.Id,
                    Name = beer.Name,
                    Description = beer.Description,
                    ABV = beer.ABV,
                    BreweryId = beer.BreweryId,
                    StyleId = beer.StyleId
                }).ToList();

                return beersDTO;
            }
        }

        public async Task<IEnumerable<BeerDTO>> FilterBeersAsync(string criteria, string filter)
        {
            var beers = criteria switch
            {
                "country" => await this._context.Beers.Where(x => x.Brewery.Country.Name.Contains(filter)).ToListAsync(),
                "brewery" => await this._context.Beers.Where(x => x.Brewery.Name.Contains(filter)).ToListAsync(),
                "style" => await this._context.Beers.Where(x => x.Style.Name.Contains(filter)).ToListAsync(),
                _ => throw new InvalidOperationException("Invalid search criteria")
            };

            var resultBeers = beers.Select(beer => new BeerDTO
            {
                Id = beer.Id,
                Name = beer.Name,
                Description = beer.Description,
                ABV = beer.ABV,
                BreweryId = beer.BreweryId,
                StyleId = beer.StyleId
            }).ToList();

            return resultBeers;
        }

        public IEnumerable<BeerDTO> GetBeerByRating()
        {
            var beers = GetAllBeersAsync().Result;
            Dictionary<int, double> idRating = new Dictionary<int, double>();

            foreach (var beer in beers)
            {
                double rating = GetBeerRatingAsync(beer.Id).Result;
                if (rating > -1)
                {
                    idRating.Add(beer.Id, rating);
                }
            }

            idRating = idRating.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, y => y.Value);
            var orderedBeers = new List<BeerDTO>();

            foreach (var orderedBeer in idRating)
            {
                orderedBeers.Add(beers.Where(x => x.Id == orderedBeer.Key).FirstOrDefault());
            }

            return orderedBeers;
        }


        public IEnumerable<BeerDTO> SortBeersAsync(string criteria)
        {
            var sortedBeers = criteria switch
            {
                "name" => GetAllBeersAsync().Result.OrderBy(x => x.Name),
                "ABV" => GetAllBeersAsync().Result.OrderBy(x => x.ABV),
                "rating" => GetBeerByRating(),

                _ => throw new InvalidOperationException("Invalid search criteria")
            };

            return sortedBeers;
        }

        public async Task<BeerRatingDTO> RateBeerAsync(BeerRatingDTO beerRatingDTO)
        {
            var beerRatings = _context.Ratings.ToList();

            var beerRating = new BeerRating()
            {
                Id = _context.Ratings.Count() + 1,
                BeerId = beerRatingDTO.BeerId,
                UserId = beerRatingDTO.UserId,
                Rating = beerRatingDTO.Rating,
                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            if (beerRatings.Any(br => br.BeerId == beerRating.BeerId && br.UserId == beerRating.UserId))
            {
                return null;
            }
            else
            {
                await this._context.Ratings.AddAsync(beerRating);
                await this._context.SaveChangesAsync();

                return beerRatingDTO;
            }
        }

        public async Task<double> GetBeerRatingAsync(int beerId)
        {
            var beerRatings = await this._context.Ratings
            .Where(x => x.BeerId == beerId).Select(y => y.Rating).ToListAsync();

            if(beerRatings.Count > 0)
            {
                double averageRating = Math.Round(((double)beerRatings.Sum() / (double)beerRatings.Count), 1);
                return averageRating;
            }
            else
            {
                return -1;
            }
        }
    }
}
