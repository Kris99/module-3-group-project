﻿using BeerOverflow.Data.Context;
using BeerOverflow.Data.Models;
using BeerOverflow.Service.Contracts;
using BeerOverflow.Service.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Service.Services
{
    public class StyleService : IStyleService
    {
        private readonly BeerOverflowContext _context;
        public StyleService(BeerOverflowContext context)
        {
            this._context = context;
        }

        public async Task<StyleDTO> CreateStyleAsync(StyleDTO styleDTO)
        {
            var style = new Style
            {
                Name = styleDTO.Name,
            };
            
            await _context.Styles.AddAsync(style);
            await _context.SaveChangesAsync();

            return styleDTO;
        }

        public async Task<StyleDTO> GetStyleAsync(int styleId)
        {
            var style = await this._context.Styles.FirstOrDefaultAsync(s => s.Id == styleId && s.IsDeleted == false);

            if (style == null)
            {
                throw new ArgumentNullException();
            }

            var styleDTO = new StyleDTO(style.Id, style.Name);

            await _context.SaveChangesAsync();

            return styleDTO;
        }

        public async Task<IEnumerable<StyleDTO>> GetAllStyles()
        {
            var styles = await _context.Styles
              .Include(s => s)
              .Where(s => s.IsDeleted == false)
              .Select(s => new StyleDTO(s.Id, s.Name)).ToListAsync();

            return styles;
        }

        public async Task DeleteStyleAsync(int id)
        {
            var style = await this._context.Styles.FirstOrDefaultAsync(s => s.Id == id && s.IsDeleted == false);

            if (id == 0 || style == null)
            {
                throw new ArgumentNullException();
            }

            style.IsDeleted = true;
            style.DeletedOn = DateTime.Now;

            this._context.Update(style);
            await _context.SaveChangesAsync();
        }

        public async Task<StyleDTO> UpdateStyleAsync(int id, string newName)
        {
            var style = await _context.Styles.FirstOrDefaultAsync(s => s.Id == id && s.IsDeleted == false);

            style.Name = newName;

            this._context.Update(style);
            await _context.SaveChangesAsync();

            var styleDTO = new StyleDTO(style.Id, style.Name);
            style.ModifiedOn = DateTime.Now;

            return styleDTO;
        }

        public async Task<StyleDTO> UpdateStyleAsync(StyleDTO styleDTO)
        {
            var style = await _context.Styles.FirstOrDefaultAsync(style => style.Id == styleDTO.Id);

            if (style == null)
            {
                throw new ArgumentNullException();
            }

            style.Name = styleDTO.Name;
            styleDTO = new StyleDTO { Name = style.Name };

            _context.Styles.Update(style);
            await _context.SaveChangesAsync();

            return styleDTO;
        }
    }
}
