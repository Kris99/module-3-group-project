##### This is the BeerOverflow team project of Team 12.

##### Team 12 members are Kristian Dimitrov, Georgi I. Georgiev and Mariya Topchieva, part of Telerik Academy Alpha .NET 23.

##### Our Trello Board: https://trello.com/b/EjTNfry5/module-3-group-project

<br />

# **BeerOverflow project**

##### **The BeerOverflow project consists of web application and REST API.**
##### **The BeerOverflow web application manages users and the beers they drink.**
##### **The _mission_ of BeerOverflow is to allow users get an insight on beers from all around the world and choose the next best beer to drink.** 
<br />

## **Functionalities of the BeerOverflow web application**

#### **Each unauthenticated _visitor_ of the BeerOverflow web app can**:

✅ **See all beers** on the app’s database, search through them, filter the results (by country and style) and sort them in ascending or descending order.

✅ See **details of a specific beer**

✅ **Read the beer reviews**, given by registered users

✅ **See the beer’s rating**, an average value of all ratings given by users

✅ **Register** with an account for the web application, **login** and **logout**

<br />

#### **After registration on the BeerOverflow web application, an authenticated _user_ can**:

✅ **Create a beer**

✅ **Add beer** to wish list or list of beers already drank

✅ **Write a beer review**

✅ **Rate a beer** from 1 to 10

✅ **Flag a beer review** as inappropriate

✅ Check his/her **status**

✅ **Track and see** all items in his/her **Beers Wish list** and **Drank Beers list**

<br />

#### **Further to the functionalities available for registered users, the _admin_ user on the BeerOverflow web application can**:

✅ Create, edit, delete and unlist any **beer** on the app’s database

✅ Create, edit and delete any **beer review**, e.g. flagged ones

✅ Ban and delete **users**

<br />

### The **REST API** of BeerOverflow project supports the following functionalities:

- **Countries**: CRUD Operations

- **Breweries**: CRUD Operations

- **Styles**: CRUD Operations

- **Beers**: CRUD Operations; Filter by different criteria (country, style); Sort by name, ABV, rating; Rate a beer

- **Users**: CRUD Operations; Add beer to wish list ; Add beer to the list of beers already drank; Get wish list beers ; Get drank list beers

<br />

## **Technologies used in the BeerOverflow project**:

ASP.NET Core 3.1 and Visual Studio 2019

REST API

Database back-end: MS SQL Server

Entity Framework Core, EF InMemory

Standard ASP.NET Identity System

Unit test framework: MSTest

Mocking framework: Moq

Frontend: HTML, CSS, Bootstrap, DataTables

Team work: Git (gitlab) with separate branches, Trello

Best programming practices and principles used: OOP principles, client-side and server-side data validation, error handling, Dependency Inversion principle, unit testing of the "business" functionality, etc.

<br />

## Views from the BeerOverflow web application

#### Home page

![Text that reveals a missing image](/Home.png)

#### Login page

![Text that reveals a missing image](/Login.png)

#### Users list page

![Text that reveals a missing image](/Users.png)

#### Beers list page Admin view

![Text that reveals a missing image](/AdminViewBeers.PNG)

#### Breweries page

![Text that reveals a missing image](/Breweries.png)

#### Create Beer page

![Text that reveals a missing image](/Create_beer.png)

#### Beer details page User view

![Text that reveals a missing image](/UserViewDetails.PNG)

#### Beer details page Admin view

![Text that reveals a missing image](/AdminViewDetails.PNG)
